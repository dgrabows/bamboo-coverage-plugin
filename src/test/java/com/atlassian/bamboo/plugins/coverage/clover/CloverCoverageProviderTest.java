package com.atlassian.bamboo.plugins.coverage.clover;

import junit.framework.TestCase;
import com.atlassian.bamboo.plugins.coverage.StatisticScope;
import com.atlassian.bamboo.plugins.coverage.CodeCoverageGranularity;

import java.util.Set;
import java.util.HashSet;

public class CloverCoverageProviderTest extends TestCase {
    public void testGetReportParser() throws Exception {
        CloverCoverageProvider provider = new CloverCoverageProvider();
        assertTrue("Parser should be an instance of the clover report parser.",
                provider.getCoverageReportParser(StatisticScope.CLASS) instanceof CloverReportParser);
    }

    public void testSupportedScopes() throws Exception {
        CloverCoverageProvider provider = new CloverCoverageProvider();
        Set expectedScopes = new HashSet();
        expectedScopes.add(StatisticScope.PROJECT);
        expectedScopes.add(StatisticScope.PACKAGE);
        expectedScopes.add(StatisticScope.CLASS);
        assertNotNull("Supported scopes required.", provider.getSupportedScopes());
        assertEquals("Incorrect number of supported scopes.", expectedScopes.size(), provider.getSupportedScopes().length);
        Set actualScopes = new HashSet();
        for (int i = 0; i < provider.getSupportedScopes().length; i++) {
            StatisticScope scope = provider.getSupportedScopes()[i];
            actualScopes.add(scope);
        }
        assertEquals("Actual supported scopes did not match expected.", expectedScopes, actualScopes);
    }

    public void testSupportedGranularities() throws Exception {
        CloverCoverageProvider provider = new CloverCoverageProvider();
        Set expectedGranularities = new HashSet();
        expectedGranularities.add(CodeCoverageGranularity.STATEMENT);
        expectedGranularities.add(CodeCoverageGranularity.ELEMENT);
        expectedGranularities.add(CodeCoverageGranularity.CONDITIONAL);
        expectedGranularities.add(CodeCoverageGranularity.METHOD);
        assertNotNull("Supported granularities required.", provider.getSupportedGranularities());
        assertEquals("Incorrect number of supported granularities", expectedGranularities.size(), provider.getSupportedGranularities().length);
        Set actualGranularities = new HashSet();
        for (int i = 0; i < provider.getSupportedGranularities().length; i++) {
            CodeCoverageGranularity granularity = provider.getSupportedGranularities()[i];
            actualGranularities.add(granularity);
        }
        assertEquals("Actual supported granularities did not match expected.", expectedGranularities, actualGranularities);
    }

    public void testPreferredGranularity() throws Exception {
        assertEquals(CodeCoverageGranularity.ELEMENT, new CloverCoverageProvider().getPreferredGranularity());
    }
}
