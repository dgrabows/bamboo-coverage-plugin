package com.atlassian.bamboo.plugins.coverage;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Tests {@link com.atlassian.bamboo.plugins.coverage.BuildCoverage BuildCoverage}.
 */
public class BuildCoverageTest extends TestCase {

    public void testSummaryContainsProjectStats() throws Exception {
        Set stats = new HashSet();
        CodeCoverageStatistic stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.LINE, 8, 10);
        stat.setDeltaFromPreviousBuild(10.0);
        stats.add(stat);

        BuildCoverage coverage = new BuildCoverage(stats, CodeCoverageGranularity.LINE);
        BuildCoverageSummary summary = coverage.getSummary();
        assertEquals("Project coverage in summary is incorrect.", 80.0, summary.getProjectCoveragePercent());
        assertEquals("Project coverage delta in summary is incorrect.", 10.0, summary.getProjectCoverageDelta());
    }

    public void testUnrankedGranularitiesAreNotStored() throws Exception {
        Set stats = new HashSet();
        CodeCoverageStatistic blockStat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.BLOCK, 8, 10);
        stats.add(blockStat);
        CodeCoverageStatistic lineStat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.LINE, 8, 10);
        stats.add(lineStat);
        CodeCoverageStatistic branchStat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.BRANCH, 8, 10);
        stats.add(branchStat);

        BuildCoverage coverage = new BuildCoverage(stats, CodeCoverageGranularity.BLOCK);
        assertEquals("Incorrect number of detailed stats stored.", 1, coverage.getDetailedStatistics().size());
        assertTrue("Expected stat missing from build coverage detailed stats.", coverage.getDetailedStatistics().contains(blockStat));
        assertFalse("Unexpected stat stored in build coverage detailed stats.", coverage.getDetailedStatistics().contains(lineStat));
        assertFalse("Unexpected stat stored in build coverage detailed stats.", coverage.getDetailedStatistics().contains(branchStat));
    }

    public void testSummaryIgnoresProjectStatsForUnrankedGranularities() throws Exception {
        Set stats = new HashSet();
        CodeCoverageStatistic stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.BLOCK, 8, 10);
        stat.setDeltaFromPreviousBuild(10.0);
        stats.add(stat);

        BuildCoverage coverage = new BuildCoverage(stats, CodeCoverageGranularity.LINE);
        BuildCoverageSummary summary = coverage.getSummary();
        assertNull("Project coverage in summary is incorrect.", summary.getProjectCoveragePercent());
        assertNull("Project coverage delta in summary is incorrect.", summary.getProjectCoverageDelta());
    }

    public void testSummaryContainsCoverageForNewClasses() throws Exception {
        Set stats = new HashSet();
        for (int i = 0; i < 5; i++) {        
            CodeCoverageStatistic stat = new CodeCoverageStatistic("com.test.Class" + i, StatisticScope.CLASS, CodeCoverageGranularity.LINE, 8, 10 + i);
            stat.setNewStatistic(true);
            stats.add(stat);
        }

        BuildCoverage coverage = new BuildCoverage(stats, CodeCoverageGranularity.LINE);
        BuildCoverageSummary summary = coverage.getSummary();
        assertTrue("New class statistics are missing.", summary.getNewClassStatistics().containsAll(stats));
    }

    public void testSummaryDoesNotLimitAtMaximumNumberOfNewClasses() throws Exception {
        Set stats = new HashSet();
        for (int i = 0; i < BuildCoverageSummary.MAX_NEW_CLASSES; i++) {
            CodeCoverageStatistic stat = new CodeCoverageStatistic("com.test.Class" + i, StatisticScope.CLASS, CodeCoverageGranularity.LINE, 8, 10 + i);
            stat.setNewStatistic(true);
            stats.add(stat);
        }

        BuildCoverage coverage = new BuildCoverage(stats, CodeCoverageGranularity.LINE);
        BuildCoverageSummary summary = coverage.getSummary();
        assertTrue("New class statistics are missing.", summary.getNewClassStatistics().containsAll(stats));
        assertFalse("New class statistics should not be limited.", summary.isNewClassStatisticsLimited());
    }

    public void testSummaryLimitsNewClassesPastMaximum() throws Exception {
        Set includedStats = new HashSet();
        for (int i = 0; i < BuildCoverageSummary.MAX_NEW_CLASSES; i++) {
            CodeCoverageStatistic stat = new CodeCoverageStatistic("com.test.Class" + i, StatisticScope.CLASS, CodeCoverageGranularity.LINE, 8, 10 + i);
            stat.setNewStatistic(true);
            includedStats.add(stat);
        }

        CodeCoverageStatistic extraStat = new CodeCoverageStatistic("com.test.ExraClass", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 8, 10);
        extraStat.setNewStatistic(true);

        Set allStats = new HashSet(includedStats);
        allStats.add(extraStat);

        BuildCoverage coverage = new BuildCoverage(allStats, CodeCoverageGranularity.LINE);
        BuildCoverageSummary summary = coverage.getSummary();
        assertTrue("New class statistics are missing.", summary.getNewClassStatistics().containsAll(includedStats));
        assertFalse("Extra new class should not be included in summary.", summary.getNewClassStatistics().contains(extraStat));
        assertTrue("New class statistics should be limited.", summary.isNewClassStatisticsLimited());
    }

    public void testSummaryIgnoresNewClassStatsForUnrankedGranularities() throws Exception {
        Set stats = new HashSet();
        for (int i = 0; i < 5; i++) {
            CodeCoverageStatistic stat = new CodeCoverageStatistic("com.test.Class" + i, StatisticScope.CLASS, CodeCoverageGranularity.LINE, 8, 10 + i);
            stat.setNewStatistic(true);
            stats.add(stat);
        }

        CodeCoverageStatistic unranked = new CodeCoverageStatistic("com.test.UnrankedClass", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 8, 10);
        unranked.setNewStatistic(true);

        Set allStats = new HashSet(stats);
        allStats.add(unranked);

        BuildCoverage coverage = new BuildCoverage(allStats, CodeCoverageGranularity.LINE);
        BuildCoverageSummary summary = coverage.getSummary();
        assertTrue("New class statistics are missing.", summary.getNewClassStatistics().containsAll(stats));
        assertFalse("New class statistics contain stat for wrong granularity.", summary.getNewClassStatistics().contains(unranked));
    }

    public void testDetailStatsNeededToCalculateChanges() throws Exception {
        BuildCoverage coverage = new BuildCoverage(new BuildCoverageSummary());
        BuildCoverage previousBuildCoverage = new BuildCoverage(new BuildCoverageSummary());

        try {
            coverage.populateChangeFromPreviousBuild(previousBuildCoverage);
            fail("Exception should have been thrown.");
        } catch (IllegalStateException e) {}
    }

    public void testPrebuiltSummaryNotModified() throws Exception {
        Set stats = new HashSet();
        CodeCoverageStatistic stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.LINE, 8, 10);
        stat.setDeltaFromPreviousBuild(10.0);
        stats.add(stat);

        BuildCoverageSummary prebuiltSummary = new BuildCoverageSummary();
        prebuiltSummary.setProjectCoverageDelta(15.5);
        prebuiltSummary.setProjectCoveragePercent(87.6);

        BuildCoverage coverage = new BuildCoverage(prebuiltSummary, stats);
        assertEquals("Project coverage in summary was modified.", 87.6, coverage.getSummary().getProjectCoveragePercent());
        assertEquals("Project coverage delta in summary was modified.", 15.5, coverage.getSummary().getProjectCoverageDelta());
    }

    public void testChangesPopulatedCorrectly() throws Exception {
        Set stats = new HashSet();
        CodeCoverageStatistic increasingStat = new CodeCoverageStatistic("com.test.IncreasingClass", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 8, 10);
        stats.add(increasingStat);
        CodeCoverageStatistic decreasingStat = new CodeCoverageStatistic("com.test.decreasingpackage", StatisticScope.PACKAGE, CodeCoverageGranularity.LINE, 5, 10);
        stats.add(decreasingStat);
        CodeCoverageStatistic newStat = new CodeCoverageStatistic("com.test.NewClass", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 3, 10);
        stats.add(newStat);
        BuildCoverage latestCoverage = new BuildCoverage(stats, CodeCoverageGranularity.LINE);

        stats = new HashSet();
        CodeCoverageStatistic previousIncreasingStat = new CodeCoverageStatistic("com.test.IncreasingClass", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 7, 10);
        stats.add(previousIncreasingStat);
        CodeCoverageStatistic previousDecreasingStat = new CodeCoverageStatistic("com.test.decreasingpackage", StatisticScope.PACKAGE, CodeCoverageGranularity.LINE, 8, 10);
        stats.add(previousDecreasingStat);
        CodeCoverageStatistic extraStat = new CodeCoverageStatistic("com.test.ExtraClass", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 5, 10);
        stats.add(extraStat);
        BuildCoverage previousCoverage = new BuildCoverage(stats, CodeCoverageGranularity.LINE);

        latestCoverage.populateChangeFromPreviousBuild(previousCoverage);
        assertEquals("Coverage delta for increasing stat is not correct.", 10.0, increasingStat.getDeltaFromPreviousBuild());
        assertEquals("Coverage delta for decreasing stat is not correct.", -30.0, decreasingStat.getDeltaFromPreviousBuild());
        assertTrue("New class is not flagged as new.", newStat.isNewStatistic());
    }

    public void testIncreasingClassesPopulated() throws Exception {
        List expectedIncreasingClasses = new ArrayList();
        for (double delta = 70.0; delta > 30.0; delta -= 10.0) {
            CodeCoverageStatistic stat = new CodeCoverageStatistic("com.test.IncreasingClass" + delta, StatisticScope.CLASS, CodeCoverageGranularity.LINE, 8, 10);
            stat.setDeltaFromPreviousBuild(delta);
            expectedIncreasingClasses.add(stat);
        }

        Set allStats = new HashSet(expectedIncreasingClasses);
        CodeCoverageStatistic decreasingStat = new CodeCoverageStatistic("com.test.DecreasingClass", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 8, 10);
        decreasingStat.setDeltaFromPreviousBuild(-15.0);
        allStats.add(decreasingStat);

        BuildCoverage coverage = new BuildCoverage(allStats, CodeCoverageGranularity.LINE);
        BuildCoverageSummary summary = coverage.getSummary();
        assertEquals("Increasing class list is not as expected.", expectedIncreasingClasses, summary.getPositiveClassStatistics());
        assertFalse("Increasing class list should not contain class with negative coverage delta.", summary.getPositiveClassStatistics().contains(decreasingStat));
    }

    public void testDecreasingClassesPopulated() throws Exception {
        List expectedDecreasingClasses = new ArrayList();
        for (double delta = -70.0; delta < -30.0; delta += 10.0) {
            CodeCoverageStatistic stat = new CodeCoverageStatistic("com.test.DecreasingClass" + delta, StatisticScope.CLASS, CodeCoverageGranularity.LINE, 8, 10);
            stat.setDeltaFromPreviousBuild(delta);
            expectedDecreasingClasses.add(stat);
        }

        Set allStats = new HashSet(expectedDecreasingClasses);
        CodeCoverageStatistic increasingStat = new CodeCoverageStatistic("com.test.IncreasingClass", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 8, 10);
        increasingStat.setDeltaFromPreviousBuild(15.0);
        allStats.add(increasingStat);

        BuildCoverage coverage = new BuildCoverage(allStats, CodeCoverageGranularity.LINE);
        BuildCoverageSummary summary = coverage.getSummary();
        assertEquals("Decreasing class list is not as expected.", expectedDecreasingClasses, summary.getNegativeClassStatistics());
        assertFalse("Decreasing class list should not contain class with positive coverage delta.", summary.getNegativeClassStatistics().contains(increasingStat));
    }

    public void testIncreasingPackagesPopulated() throws Exception {
        List expectedIncreasingPackages = new ArrayList();
        for (double delta = 70.0; delta > 30.0; delta -= 10.0) {
            CodeCoverageStatistic stat = new CodeCoverageStatistic("com.test.increasingpackage" + delta, StatisticScope.PACKAGE, CodeCoverageGranularity.LINE, 8, 10);
            stat.setDeltaFromPreviousBuild(delta);
            expectedIncreasingPackages.add(stat);
        }

        Set allStats = new HashSet(expectedIncreasingPackages);
        CodeCoverageStatistic decreasingStat = new CodeCoverageStatistic("com.test.decreasingpackage", StatisticScope.PACKAGE, CodeCoverageGranularity.LINE, 8, 10);
        decreasingStat.setDeltaFromPreviousBuild(-15.0);
        allStats.add(decreasingStat);

        BuildCoverage coverage = new BuildCoverage(allStats, CodeCoverageGranularity.LINE);
        BuildCoverageSummary summary = coverage.getSummary();
        assertEquals("Increasing package list is not as expected.", expectedIncreasingPackages, summary.getPositivePackageStatistics());
        assertFalse("Increasing package list should not contain package with negative coverage delta.", summary.getPositivePackageStatistics().contains(decreasingStat));
    }

    public void testDecreasingPackagesPopulated() throws Exception {
        List expectedDecreasingPackages = new ArrayList();
        for (double delta = -70.0; delta < -30.0; delta += 10.0) {
            CodeCoverageStatistic stat = new CodeCoverageStatistic("com.test.decreasingpackage" + delta, StatisticScope.PACKAGE, CodeCoverageGranularity.LINE, 8, 10);
            stat.setDeltaFromPreviousBuild(delta);
            expectedDecreasingPackages.add(stat);
        }

        Set allStats = new HashSet(expectedDecreasingPackages);
        CodeCoverageStatistic increasingStat = new CodeCoverageStatistic("com.test.increasingpackage", StatisticScope.PACKAGE, CodeCoverageGranularity.LINE, 8, 10);
        increasingStat.setDeltaFromPreviousBuild(15.0);
        allStats.add(increasingStat);

        BuildCoverage coverage = new BuildCoverage(allStats, CodeCoverageGranularity.LINE);
        BuildCoverageSummary summary = coverage.getSummary();
        assertEquals("Decreasing package list is not as expected.", expectedDecreasingPackages, summary.getNegativePackageStatistics());
        assertFalse("Decreasing package list should not contain package with positive coverage delta.", summary.getNegativePackageStatistics().contains(increasingStat));
    }
}
