package com.atlassian.bamboo.plugins.coverage;

import junit.framework.TestCase;

public class PositiveCoverageDeltaPredicateTest extends TestCase {
    public void testInstanceMismatchReturnsFalse() throws Exception {
        PositiveCoverageDeltaPredicate predicate = new PositiveCoverageDeltaPredicate();
        assertFalse(predicate.evaluate(new Integer(4)));
    }

    public void testPostiveDeltaReturnsTrue() throws Exception {
        PositiveCoverageDeltaPredicate predicate = new PositiveCoverageDeltaPredicate();
        CodeCoverageStatistic stat = new CodeCoverageStatistic();
        stat.setDeltaFromPreviousBuild(0.1);
        assertTrue(predicate.evaluate(stat));
    }

    public void testNonPositiveDeltaReturnsFalse() throws Exception {
        PositiveCoverageDeltaPredicate predicate = new PositiveCoverageDeltaPredicate();
        CodeCoverageStatistic stat = new CodeCoverageStatistic();
        stat.setDeltaFromPreviousBuild(-0.1);
        assertFalse(predicate.evaluate(stat));
        stat.setDeltaFromPreviousBuild(0.0);
        assertFalse(predicate.evaluate(stat));
    }
}
