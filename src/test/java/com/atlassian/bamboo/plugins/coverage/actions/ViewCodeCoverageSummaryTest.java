package com.atlassian.bamboo.plugins.coverage.actions;

import org.jmock.MockObjectTestCase;
import org.jmock.Mock;

import java.util.*;

import com.atlassian.bamboo.plugins.coverage.*;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;

/**
 * Tests {@link com.atlassian.bamboo.plugins.coverage.actions.ViewCodeCoverageSummary}.
 */
public class ViewCodeCoverageSummaryTest extends MockObjectTestCase {
    protected void setUp() throws Exception {
        mockCoverageProvider = mock(CoverageProvider.class);

        mockCoverageProviderLocator = mock(CoverageProviderLocator.class);
        mockCoverageProviderLocator.expects(once()).method("findCoverageProvider").with(isA(Map.class)).will(returnValue(mockCoverageProvider.proxy()));
    }

    public void testLatestBuildSummaryStatsPopulated() throws Exception {
        List buildResults = new ArrayList();
        buildResults.add(setupMockBuildResultsSummary(65.3, 3.3, true));
        buildResults.add(setupMockBuildResultsSummary(62.0, -2.0, true));
        buildResults.add(setupMockBuildResultsSummary(64.0, 0.0, true));

        ViewCodeCoverageSummary viewCodeCoverageSummary = new ViewCodeCoverageSummary();
        viewCodeCoverageSummary.setResultsList(buildResults);
        viewCodeCoverageSummary.setCoverageProviderLocator((CoverageProviderLocator) mockCoverageProviderLocator.proxy());
        viewCodeCoverageSummary.setCodeCoverageStatisticPersister(new BuildCoverageMapPersister());
        viewCodeCoverageSummary.execute();

        BuildCoverageSummary coverageSummary = viewCodeCoverageSummary.getBuildCoverageSummary();
        assertEquals("Incorrect coverage percentage for latest build.", 65.3, coverageSummary.getProjectCoveragePercent().doubleValue(), 0.001);
        assertEquals("Incorrect coverage delta for latest build.", 3.3, coverageSummary.getProjectCoverageDelta().doubleValue(), 0.001);
        assertEquals("Incorrect number of builds since last coverage decrease.", 1, viewCodeCoverageSummary.getBuildsSinceDecrease().intValue());
    }

    public void testCoverageNeverDecreased() throws Exception {
        List buildResults = new ArrayList();
        buildResults.add(setupMockBuildResultsSummary(65.3, 3.3, true));
        buildResults.add(setupMockBuildResultsSummary(62.0, 2.0, true));
        buildResults.add(setupMockBuildResultsSummary(60.0, 0.0, true));

        ViewCodeCoverageSummary viewCodeCoverageSummary = new ViewCodeCoverageSummary();
        viewCodeCoverageSummary.setResultsList(buildResults);
        viewCodeCoverageSummary.setCoverageProviderLocator((CoverageProviderLocator) mockCoverageProviderLocator.proxy());
        viewCodeCoverageSummary.setCodeCoverageStatisticPersister(new BuildCoverageMapPersister());
        viewCodeCoverageSummary.execute();

        BuildCoverageSummary coverageSummary = viewCodeCoverageSummary.getBuildCoverageSummary();
        assertEquals("Incorrect coverage percentage for latest build.", 65.3, coverageSummary.getProjectCoveragePercent().doubleValue(), 0.001);
        assertEquals("Incorrect coverage delta for latest build.", 3.3, coverageSummary.getProjectCoverageDelta().doubleValue(), 0.001);
        assertEquals("The number of builds since the last coverage decrease should be equal to the total number of builds if coverage has never decreased.",
                3, viewCodeCoverageSummary.getBuildsSinceDecrease().intValue());
    }

    public void testOnlyOneBuildExists() throws Exception {
        List buildResults = new ArrayList();
        buildResults.add(setupMockBuildResultsSummary(65.3, 0.0, true));

        ViewCodeCoverageSummary viewCodeCoverageSummary = new ViewCodeCoverageSummary();
        viewCodeCoverageSummary.setResultsList(buildResults);
        viewCodeCoverageSummary.setCoverageProviderLocator((CoverageProviderLocator) mockCoverageProviderLocator.proxy());
        viewCodeCoverageSummary.setCodeCoverageStatisticPersister(new BuildCoverageMapPersister());
        viewCodeCoverageSummary.execute();

        BuildCoverageSummary coverageSummary = viewCodeCoverageSummary.getBuildCoverageSummary();
        assertEquals("Incorrect coverage percentage for latest build.", 65.3, coverageSummary.getProjectCoveragePercent().doubleValue(), 0.001);
        assertEquals("Incorrect coverage delta for latest build.", 0.0, coverageSummary.getProjectCoverageDelta().doubleValue(), 0.001);
        assertEquals("Incorrect number of builds since last coverage decrease.", 1, viewCodeCoverageSummary.getBuildsSinceDecrease().intValue());
    }

    public void testNoCoverageStatsForAnyBuild() throws Exception {
        List buildResults = new ArrayList();
        buildResults.add(setupMockBuildResultsSummary(0.0, 0.0, false));
        buildResults.add(setupMockBuildResultsSummary(0.0, 0.0, false));
        buildResults.add(setupMockBuildResultsSummary(0.0, 0.0, false));

        ViewCodeCoverageSummary viewCodeCoverageSummary = new ViewCodeCoverageSummary();
        viewCodeCoverageSummary.setResultsList(buildResults);
        viewCodeCoverageSummary.setCoverageProviderLocator((CoverageProviderLocator) mockCoverageProviderLocator.proxy());
        viewCodeCoverageSummary.setCodeCoverageStatisticPersister(new BuildCoverageMapPersister());
        viewCodeCoverageSummary.execute();

        assertNull("No build coverage summary expected.", viewCodeCoverageSummary.getBuildCoverageSummary());
        assertNull("Incorrect number of builds since last coverage decrease.", viewCodeCoverageSummary.getBuildsSinceDecrease());
    }

    public void testNoCoverageStatsForMostRecentBuild() throws Exception {
        List buildResults = new ArrayList();
        buildResults.add(setupMockBuildResultsSummary(0.0, 0.0, false));
        buildResults.add(setupMockBuildResultsSummary(65.3, 3.3, true));
        buildResults.add(setupMockBuildResultsSummary(62.0, -2.0, true));
        buildResults.add(setupMockBuildResultsSummary(64.0, 0.0, true));

        ViewCodeCoverageSummary viewCodeCoverageSummary = new ViewCodeCoverageSummary();
        viewCodeCoverageSummary.setResultsList(buildResults);
        viewCodeCoverageSummary.setCoverageProviderLocator((CoverageProviderLocator) mockCoverageProviderLocator.proxy());
        viewCodeCoverageSummary.setCodeCoverageStatisticPersister(new BuildCoverageMapPersister());
        viewCodeCoverageSummary.execute();

        BuildCoverageSummary coverageSummary = viewCodeCoverageSummary.getBuildCoverageSummary();
        assertEquals("Incorrect coverage percentage for latest build.", 65.3, coverageSummary.getProjectCoveragePercent().doubleValue(), 0.001);
        assertEquals("Incorrect coverage delta for latest build.", 3.3, coverageSummary.getProjectCoverageDelta().doubleValue(), 0.001);
        assertEquals("Incorrect number of builds since last coverage decrease.", 2, viewCodeCoverageSummary.getBuildsSinceDecrease().intValue());
    }

    public void testMissingStatsSkipsBuild() throws Exception {
        List buildResults = new ArrayList();
        buildResults.add(setupMockBuildResultsSummary(65.3, 3.3, true));
        buildResults.add(setupMockBuildResultsSummary(0.0, 0.0, false));
        buildResults.add(setupMockBuildResultsSummary(62.0, -2.0, true));
        buildResults.add(setupMockBuildResultsSummary(64.0, 0.0, true));

        ViewCodeCoverageSummary viewCodeCoverageSummary = new ViewCodeCoverageSummary();
        viewCodeCoverageSummary.setResultsList(buildResults);
        viewCodeCoverageSummary.setCoverageProviderLocator((CoverageProviderLocator) mockCoverageProviderLocator.proxy());
        viewCodeCoverageSummary.setCodeCoverageStatisticPersister(new BuildCoverageMapPersister());
        viewCodeCoverageSummary.execute();

        BuildCoverageSummary coverageSummary = viewCodeCoverageSummary.getBuildCoverageSummary();
        assertEquals("Incorrect coverage percentage for latest build.", 65.3, coverageSummary.getProjectCoveragePercent().doubleValue(), 0.001);
        assertEquals("Incorrect coverage delta for latest build.", 3.3, coverageSummary.getProjectCoverageDelta().doubleValue(), 0.001);
        assertEquals("Incorrect number of builds since last coverage decrease.", 2, viewCodeCoverageSummary.getBuildsSinceDecrease().intValue());
    }

    public void testPluginDisabledDoesNotPopulateStats() throws Exception {
        List buildResults = new ArrayList();
        buildResults.add(setupMockBuildResultsSummary(65.3, 3.3, true));
        buildResults.add(setupMockBuildResultsSummary(62.0, -2.0, true));
        buildResults.add(setupMockBuildResultsSummary(64.0, 0.0, true));

        ViewCodeCoverageSummary viewCodeCoverageSummary = new ViewCodeCoverageSummary();
        viewCodeCoverageSummary.setResultsList(buildResults);
        mockCoverageProviderLocator.reset();
        mockCoverageProviderLocator.expects(once()).method("findCoverageProvider").with(isA(Map.class)).will(returnValue(null));
        viewCodeCoverageSummary.setCoverageProviderLocator((CoverageProviderLocator) mockCoverageProviderLocator.proxy());
        viewCodeCoverageSummary.setCodeCoverageStatisticPersister(new BuildCoverageMapPersister());
        viewCodeCoverageSummary.execute();

        assertNull("No build coverage summary should be present.", viewCodeCoverageSummary.getBuildCoverageSummary());
    }

    private BuildResultsSummary setupMockBuildResultsSummary(double coveragePercent, double coverageDelta, boolean includeStats) {
        Map customData = new HashMap();

        if (includeStats) {
            BuildCoverageSummary summary = new BuildCoverageSummary();
            summary.setProjectCoveragePercent(coveragePercent);
            summary.setProjectCoverageDelta(coverageDelta);
            BuildCoverage buildCoverage = new BuildCoverage(summary);
            new BuildCoverageMapPersister().writeBuildCoverage(customData, buildCoverage);
        }

        Mock mockBuildResults = new Mock(BuildResultsSummary.class);
        mockBuildResults.expects(atLeastOnce()).method("getCustomBuildData").withNoArguments().will(returnValue(customData));

        return (BuildResultsSummary) mockBuildResults.proxy();
    }

    Mock mockCoverageProviderLocator;
    Mock mockCoverageProvider;
}
