package com.atlassian.bamboo.plugins.coverage;

import junit.framework.TestCase;

public class CoverageStatisticPredicateTest extends TestCase {
    public void testInstanceMismatch() throws Exception {
        CoverageStatisticPredicate predicate = new CoverageStatisticPredicate(StatisticScope.PROJECT);
        assertFalse(predicate.evaluate(new Integer(5)));
    }

    public void testScopeOnlyMatch() throws Exception {
        CoverageStatisticPredicate predicate = new CoverageStatisticPredicate(StatisticScope.PROJECT);
        CodeCoverageStatistic stat = new CodeCoverageStatistic();
        stat.setScope(StatisticScope.PROJECT);
        stat.setGranularity(CodeCoverageGranularity.BLOCK);
        stat.setNewStatistic(true);
        assertTrue(predicate.evaluate(stat));
        stat.setScope(StatisticScope.CLASS);
        assertFalse(predicate.evaluate(stat));
    }

    public void testGranularityOnlyMatch() throws Exception {
        CoverageStatisticPredicate predicate = new CoverageStatisticPredicate(CodeCoverageGranularity.BLOCK);
        CodeCoverageStatistic stat = new CodeCoverageStatistic();
        stat.setScope(StatisticScope.PROJECT);
        stat.setGranularity(CodeCoverageGranularity.BLOCK);
        stat.setNewStatistic(true);
        assertTrue(predicate.evaluate(stat));
        stat.setGranularity(CodeCoverageGranularity.BRANCH);
        assertFalse(predicate.evaluate(stat));
    }

    public void testScopeAndPredicateMatch() throws Exception {
        CoverageStatisticPredicate predicate = new CoverageStatisticPredicate(StatisticScope.PROJECT, CodeCoverageGranularity.BLOCK);
        CodeCoverageStatistic stat = new CodeCoverageStatistic();
        stat.setScope(StatisticScope.PROJECT);
        stat.setGranularity(CodeCoverageGranularity.BLOCK);
        stat.setNewStatistic(true);
        assertTrue(predicate.evaluate(stat));
        stat.setScope(StatisticScope.CLASS);
        assertFalse(predicate.evaluate(stat));
        stat.setScope(StatisticScope.PROJECT);
        stat.setGranularity(CodeCoverageGranularity.ELEMENT);
        assertFalse(predicate.evaluate(stat));
        stat.setScope(StatisticScope.CLASS);
        assertFalse(predicate.evaluate(stat));
    }
    
    public void testScopePredicateAndNewMatch() throws Exception {
        CoverageStatisticPredicate predicate = new CoverageStatisticPredicate(StatisticScope.PROJECT, CodeCoverageGranularity.BLOCK, Boolean.TRUE);
        CodeCoverageStatistic stat = new CodeCoverageStatistic();
        stat.setScope(StatisticScope.PROJECT);
        stat.setGranularity(CodeCoverageGranularity.BLOCK);
        stat.setNewStatistic(true);
        assertTrue(predicate.evaluate(stat));
        stat.setScope(StatisticScope.CLASS);
        assertFalse(predicate.evaluate(stat));
        stat.setScope(StatisticScope.PROJECT);
        stat.setGranularity(CodeCoverageGranularity.ELEMENT);
        assertFalse(predicate.evaluate(stat));
        stat.setScope(StatisticScope.CLASS);
        assertFalse(predicate.evaluate(stat));
        stat.setScope(StatisticScope.PROJECT);
        stat.setGranularity(CodeCoverageGranularity.BLOCK);
        stat.setNewStatistic(false);
        assertFalse(predicate.evaluate(stat));
    }
}
