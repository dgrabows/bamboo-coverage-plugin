package com.atlassian.bamboo.plugins.coverage.emma;

import com.atlassian.bamboo.plugins.coverage.*;
import junit.framework.TestCase;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.EqualPredicate;
import org.apache.commons.collections.functors.NotPredicate;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Tests {@link EmmaReportParser}
 */
public class EmmaReportParserTest extends TestCase {

    protected void setUp() throws Exception {
        expectedSmallReportStats = buildExpectedStatsForSmallReport();
    }

    /**
     * Tests support for emma xml reports generated with classes that do not contain debug info. In this case, emma
     * omits line coverage statistics and the source file level in the xml hierarchy.
     */
    public void testEmmaReportWithoutDebugInfo() throws Exception {
        EmmaReportParser parser = new EmmaReportParser(StatisticScope.CLASS);
        CoverageReportParsingResults results = parser.parse(getTestFile("test-emma-no-debug.xml"));
        assertEquals("Incorrect number of project level stats.", 3, CollectionUtils.select(results.getCoverageStats(), new CoverageStatisticPredicate(StatisticScope.PROJECT)).size());
        assertEquals("Incorrect number of package level stats.", 3, CollectionUtils.select(results.getCoverageStats(), new CoverageStatisticPredicate(StatisticScope.PACKAGE)).size());
        assertEquals("Incorrect number of class level stats.", 6, CollectionUtils.select(results.getCoverageStats(), new CoverageStatisticPredicate(StatisticScope.CLASS)).size());
    }

    public void testLoadSmallEmmaReport() throws Exception {
        EmmaReportParser parser = new EmmaReportParser(StatisticScope.CLASS);
        CoverageReportParsingResults results = parser.parse(getTestFile("test-emma-report.xml"));
        assertEquals("Expected and actual statistic counts do not match.", expectedSmallReportStats.size(), results.getCoverageStats().size());
        assertFalse("No errors expected.", results.hasErrors());
        compareCoveragePercents(expectedSmallReportStats, results.getCoverageStats());
    }

    public void testPackageScopeLimit() throws Exception {
        EmmaReportParser parser = new EmmaReportParser(StatisticScope.PACKAGE);
        CollectionUtils.filter(expectedSmallReportStats, new NotPredicate(new CoverageStatisticPredicate(StatisticScope.CLASS)));
        CoverageReportParsingResults results = parser.parse(getTestFile("test-emma-report.xml"));
        assertEquals("Expected and actual statistic counts do not match.", expectedSmallReportStats.size(), results.getCoverageStats().size());
        assertFalse("No errors expected.", results.hasErrors());
        compareCoveragePercents(expectedSmallReportStats, results.getCoverageStats());
    }

    public void testProjectScopeLimit() throws Exception {
        EmmaReportParser parser = new EmmaReportParser(StatisticScope.PROJECT);
        CollectionUtils.filter(expectedSmallReportStats, new CoverageStatisticPredicate(StatisticScope.PROJECT));
        CoverageReportParsingResults results = parser.parse(getTestFile("test-emma-report.xml"));
        assertEquals("Expected and actual statistic counts do not match.", expectedSmallReportStats.size(), results.getCoverageStats().size());
        assertFalse("No errors expected.", results.hasErrors());
        compareCoveragePercents(expectedSmallReportStats, results.getCoverageStats());
    }

    public void testLoadMediumEmmaReport() throws Exception {
        EmmaReportParser parser = new EmmaReportParser(StatisticScope.CLASS);
        CoverageReportParsingResults results = parser.parse(getTestFile("emma-coverage.xml"));
        assertEquals("Incorrect number of class scope stats loaded.", 740, CollectionUtils.select(results.getCoverageStats(), new CoverageStatisticPredicate(StatisticScope.CLASS)).size());
        assertEquals("Incorrect number of package scope stats loaded.", 32, CollectionUtils.select(results.getCoverageStats(), new CoverageStatisticPredicate(StatisticScope.PACKAGE)).size());
        assertEquals("Incorrect number of project scope stats loaded.", 4, CollectionUtils.select(results.getCoverageStats(), new CoverageStatisticPredicate(StatisticScope.PROJECT)).size());
        assertFalse("No errors expected.", results.hasErrors());
    }

    private void compareCoveragePercents(Collection expectedStats, Collection actualStats) {
        for (Iterator iterator = expectedStats.iterator(); iterator.hasNext();) {
            CodeCoverageStatistic expectedStat = (CodeCoverageStatistic) iterator.next();
            CodeCoverageStatistic actualStat = (CodeCoverageStatistic) CollectionUtils.find(actualStats, new EqualPredicate(expectedStat));
            assertNotNull("Unable to find expected code coverage statistic " + expectedStat + ".", actualStat);
            assertEquals("Expected and actual coverage percent do not match for " + expectedStat + ".", expectedStat.getPercent(), actualStat.getPercent(), 0.0);
        }
    }

    public void testMissingDataSkipped() throws Exception {
        EmmaReportParser parser = new EmmaReportParser(StatisticScope.CLASS);
        CoverageReportParsingResults results = parser.parse(getTestFile("test-emma-missing-data.xml"));
        assertFalse("No errors expected.", results.hasErrors());
        assertNull("Project block coverage should not have been loaded.",
                CollectionUtils.find(results.getCoverageStats(), new CoverageStatisticPredicate(StatisticScope.PROJECT, CodeCoverageGranularity.BLOCK)));
    }

    public void testDifferentRootInvalid() throws Exception {
        EmmaReportParser parser = new EmmaReportParser(StatisticScope.CLASS);
        CoverageReportParsingResults results = parser.parse(getTestFile("test-emma-different-root.xml"));
        assertTrue("There should be no stats loaded.", results.getCoverageStats().isEmpty());
        assertTrue("There should be errors.", results.hasErrors());
        assertTrue("Incorrect error message.", ((String) results.getErrorMessages().iterator().next()).startsWith("The coverage plugin skipped"));
    }

    public void testNoDataElementInvalid() throws Exception {
        EmmaReportParser parser = new EmmaReportParser(StatisticScope.CLASS);
        CoverageReportParsingResults results = parser.parse(getTestFile("test-emma-no-data.xml"));
        assertTrue("There should be no stats loaded.", results.getCoverageStats().isEmpty());
        assertTrue("There should be errors.", results.hasErrors());
        assertTrue("Incorrect error message.", ((String) results.getErrorMessages().iterator().next()).startsWith("The coverage plugin skipped"));

    }

    public void testNoStatsElementInvalid() throws Exception {
        EmmaReportParser parser = new EmmaReportParser(StatisticScope.CLASS);
        CoverageReportParsingResults results = parser.parse(getTestFile("test-emma-no-stats.xml"));
        assertTrue("There should be no stats loaded.", results.getCoverageStats().isEmpty());
        assertTrue("There should be errors.", results.hasErrors());
        assertTrue("Incorrect error message.", ((String) results.getErrorMessages().iterator().next()).startsWith("The coverage plugin skipped"));

    }

    private Collection buildExpectedStatsForSmallReport() {
        Collection expectedStats = new ArrayList();

        CodeCoverageStatistic stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.LINE, 12, 16);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.BLOCK, 25, 40);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.CLASS, 2, 2);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.METHOD, 3, 3);
        expectedStats.add(stat);

        stat = new CodeCoverageStatistic("some.package", StatisticScope.PACKAGE, CodeCoverageGranularity.LINE, 12, 24);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("some.package", StatisticScope.PACKAGE, CodeCoverageGranularity.BLOCK, 25, 40);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("some.package", StatisticScope.PACKAGE, CodeCoverageGranularity.CLASS, 1, 2);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("some.package", StatisticScope.PACKAGE, CodeCoverageGranularity.METHOD, 3, 3);
        expectedStats.add(stat);

        stat = new CodeCoverageStatistic("some.package.AClass", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 0, 5);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("some.package.AClass", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 15, 15);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("some.package.AClass", StatisticScope.CLASS, CodeCoverageGranularity.CLASS, 1, 1);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("some.package.AClass", StatisticScope.CLASS, CodeCoverageGranularity.METHOD, 2, 2);
        expectedStats.add(stat);

        stat = new CodeCoverageStatistic("some.package.AnotherClass", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 3, 12);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("some.package.AnotherClass", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 10, 25);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("some.package.AnotherClass", StatisticScope.CLASS, CodeCoverageGranularity.CLASS, 1, 1);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("some.package.AnotherClass", StatisticScope.CLASS, CodeCoverageGranularity.METHOD, 1, 1);
        expectedStats.add(stat);

        return expectedStats;
    }

    private File getTestFile(String fileName) throws Exception {
        return new File(this.getClass().getClassLoader().getResource(fileName).toURI());
    }

    private Collection expectedSmallReportStats;
}
