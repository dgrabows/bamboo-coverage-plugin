package com.atlassian.bamboo.plugins.coverage;

import junit.framework.TestCase;

public class NegativeCoverageDeltaPredicateTest extends TestCase {
    public void testInstanceMismatchReturnsFalse() throws Exception {
        NegativeCoverageDeltaPredicate predicate = new NegativeCoverageDeltaPredicate();
        assertFalse(predicate.evaluate(new Integer(4)));
    }

    public void testNegativeDeltaReturnsTrue() throws Exception {
        NegativeCoverageDeltaPredicate predicate = new NegativeCoverageDeltaPredicate();
        CodeCoverageStatistic stat = new CodeCoverageStatistic();
        stat.setDeltaFromPreviousBuild(-0.1);
        assertTrue(predicate.evaluate(stat));
    }

    public void testNonNegativeDeltaReturnsFalse() throws Exception {
        NegativeCoverageDeltaPredicate predicate = new NegativeCoverageDeltaPredicate();
        CodeCoverageStatistic stat = new CodeCoverageStatistic();
        stat.setDeltaFromPreviousBuild(0.1);
        assertFalse(predicate.evaluate(stat));
        stat.setDeltaFromPreviousBuild(0.0);
        assertFalse(predicate.evaluate(stat));
    }
}
