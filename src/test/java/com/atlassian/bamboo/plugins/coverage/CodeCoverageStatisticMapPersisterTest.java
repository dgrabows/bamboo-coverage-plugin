package com.atlassian.bamboo.plugins.coverage;

import junit.framework.TestCase;

import java.util.*;

import org.apache.commons.collections.CollectionUtils;

public class CodeCoverageStatisticMapPersisterTest extends TestCase {
    public void testSingleStatRoundTrip() throws Exception {
        Set startStats = new HashSet();
        CodeCoverageStatistic startStat = new CodeCoverageStatistic("name", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 345, 1000);
        startStat.setDeltaFromPreviousBuild(2.5);
        startStat.setNewStatistic(true);
        startStats.add(startStat);
        BuildCoverage buildCoverage = new BuildCoverage(startStats, CodeCoverageGranularity.BLOCK);
        Map data = new HashMap();
        persister = new BuildCoverageMapPersister();
        persister.writeBuildCoverage(data, buildCoverage);
        Set endStats = new HashSet(persister.readBuildCoverage(data).getDetailedStatistics());
        assertEquals(1, endStats.size());
        CodeCoverageStatistic endStat = (CodeCoverageStatistic) endStats.iterator().next();
        assertEquals(startStat, endStat);
        assertEquals(startStat.getPercent(), endStat.getPercent(), 0.0);
        assertEquals(startStat.getDeltaFromPreviousBuild(), endStat.getDeltaFromPreviousBuild(), 0.0);
        assertEquals(startStat.isNewStatistic(), endStat.isNewStatistic());
    }

    public void testMultipleStatRoundTrip() throws Exception {
        Set startStats = buildStats();
        BuildCoverage buildCoverage = new BuildCoverage(startStats, CodeCoverageGranularity.LINE);
        Map data = new HashMap();
        persister.writeBuildCoverage(data, buildCoverage);
        Set endStats = new HashSet(persister.readBuildCoverage(data).getDetailedStatistics());
        CollectionUtils.filter(startStats, new CoverageStatisticPredicate(CodeCoverageGranularity.LINE));
        assertEquals(startStats, endStats);
    }

    public void testStatsWithNoVersionIgnored() throws Exception {
        BuildCoverage buildCoverage = new BuildCoverage(buildStats(), CodeCoverageGranularity.LINE);
        Map data = new HashMap();
        persister.writeBuildCoverage(data, buildCoverage);
        data.remove(BuildCoverageMapPersister.VERSION_PROPERTY);
        buildCoverage = persister.readBuildCoverage(data);
        assertNull(buildCoverage);
    }

    public void testContainsSupportedVersionCheck() throws Exception {
        Map data = new HashMap();
        assertFalse(persister.containsStatsForSupportedVersion(data));
        data.put(BuildCoverageMapPersister.VERSION_PROPERTY, "1.0");
        assertFalse(persister.containsStatsForSupportedVersion(data));
        data.clear();
        data.put(BuildCoverageMapPersister.VERSION_PROPERTY, "1.1");
        assertTrue(persister.containsStatsForSupportedVersion(data));
    }

    public void testRemoveDetailsStats() throws Exception {
        BuildCoverage fatBuildCoverage = new BuildCoverage(buildStats(), CodeCoverageGranularity.LINE);
        Map data = new HashMap();
        persister.writeBuildCoverage(data, fatBuildCoverage);
        persister.removeDetailedCoverageStatistics(data);
        BuildCoverage thinBuildCoverage = persister.readBuildCoverage(data);
        assertFalse(thinBuildCoverage.containsDetailedStatistics());        
    }

    public void testSummaryRoundTrip() throws Exception {
        BuildCoverageSummary expectedSummary = new BuildCoverageSummary();
        expectedSummary.setProjectCoveragePercent(85.6);
        expectedSummary.setProjectCoverageDelta(12.5);
        expectedSummary.setNewClassStatisticsLimited(true);

        List stats = new ArrayList();
        CodeCoverageStatistic stat = new CodeCoverageStatistic("com.test.NegativeClass1", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 5, 10);
        stat.setDeltaFromPreviousBuild(-12.0);
        stats.add(stat);
        stat = new CodeCoverageStatistic("com.test.NegativeClass2", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 4, 10);
        stat.setDeltaFromPreviousBuild(-10.0);
        stats.add(stat);
        expectedSummary.addNegativeClassStatistics(stats);

        stats = new ArrayList();
        stat = new CodeCoverageStatistic("com.test.PositiveClass1", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 5, 10);
        stat.setDeltaFromPreviousBuild(12.0);
        stats.add(stat);
        stat = new CodeCoverageStatistic("com.test.PositiveClass2", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 4, 10);
        stat.setDeltaFromPreviousBuild(10.0);
        stats.add(stat);
        expectedSummary.addPositiveClassStatistics(stats);

        stats = new ArrayList();
        stat = new CodeCoverageStatistic("com.test.negativepackageone", StatisticScope.PACKAGE, CodeCoverageGranularity.LINE, 5, 10);
        stat.setDeltaFromPreviousBuild(-13.0);
        stats.add(stat);
        stat = new CodeCoverageStatistic("com.test.negativepackagetwo", StatisticScope.PACKAGE, CodeCoverageGranularity.LINE, 4, 10);
        stat.setDeltaFromPreviousBuild(-11.0);
        stats.add(stat);
        expectedSummary.addNegativePackageStatistics(stats);

        stats = new ArrayList();
        stat = new CodeCoverageStatistic("com.test.positivepackageone", StatisticScope.PACKAGE, CodeCoverageGranularity.LINE, 5, 10);
        stat.setDeltaFromPreviousBuild(13.0);
        stats.add(stat);
        stat = new CodeCoverageStatistic("com.test.positivepackagetwo", StatisticScope.PACKAGE, CodeCoverageGranularity.LINE, 4, 10);
        stat.setDeltaFromPreviousBuild(11.0);
        stats.add(stat);
        expectedSummary.addPositivePackageStatistics(stats);

        stats = new ArrayList();
        stat = new CodeCoverageStatistic("com.test.NewClassOne", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 7, 10);
        stats.add(stat);
        stat = new CodeCoverageStatistic("com.test.NewClassTwo", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 2, 10);
        stats.add(stat);
        expectedSummary.addNewClassStatistics(stats);

        BuildCoverage coverage = new BuildCoverage(expectedSummary);
        Map storage = new HashMap();
        persister.writeBuildCoverage(storage, coverage);
        BuildCoverageSummary actualSummary = persister.readBuildCoverageSummary(storage);
        clearScopeAndGranularity(expectedSummary);
        assertEquals("Build coverage summary not read back in with same values that were written out.", expectedSummary, actualSummary);
    }

    protected void setUp() throws Exception {
        persister = new BuildCoverageMapPersister();
    }

    private void clearScopeAndGranularity(BuildCoverageSummary expectedSummary) {
        clearScopeAndGranularity(expectedSummary.getNewClassStatistics());
        clearScopeAndGranularity(expectedSummary.getPositiveClassStatistics());
        clearScopeAndGranularity(expectedSummary.getNegativeClassStatistics());
        clearScopeAndGranularity(expectedSummary.getPositivePackageStatistics());
        clearScopeAndGranularity(expectedSummary.getNegativePackageStatistics());
    }

    private void clearScopeAndGranularity(List<CodeCoverageStatistic> newClassStatistics) {
        for (CodeCoverageStatistic stat : newClassStatistics) {
            stat.setGranularity(null);
            stat.setScope(null);
        }
    }

    private Set buildStats() {
        Set expectedStats = new HashSet();

        CodeCoverageStatistic stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.LINE, 750, 1000);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.BLOCK, 625, 1000);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.CLASS, 10, 10);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.METHOD, 10, 10);
        expectedStats.add(stat);

        stat = new CodeCoverageStatistic("some.package", StatisticScope.PACKAGE, CodeCoverageGranularity.LINE, 50, 100);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("some.package", StatisticScope.PACKAGE, CodeCoverageGranularity.BLOCK, 625, 1000);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("some.package", StatisticScope.PACKAGE, CodeCoverageGranularity.CLASS, 50, 100);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("some.package", StatisticScope.PACKAGE, CodeCoverageGranularity.METHOD, 15, 15);
        expectedStats.add(stat);

        stat = new CodeCoverageStatistic("some.package.AClass", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 0, 45);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("some.package.AClass", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 100, 100);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("some.package.AClass", StatisticScope.CLASS, CodeCoverageGranularity.CLASS, 100, 100);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("some.package.AClass", StatisticScope.CLASS, CodeCoverageGranularity.METHOD, 100, 100);
        expectedStats.add(stat);

        stat = new CodeCoverageStatistic("some.package.AnotherClass", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 25, 100);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("some.package.AnotherClass", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 40, 100);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("some.package.AnotherClass", StatisticScope.CLASS, CodeCoverageGranularity.CLASS, 100, 100);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("some.package.AnotherClass", StatisticScope.CLASS, CodeCoverageGranularity.METHOD, 100, 100);
        expectedStats.add(stat);

        return expectedStats;
    }

    private BuildCoverageMapPersister persister;
}
