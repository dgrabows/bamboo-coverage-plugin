package com.atlassian.bamboo.plugins.coverage;

import junit.framework.TestCase;

import java.util.Map;
import java.util.HashMap;

import com.atlassian.bamboo.plugins.coverage.clover.CloverCoverageProvider;
import com.atlassian.bamboo.plugins.coverage.emma.EmmaCoverageProvider;
import com.atlassian.bamboo.plugins.coverage.cobertura.CoberturaCoverageProvider;

public class CoverageProviderLocatorTest extends TestCase {
    public void testEmptyProviderName() throws Exception {
        Map buildConfig = new HashMap();
        assertNull(coverageProviderLocator.findCoverageProvider(buildConfig));
        buildConfig.put(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY, "");
        assertNull(coverageProviderLocator.findCoverageProvider(buildConfig));
    }

    public void testReturnsCorrectProvider() throws Exception {
        Map buildConfig = new HashMap();
        buildConfig.put(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY, CoverageBuildProcessor.CLOVER_PROVIDER_NAME);
        assertTrue(coverageProviderLocator.findCoverageProvider(buildConfig) instanceof CloverCoverageProvider);
        buildConfig.put(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY, CoverageBuildProcessor.EMMA_PROVIDER_NAME);
        assertTrue(coverageProviderLocator.findCoverageProvider(buildConfig) instanceof EmmaCoverageProvider);
        buildConfig.put(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY, CoverageBuildProcessor.COBERTURA_PROVIDER_NAME);
        assertTrue(coverageProviderLocator.findCoverageProvider(buildConfig) instanceof CoberturaCoverageProvider);
    }

    public void testInvalidProviderName() throws Exception {
        Map buildConfig = new HashMap();
        buildConfig.put(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY, "stuff");
        assertNull(coverageProviderLocator.findCoverageProvider(buildConfig));
    }

    protected void setUp() throws Exception {
        super.setUp();
        coverageProviderLocator = new CoverageProviderLocatorImpl();
    }

    private CoverageProviderLocatorImpl coverageProviderLocator;
}
