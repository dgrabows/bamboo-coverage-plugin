package com.atlassian.bamboo.plugins.coverage;

import junit.framework.TestCase;

public class CoverageStatisticNameComparatorTest extends TestCase {
    public void testComparison() throws Exception {
        CoverageStatisticNameComparator comparator = new CoverageStatisticNameComparator();
        CodeCoverageStatistic stat1 = new CodeCoverageStatistic();
        stat1.setName("name1");
        CodeCoverageStatistic stat2 = new CodeCoverageStatistic();
        stat2.setName("name2");
        assertEquals("name1".compareTo("name2"), comparator.compare(stat1, stat2));
    }

    public void testClassCastException() throws Exception {
        CoverageStatisticNameComparator comparator = new CoverageStatisticNameComparator();
        CodeCoverageStatistic stat1 = new CodeCoverageStatistic();
        stat1.setName("name1");
        Integer stat2 = new Integer(3);
        try {
            comparator.compare(stat1, stat2);
            fail("Expected class cast exception to be thrown.");
        } catch (ClassCastException e) {
            // expected
        }
    }
}
