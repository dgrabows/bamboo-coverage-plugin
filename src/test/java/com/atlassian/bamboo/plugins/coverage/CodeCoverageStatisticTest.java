package com.atlassian.bamboo.plugins.coverage;

import junit.framework.TestCase;

/**
 * tests functionality of {@link com.atlassian.bamboo.plugins.coverage.CodeCoverageStatistic}.
 */
public class CodeCoverageStatisticTest extends TestCase {
    public void testPercentCalculation() throws Exception {
        CodeCoverageStatistic stat = new CodeCoverageStatistic(null, null, null, 50, 100);
        assertEquals(50.0, stat.getPercent());
        stat = new CodeCoverageStatistic(null, null, null, 1, 3);
        assertEquals(33.33, stat.getPercent(), 0.01);
    }
    
    public void testAvoidDivideByZero() throws Exception {
        CodeCoverageStatistic stat = new CodeCoverageStatistic(null, null, null, 1, 0);
        assertEquals(0.0, stat.getPercent());
    }

    public void testCountsMergeCorrectly() throws Exception {
        CodeCoverageStatistic statOne = new CodeCoverageStatistic("com.test.Class1", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 12L, 24L);
        CodeCoverageStatistic statTwo = new CodeCoverageStatistic("com.test.Class1", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 10L, 30L);

        statOne.merge(statTwo);
        assertEquals(22, statOne.getCoveredCount());
        assertEquals(54, statOne.getTotalCount());
    }

    public void testStatPassedToMergeUnmodified() throws Exception {
        CodeCoverageStatistic statOne = new CodeCoverageStatistic("com.test.Class1", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 12L, 24L);
        CodeCoverageStatistic statTwo = new CodeCoverageStatistic("com.test.Class1", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 10L, 30L);

        statOne.merge(statTwo);
        assertEquals(10, statTwo.getCoveredCount());
        assertEquals(30, statTwo.getTotalCount());
    }
    
    public void testOnlyEqualStatsCanMerge() throws Exception {
        CodeCoverageStatistic statOne = new CodeCoverageStatistic("com.test.Class1", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 12L, 24L);
        CodeCoverageStatistic statTwo = new CodeCoverageStatistic("com.test.Class1", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 10L, 30L);
        CodeCoverageStatistic statThree = new CodeCoverageStatistic("com.test.Class2", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 20L, 35L);

        statOne.merge(statTwo);
        try {
            statTwo.merge(statThree);
            fail("Illegal argument exception should have been thrown.");
        } catch (IllegalArgumentException e) {
            // expected exception
        }
    }
}
