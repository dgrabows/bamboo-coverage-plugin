package com.atlassian.bamboo.plugins.coverage.cobertura;

import com.atlassian.bamboo.plugins.coverage.CodeCoverageGranularity;
import com.atlassian.bamboo.plugins.coverage.StatisticScope;
import junit.framework.TestCase;

import java.util.HashSet;
import java.util.Set;

public class CoberturaCoverageProviderTest extends TestCase {
    public void testGetReportParser() throws Exception {
        CoberturaCoverageProvider provider = new CoberturaCoverageProvider();
        assertTrue("Parser should be an instance of the cobertura report parser.",
                provider.getCoverageReportParser(StatisticScope.CLASS) instanceof CoberturaReportParser);
    }

    public void testSupportedScopes() throws Exception {
        CoberturaCoverageProvider provider = new CoberturaCoverageProvider();
        Set expectedScopes = new HashSet();
        expectedScopes.add(StatisticScope.PROJECT);
        expectedScopes.add(StatisticScope.PACKAGE);
        expectedScopes.add(StatisticScope.CLASS);
        assertNotNull("Supported scopes required.", provider.getSupportedScopes());
        assertEquals("Incorrect number of supported scopes.", expectedScopes.size(), provider.getSupportedScopes().length);
        Set actualScopes = new HashSet();
        for (int i = 0; i < provider.getSupportedScopes().length; i++) {
            StatisticScope scope = provider.getSupportedScopes()[i];
            actualScopes.add(scope);
        }
        assertEquals("Actual supported scopes did not match expected.", expectedScopes, actualScopes);
    }

    public void testSupportedGranularities() throws Exception {
        CoberturaCoverageProvider provider = new CoberturaCoverageProvider();
        Set expectedGranularities = new HashSet();
        expectedGranularities.add(CodeCoverageGranularity.LINE);
        expectedGranularities.add(CodeCoverageGranularity.BRANCH);
        assertNotNull("Supported granularities required.", provider.getSupportedGranularities());
        assertEquals("Incorrect number of supported granularities", expectedGranularities.size(), provider.getSupportedGranularities().length);
        Set actualGranularities = new HashSet();
        for (int i = 0; i < provider.getSupportedGranularities().length; i++) {
            CodeCoverageGranularity granularity = provider.getSupportedGranularities()[i];
            actualGranularities.add(granularity);
        }
        assertEquals("Actual supported granularities did not match expected.", expectedGranularities, actualGranularities);
    }

    public void testPreferredGranularity() throws Exception {
        assertEquals(CodeCoverageGranularity.LINE, new CoberturaCoverageProvider().getPreferredGranularity());
    }
}
