package com.atlassian.bamboo.plugins.coverage.clover;

import com.atlassian.bamboo.utils.BambooTestUtils;
import com.atlassian.bamboo.plugins.coverage.*;
import com.atlassian.bamboo.plugins.coverage.emma.EmmaReportParser;
import junit.framework.TestCase;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.File;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.NotPredicate;
import org.apache.commons.collections.functors.EqualPredicate;

/**
 * Tests {@link CloverReportParser}
 */
public class CloverReportParserTest extends TestCase {
    protected void setUp() throws Exception {
        expectedSmallReportStats = buildExpectedStatsForSmallReport();
    }

    public void testLoadSmallEmmaReport() throws Exception {
        CloverReportParser parser = new CloverReportParser(StatisticScope.CLASS);
        CoverageReportParsingResults results = parser.parse(getTestFile("test-clover-report.xml"));
        assertEquals("Expected and actual statistic counts do not match.", expectedSmallReportStats.size(), results.getCoverageStats().size());
        assertFalse("No errors expected.", results.hasErrors());
        compareCoveragePercents(expectedSmallReportStats, results.getCoverageStats());
    }

    public void testPackageScopeLimit() throws Exception {
        CloverReportParser parser = new CloverReportParser(StatisticScope.PACKAGE);
        CollectionUtils.filter(expectedSmallReportStats, new NotPredicate(new CoverageStatisticPredicate(StatisticScope.CLASS)));
        CoverageReportParsingResults results = parser.parse(getTestFile("test-clover-report.xml"));
        assertEquals("Expected and actual statistic counts do not match.", expectedSmallReportStats.size(), results.getCoverageStats().size());
        assertFalse("No errors expected.", results.hasErrors());
        compareCoveragePercents(expectedSmallReportStats, results.getCoverageStats());
    }

    public void testProjectScopeLimit() throws Exception {
        CloverReportParser parser = new CloverReportParser(StatisticScope.PROJECT);
        CollectionUtils.filter(expectedSmallReportStats, new CoverageStatisticPredicate(StatisticScope.PROJECT));
        CoverageReportParsingResults results = parser.parse(getTestFile("test-clover-report.xml"));
        assertEquals("Expected and actual statistic counts do not match.", expectedSmallReportStats.size(), results.getCoverageStats().size());
        assertFalse("No errors expected.", results.hasErrors());
        compareCoveragePercents(expectedSmallReportStats, results.getCoverageStats());
    }

    public void testLoadMediumReport() throws Exception {
        CloverReportParser parser = new CloverReportParser(StatisticScope.CLASS);
        CoverageReportParsingResults results = parser.parse(getTestFile("test-clover-medium.xml"));
        assertEquals("Incorrect number of class scope stats loaded.", 1900, CollectionUtils.select(results.getCoverageStats(), new CoverageStatisticPredicate(StatisticScope.CLASS)).size());
        assertEquals("Incorrect number of package scope stats loaded.", 264, CollectionUtils.select(results.getCoverageStats(), new CoverageStatisticPredicate(StatisticScope.PACKAGE)).size());
        assertEquals("Incorrect number of project scope stats loaded.", 4, CollectionUtils.select(results.getCoverageStats(), new CoverageStatisticPredicate(StatisticScope.PROJECT)).size());
        assertFalse("No errors expected.", results.hasErrors());
    }

    public void testInvalidFileFormat() throws Exception {
        CloverReportParser parser = new CloverReportParser(StatisticScope.CLASS);
        CoverageReportParsingResults results = parser.parse(getTestFile("test-cobertura-report.xml"));
        assertEquals("Incorrect number of statistics loaded.", 0, results.getCoverageStats().size());
        assertTrue("Errors expected.", results.hasErrors());
        assertTrue("Incorrect error message.", ((String) results.getErrorMessages().iterator().next()).startsWith("The coverage plugin skipped"));
    }

    private void compareCoveragePercents(Collection expectedStats, Collection actualStats) {
        for (Iterator iterator = expectedStats.iterator(); iterator.hasNext();) {
            CodeCoverageStatistic expectedStat = (CodeCoverageStatistic) iterator.next();
            CodeCoverageStatistic actualStat = (CodeCoverageStatistic) CollectionUtils.find(actualStats, new EqualPredicate(expectedStat));
            assertNotNull("Unable to find expected code coverage statistic " + expectedStat + ".", actualStat);
            assertEquals("Expected and actual coverage percent do not match for " + expectedStat + ".", expectedStat.getPercent(), actualStat.getPercent(), 0.1);
        }
    }

    private Collection buildExpectedStatsForSmallReport() {
        Collection expectedStats = new ArrayList();

        CodeCoverageStatistic stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.ELEMENT, 21, 53);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.CONDITIONAL, 6, 16);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.METHOD, 1, 4);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.STATEMENT, 14, 33);
        expectedStats.add(stat);

        stat = new CodeCoverageStatistic("grabowski.dan.bamboo.plugin", StatisticScope.PACKAGE, CodeCoverageGranularity.ELEMENT, 21, 53);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("grabowski.dan.bamboo.plugin", StatisticScope.PACKAGE, CodeCoverageGranularity.CONDITIONAL, 6, 16);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("grabowski.dan.bamboo.plugin", StatisticScope.PACKAGE, CodeCoverageGranularity.METHOD, 1, 4);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("grabowski.dan.bamboo.plugin", StatisticScope.PACKAGE, CodeCoverageGranularity.STATEMENT, 14, 33);
        expectedStats.add(stat);

        stat = new CodeCoverageStatistic("grabowski.dan.bamboo.plugin.BuildLabeller", StatisticScope.CLASS, CodeCoverageGranularity.ELEMENT, 21, 53);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("grabowski.dan.bamboo.plugin.BuildLabeller", StatisticScope.CLASS, CodeCoverageGranularity.CONDITIONAL, 6, 16);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("grabowski.dan.bamboo.plugin.BuildLabeller", StatisticScope.CLASS, CodeCoverageGranularity.METHOD, 1, 4);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("grabowski.dan.bamboo.plugin.BuildLabeller", StatisticScope.CLASS, CodeCoverageGranularity.STATEMENT, 14, 33);
        expectedStats.add(stat);

        return expectedStats;
    }

    private File getTestFile(String fileName) throws Exception {
        return new File(this.getClass().getClassLoader().getResource(fileName).toURI());
    }

    private Collection expectedSmallReportStats;
}
