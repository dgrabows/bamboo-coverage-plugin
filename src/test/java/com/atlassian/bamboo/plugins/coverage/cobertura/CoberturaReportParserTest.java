package com.atlassian.bamboo.plugins.coverage.cobertura;

import com.atlassian.bamboo.plugins.coverage.*;
import junit.framework.TestCase;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.EqualPredicate;
import org.apache.commons.collections.functors.NotPredicate;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Tests {@link CoberturaReportParser}
 */
public class CoberturaReportParserTest extends TestCase {

    protected void setUp() throws Exception {
        expectedSmallReportStats = buildExpectedStatsForSmallReport();
    }

    public void testCoberturaVersion1_9() throws Exception {
        CoberturaReportParser parser = new CoberturaReportParser(StatisticScope.CLASS);
        CoverageReportParsingResults results = parser.parse(getTestFile("cobertura-1.9.xml"));
        assertEquals("Incorrect number of class scope stats loaded.", 682, CollectionUtils.select(results.getCoverageStats(), new CoverageStatisticPredicate(StatisticScope.CLASS)).size());
        assertEquals("Incorrect number of package scope stats loaded.", 42, CollectionUtils.select(results.getCoverageStats(), new CoverageStatisticPredicate(StatisticScope.PACKAGE)).size());
        assertEquals("Incorrect number of project scope stats loaded.", 2, CollectionUtils.select(results.getCoverageStats(), new CoverageStatisticPredicate(StatisticScope.PROJECT)).size());
        assertFalse("No errors expected.", results.hasErrors());
    }

    public void testLoadSmallReport() throws Exception {
        CoberturaReportParser parser = new CoberturaReportParser(StatisticScope.CLASS);
        CoverageReportParsingResults results = parser.parse(getTestFile("test-cobertura-report.xml"));
        assertEquals("Expected and actual statistic counts do not match.", expectedSmallReportStats.size(), results.getCoverageStats().size());
        assertFalse("No errors expected.", results.hasErrors());
        compareCoveragePercents(expectedSmallReportStats, results.getCoverageStats());
    }

    public void testPackageScopeLimit() throws Exception {
        CoberturaReportParser parser = new CoberturaReportParser(StatisticScope.PACKAGE);
        CollectionUtils.filter(expectedSmallReportStats, new NotPredicate(new CoverageStatisticPredicate(StatisticScope.CLASS)));
        CoverageReportParsingResults results = parser.parse(getTestFile("test-cobertura-report.xml"));
        assertEquals("Expected and actual statistic counts do not match.", expectedSmallReportStats.size(), results.getCoverageStats().size());
        assertFalse("No errors expected.", results.hasErrors());
        compareCoveragePercents(expectedSmallReportStats, results.getCoverageStats());
    }

    public void testProjectScopeLimit() throws Exception {
        CoberturaReportParser parser = new CoberturaReportParser(StatisticScope.PROJECT);
        CollectionUtils.filter(expectedSmallReportStats, new CoverageStatisticPredicate(StatisticScope.PROJECT));
        CoverageReportParsingResults results = parser.parse(getTestFile("test-cobertura-report.xml"));
        assertEquals("Expected and actual statistic counts do not match.", expectedSmallReportStats.size(), results.getCoverageStats().size());
        assertFalse("No errors expected.", results.hasErrors());
        compareCoveragePercents(expectedSmallReportStats, results.getCoverageStats());
    }

    public void testLoadMediumReport() throws Exception {
        CoberturaReportParser parser = new CoberturaReportParser(StatisticScope.CLASS);
        CoverageReportParsingResults results = parser.parse(getTestFile("test-cobertura-medium.xml"));
        assertEquals("Incorrect number of class scope stats loaded.", 400, CollectionUtils.select(results.getCoverageStats(), new CoverageStatisticPredicate(StatisticScope.CLASS)).size());
        assertEquals("Incorrect number of package scope stats loaded.", 38, CollectionUtils.select(results.getCoverageStats(), new CoverageStatisticPredicate(StatisticScope.PACKAGE)).size());
        assertEquals("Incorrect number of project scope stats loaded.", 2, CollectionUtils.select(results.getCoverageStats(), new CoverageStatisticPredicate(StatisticScope.PROJECT)).size());
        assertFalse("No errors expected.", results.hasErrors());
    }

    public void testInvalidFileFormat() throws Exception {
        CoberturaReportParser parser = new CoberturaReportParser(StatisticScope.CLASS);
        CoverageReportParsingResults results = parser.parse(getTestFile("test-clover-report.xml"));
        assertEquals("Incorrect number of statistics loaded.", 0, results.getCoverageStats().size());
        assertTrue("Errors expected.", results.hasErrors());
        assertTrue("Incorrect error message.", ((String) results.getErrorMessages().iterator().next()).startsWith("The coverage plugin skipped"));
    }

    private void compareCoveragePercents(Collection expectedStats, Collection actualStats) {
        for (Iterator iterator = expectedStats.iterator(); iterator.hasNext();) {
            CodeCoverageStatistic expectedStat = (CodeCoverageStatistic) iterator.next();
            CodeCoverageStatistic actualStat = (CodeCoverageStatistic) CollectionUtils.find(actualStats, new EqualPredicate(expectedStat));
            assertNotNull("Unable to find expected code coverage statistic " + expectedStat + ".", actualStat);
            assertEquals("Expected and actual coverage percent do not match for " + expectedStat + ".", expectedStat.getPercent(), actualStat.getPercent(), 0.1);
        }
    }

    private Collection buildExpectedStatsForSmallReport() {
        Collection expectedStats = new ArrayList();

        CodeCoverageStatistic stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.LINE, 90, 131);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.BRANCH, 24, 64);
        expectedStats.add(stat);

        stat = new CodeCoverageStatistic("com.opensymphony.xwork2", StatisticScope.PACKAGE, CodeCoverageGranularity.LINE, 90, 131);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("com.opensymphony.xwork2", StatisticScope.PACKAGE, CodeCoverageGranularity.BRANCH, 24, 64);
        expectedStats.add(stat);

        stat = new CodeCoverageStatistic("com.opensymphony.xwork2.ActionChainResult", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 47, 81);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("com.opensymphony.xwork2.ActionChainResult", StatisticScope.CLASS, CodeCoverageGranularity.BRANCH, 18, 58);
        expectedStats.add(stat);

        stat = new CodeCoverageStatistic("com.opensymphony.xwork2.ActionContext", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 43, 50);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("com.opensymphony.xwork2.ActionContext", StatisticScope.CLASS, CodeCoverageGranularity.BRANCH, 6, 6);
        expectedStats.add(stat);

        return expectedStats;
    }

    private File getTestFile(String fileName) throws Exception {
        return new File(this.getClass().getClassLoader().getResource(fileName).toURI());
    }

    private Collection expectedSmallReportStats;
}
