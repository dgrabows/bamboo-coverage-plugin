package com.atlassian.bamboo.plugins.coverage;

import com.atlassian.bamboo.build.Build;
import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.plugins.coverage.cobertura.CoberturaReportParser;
import com.atlassian.bamboo.results.BuildResults;
import com.atlassian.bamboo.results.BuildResultsImpl;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.Session;
import net.sf.hibernate.SessionFactory;
import net.sf.hibernate.Transaction;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.EqualPredicate;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

import java.io.File;
import java.util.*;

public class CoverageBuildProcessorTest extends MockObjectTestCase {
    public void testOnlyValidateIfCoverageEnabled() throws Exception {
        CoverageBuildProcessor processor = new CoverageBuildProcessor();

        BuildConfiguration config = new BuildConfiguration();
        ErrorCollection errors = processor.validate(config);
        assertFalse("There should be no validation errors.", errors.hasAnyErrors());

        config.setProperty(CoverageBuildProcessor.COVERAGE_ENABLED_KEY, "false");
        errors = processor.validate(config);
        assertFalse("There should be no validation errors.", errors.hasAnyErrors());
    }

    public void testValidateRequiredConfiguration() throws Exception {
        CoverageBuildProcessor processor = new CoverageBuildProcessor();

        BuildConfiguration config = new BuildConfiguration();
        config.setProperty(CoverageBuildProcessor.COVERAGE_ENABLED_KEY, "true");

        Map expectedErrors = new HashMap();
        expectedErrors.put(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY, "Please specify the code coverage provider.");
        expectedErrors.put(CoverageBuildProcessor.COVERAGE_RESULTS_XML_PATH_KEY, "Please specify the directory containing the code coverage XML report file.");
        expectedErrors.put(CoverageBuildProcessor.COVERAGE_SCOPE_KEY, "Please specify the code coverage scope.");

        ErrorCollection errors = processor.validate(config);
        assertTrue("There should be validation errors.", errors.hasAnyErrors());
        assertEquals("Errors do not match expected.", expectedErrors, errors.getErrors());        
    }

    public void testValidConfiguration() throws Exception {
        CoverageBuildProcessor processor = new CoverageBuildProcessor();

        BuildConfiguration config = new BuildConfiguration();
        config.setProperty(CoverageBuildProcessor.COVERAGE_ENABLED_KEY, "true");
        config.setProperty(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY, CoverageBuildProcessor.EMMA_PROVIDER_NAME);
        config.setProperty(CoverageBuildProcessor.COVERAGE_RESULTS_XML_PATH_KEY, "**/target/*.xml");
        config.setProperty(CoverageBuildProcessor.COVERAGE_SCOPE_KEY, StatisticScope.CLASS.toString());
        
        ErrorCollection errors = processor.validate(config);
        assertFalse("There should be no validation errors.", errors.hasAnyErrors());
    }

    public void testNoProcessingWhenEnabledSettingNull() throws Exception {
        Map customBuildConfiguration = new HashMap();

        Build build = setupMockBuild(customBuildConfiguration);

        Mock mockBuildResults = mock(BuildResults.class);
        mockBuildResults.expects(never()).method("getCustomBuildData");
        BuildResults buildResults = (BuildResults) mockBuildResults.proxy();

        CoverageBuildProcessor processor = new CoverageBuildProcessor();
        processor.run(build, buildResults);
    }

    public void testNoProcessingWhenDisabled() throws Exception {
        Map customBuildConfiguration = new HashMap();
        customBuildConfiguration.put(CoverageBuildProcessor.COVERAGE_ENABLED_KEY, "false");

        Build build = setupMockBuild(customBuildConfiguration);

        Mock mockBuildResults = mock(BuildResults.class);
        mockBuildResults.expects(never()).method("getCustomBuildData");
        BuildResults buildResults = (BuildResults) mockBuildResults.proxy();

        CoverageBuildProcessor processor = new CoverageBuildProcessor();
        processor.run(build, buildResults);
    }

    public void testCleanRun() throws Exception {
        // setup the the build
        Map customBuildConfiguration = new HashMap();
        String resultsPath = "target/**/*.xml";
        customBuildConfiguration.put(CoverageBuildProcessor.COVERAGE_ENABLED_KEY, "true");
        customBuildConfiguration.put(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY, CoverageBuildProcessor.COBERTURA_PROVIDER_NAME);
        customBuildConfiguration.put(CoverageBuildProcessor.COVERAGE_SCOPE_KEY, StatisticScope.CLASS.toString());
        customBuildConfiguration.put(CoverageBuildProcessor.COVERAGE_RESULTS_XML_PATH_KEY, resultsPath);
        Build build = setupMockBuild(customBuildConfiguration);

        // setup the build results
        Map previousCustomBuildData = new HashMap();
        BuildCoverage previousBuildCoverage = new BuildCoverage(setupPreviousBuildCoverageStats(), CodeCoverageGranularity.LINE);
        new BuildCoverageMapPersister().writeBuildCoverage(previousCustomBuildData, previousBuildCoverage);
        BuildResultsSummary previousBuildResultsSummary = setupMockBuildResultsSummary(previousCustomBuildData);
        BuildResults previousBuildResults = setupMockBuildResults(null, previousBuildResultsSummary, new BuildResultsImpl(), null, null);

        Map customBuildData = new HashMap();
        File baseDirectory = new File("/temp/project");
        BuildResults buildResults = setupMockBuildResults(customBuildData, null, previousBuildResults, baseDirectory, null);

        // setup the dependencies
        CoverageReportParser coverageReportParser = new CoberturaReportParser(StatisticScope.CLASS);
        CoverageProvider coverageProvider = setupMockCoverageProvider(coverageReportParser);
        CoverageProviderLocator coverageProviderLocator = setupMockCoverageProviderLocator(customBuildConfiguration, coverageReportParser, coverageProvider);
        FileVisitorFactory fileVisitorFactory = setupMockFileVisitorFactory(resultsPath, setupCoverageParseResults(), baseDirectory, coverageProvider, StatisticScope.CLASS);
        SessionFactory sessionFactory = setupMockSessionFactory(previousBuildResultsSummary, false, false);

        CoverageBuildProcessor processor = new CoverageBuildProcessor();
        processor.setCoverageProviderLocator(coverageProviderLocator);
        processor.setFileVisitorFactory(fileVisitorFactory);
        processor.setSessionFactory(sessionFactory);
        processor.setCodeCoverageStatisticPersister(new BuildCoverageMapPersister());

        // execute the code being tested
        processor.run(build, buildResults);

        // check the results
        assertTrue("Coverage provider name should be saved to custom build data.", customBuildData.containsKey(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY));
        assertEquals("Incorrect coverage provider name saved to custom build data.", CoverageBuildProcessor.COBERTURA_PROVIDER_NAME, customBuildData.get(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY));

        Set<CodeCoverageStatistic> expectedStats = setupExpectedStats();
        BuildCoverage buildCoverage = new BuildCoverageMapPersister().readBuildCoverage(customBuildData);
        checkCoverageStats(expectedStats, buildCoverage.getDetailedStatistics());
        // todo check that previous detailed stats were deleted
        // todo check values in build coverage summary
    }

    public void testParseErrorAddedToResults() throws Exception {
        // setup the the build
        Map customBuildConfiguration = new HashMap();
        String resultsPath = "target/**/*.xml";
        customBuildConfiguration.put(CoverageBuildProcessor.COVERAGE_ENABLED_KEY, "true");
        customBuildConfiguration.put(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY, CoverageBuildProcessor.COBERTURA_PROVIDER_NAME);
        customBuildConfiguration.put(CoverageBuildProcessor.COVERAGE_SCOPE_KEY, StatisticScope.CLASS.toString());
        customBuildConfiguration.put(CoverageBuildProcessor.COVERAGE_RESULTS_XML_PATH_KEY, resultsPath);
        Build build = setupMockBuild(customBuildConfiguration);

        // setup the build results
        Map previousCustomBuildData = new HashMap();
        BuildCoverage previousBuildCoverage = new BuildCoverage(setupPreviousBuildCoverageStats(), CodeCoverageGranularity.LINE);
        new BuildCoverageMapPersister().writeBuildCoverage(previousCustomBuildData, previousBuildCoverage);
        BuildResultsSummary previousBuildResultsSummary = setupMockBuildResultsSummary(previousCustomBuildData);
        BuildResults previousBuildResults = setupMockBuildResults(null, previousBuildResultsSummary, new BuildResultsImpl(), null, null);

        Map customBuildData = new HashMap();
        File baseDirectory = new File("/temp/project");
        BuildResults buildResults = setupMockBuildResults(customBuildData, null, previousBuildResults, baseDirectory, "Some error.");

        // setup the dependencies
        CoverageReportParser coverageReportParser = new CoberturaReportParser(StatisticScope.CLASS);
        CoverageProvider coverageProvider = setupMockCoverageProvider(coverageReportParser);
        CoverageProviderLocator coverageProviderLocator = setupMockCoverageProviderLocator(customBuildConfiguration, coverageReportParser, coverageProvider);
        CoverageReportParsingResults results = setupCoverageParseResults();
        results.addErrorMessage("Some error.");
        FileVisitorFactory fileVisitorFactory = setupMockFileVisitorFactory(resultsPath, results, baseDirectory, coverageProvider, StatisticScope.CLASS);
        SessionFactory sessionFactory = setupMockSessionFactory(previousBuildResultsSummary, false, false);

        CoverageBuildProcessor processor = new CoverageBuildProcessor();
        processor.setCoverageProviderLocator(coverageProviderLocator);
        processor.setFileVisitorFactory(fileVisitorFactory);
        processor.setSessionFactory(sessionFactory);
        processor.setCodeCoverageStatisticPersister(new BuildCoverageMapPersister());

        // execute the code being tested
        processor.run(build, buildResults);

        // check the results
        assertTrue("Coverage provider name should be saved to custom build data.", customBuildData.containsKey(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY));
        assertEquals("Incorrect coverage provider name saved to custom build data.", CoverageBuildProcessor.COBERTURA_PROVIDER_NAME, customBuildData.get(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY));

        Set<CodeCoverageStatistic> expectedStats = setupExpectedStats();
        BuildCoverage buildCoverage = new BuildCoverageMapPersister().readBuildCoverage(customBuildData);
        checkCoverageStats(expectedStats, buildCoverage.getDetailedStatistics());
        // todo check that previous detailed stats were deleted
        // todo check values in build coverage summary
    }

    public void testExceptionOnSessionCloseIgnored() throws Exception {
        // setup the the build
        Map customBuildConfiguration = new HashMap();
        String resultsPath = "target/**/*.xml";
        customBuildConfiguration.put(CoverageBuildProcessor.COVERAGE_ENABLED_KEY, "true");
        customBuildConfiguration.put(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY, CoverageBuildProcessor.COBERTURA_PROVIDER_NAME);
        customBuildConfiguration.put(CoverageBuildProcessor.COVERAGE_SCOPE_KEY, StatisticScope.CLASS.toString());
        customBuildConfiguration.put(CoverageBuildProcessor.COVERAGE_RESULTS_XML_PATH_KEY, resultsPath);
        Build build = setupMockBuild(customBuildConfiguration);

        // setup the build results
        Map previousCustomBuildData = new HashMap();
        BuildCoverage previousBuildCoverage = new BuildCoverage(setupPreviousBuildCoverageStats(), CodeCoverageGranularity.LINE);
        new BuildCoverageMapPersister().writeBuildCoverage(previousCustomBuildData, previousBuildCoverage);
        BuildResultsSummary previousBuildResultsSummary = setupMockBuildResultsSummary(previousCustomBuildData);
        BuildResults previousBuildResults = setupMockBuildResults(null, previousBuildResultsSummary, new BuildResultsImpl(), null, null);

        Map customBuildData = new HashMap();
        File baseDirectory = new File("/temp/project");
        BuildResults buildResults = setupMockBuildResults(customBuildData, null, previousBuildResults, baseDirectory, null);

        // setup the dependencies
        CoverageReportParser coverageReportParser = new CoberturaReportParser(StatisticScope.CLASS);
        CoverageProvider coverageProvider = setupMockCoverageProvider(coverageReportParser);
        CoverageProviderLocator coverageProviderLocator = setupMockCoverageProviderLocator(customBuildConfiguration, coverageReportParser, coverageProvider);
        FileVisitorFactory fileVisitorFactory = setupMockFileVisitorFactory(resultsPath, setupCoverageParseResults(), baseDirectory, coverageProvider, StatisticScope.CLASS);
        SessionFactory sessionFactory = setupMockSessionFactory(previousBuildResultsSummary, true, false);

        CoverageBuildProcessor processor = new CoverageBuildProcessor();
        processor.setCoverageProviderLocator(coverageProviderLocator);
        processor.setFileVisitorFactory(fileVisitorFactory);
        processor.setSessionFactory(sessionFactory);
        processor.setCodeCoverageStatisticPersister(new BuildCoverageMapPersister());

        // execute the code being tested
        processor.run(build, buildResults);

        // check the results
        assertTrue("Coverage provider name should be saved to custom build data.", customBuildData.containsKey(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY));
        assertEquals("Incorrect coverage provider name saved to custom build data.", CoverageBuildProcessor.COBERTURA_PROVIDER_NAME, customBuildData.get(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY));

        Set<CodeCoverageStatistic> expectedStats = setupExpectedStats();
        BuildCoverage buildCoverage = new BuildCoverageMapPersister().readBuildCoverage(customBuildData);
        checkCoverageStats(expectedStats, buildCoverage.getDetailedStatistics());
        // todo check that previous detailed stats were deleted
        // todo check values in build coverage summary
    }

    public void testExceptionOnSessionUpdateFailsGracefully() throws Exception {
        // setup the the build
        Map customBuildConfiguration = new HashMap();
        String resultsPath = "target/**/*.xml";
        customBuildConfiguration.put(CoverageBuildProcessor.COVERAGE_ENABLED_KEY, "true");
        customBuildConfiguration.put(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY, CoverageBuildProcessor.COBERTURA_PROVIDER_NAME);
        customBuildConfiguration.put(CoverageBuildProcessor.COVERAGE_SCOPE_KEY, StatisticScope.CLASS.toString());
        customBuildConfiguration.put(CoverageBuildProcessor.COVERAGE_RESULTS_XML_PATH_KEY, resultsPath);
        Build build = setupMockBuild(customBuildConfiguration);

        // setup the build results
        BuildResultsSummary previousBuildResultsSummary = setupMockBuildResultsSummary(null);
        BuildResults previousBuildResults = setupMockBuildResults(null, previousBuildResultsSummary, null, null, null);

        Map customBuildData = new HashMap();
        File baseDirectory = new File("/temp/project");
        BuildResults buildResults = setupMockBuildResults(customBuildData, null, previousBuildResults, baseDirectory, "error while retrieving previous build data");

        // setup the dependencies
        CoverageReportParser coverageReportParser = new CoberturaReportParser(StatisticScope.CLASS);
        CoverageProvider coverageProvider = setupMockCoverageProvider(coverageReportParser);
        CoverageProviderLocator coverageProviderLocator = setupMockCoverageProviderLocator(customBuildConfiguration, coverageReportParser, coverageProvider);
        FileVisitorFactory fileVisitorFactory = setupMockFileVisitorFactory(resultsPath, setupCoverageParseResults(), baseDirectory, coverageProvider, StatisticScope.CLASS);
        SessionFactory sessionFactory = setupMockSessionFactory(previousBuildResultsSummary, false, true);

        CoverageBuildProcessor processor = new CoverageBuildProcessor();
        processor.setCoverageProviderLocator(coverageProviderLocator);
        processor.setFileVisitorFactory(fileVisitorFactory);
        processor.setSessionFactory(sessionFactory);
        processor.setCodeCoverageStatisticPersister(new BuildCoverageMapPersister());

        // execute the code being tested
        processor.run(build, buildResults);

        // check the results
        assertTrue("Coverage provider name should be saved to custom build data.", customBuildData.containsKey(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY));
        assertEquals("Incorrect coverage provider name saved to custom build data.", CoverageBuildProcessor.COBERTURA_PROVIDER_NAME, customBuildData.get(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY));

        BuildCoverage buildCoverage = new BuildCoverageMapPersister().readBuildCoverage(customBuildData);
        assertNull("No stats should have been saved.", buildCoverage);
    }

    private void checkCoverageStats(Collection expectedStats, Collection actualStats) {
        //assertTrue("Some expected stats were not saved.", actualStats.containsAll(expectedStats));
        //assertTrue("Some unexpected stats were saved.", expectedStats.containsAll(actualStats));
        for (Iterator iterator = actualStats.iterator(); iterator.hasNext();) {
            CodeCoverageStatistic actualStat = (CodeCoverageStatistic) iterator.next();
            CodeCoverageStatistic expectedStat = (CodeCoverageStatistic) CollectionUtils.find(expectedStats, new EqualPredicate(actualStat));
            assertNotNull("Unable to find matching expected stat for " + actualStat, expectedStat);
            assertEquals("Coverage delta for " + actualStat + " does not match expected value.", expectedStat.getDeltaFromPreviousBuild(), actualStat.getDeltaFromPreviousBuild(), 0.01);
            assertEquals(actualStat + " is new or not new when it should be the opposite.", expectedStat.isNewStatistic(), actualStat.isNewStatistic());
        }
    }

    private CoverageReportParsingResults setupCoverageParseResults() {
        CoverageReportParsingResults results = new CoverageReportParsingResults();
        CodeCoverageStatistic stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.BLOCK, 358, 1000);
        results.addStatistic(stat);
        stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.LINE, 525, 1000);
        results.addStatistic(stat);
        stat = new CodeCoverageStatistic("com.something.AClass", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 783, 1000);
        results.addStatistic(stat);
        stat = new CodeCoverageStatistic("com.something.NewClass", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 624, 1000);
        results.addStatistic(stat);
        return results;
    }

    private Set<CodeCoverageStatistic> setupPreviousBuildCoverageStats() {
        Set<CodeCoverageStatistic> previousCoverageStats = new HashSet<CodeCoverageStatistic>();
        CodeCoverageStatistic stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.BLOCK, 495, 1000);
        previousCoverageStats.add(stat);
        stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.LINE, 475, 1000);
        previousCoverageStats.add(stat);
        stat = new CodeCoverageStatistic("com.something.AClass", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 758, 1000);
        previousCoverageStats.add(stat);
        stat = new CodeCoverageStatistic("com.something.AnotherClass", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 671, 1000);
        previousCoverageStats.add(stat);
        return previousCoverageStats;
    }

    private Set<CodeCoverageStatistic> setupExpectedStats() {
        Set<CodeCoverageStatistic> expectedStats = new HashSet<CodeCoverageStatistic>();
        CodeCoverageStatistic stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.BLOCK, 358, 1000);
        stat.setNewStatistic(false);
        stat.setDeltaFromPreviousBuild(-13.7);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic(null, StatisticScope.PROJECT, CodeCoverageGranularity.LINE, 525, 1000);
        stat.setNewStatistic(false);
        stat.setDeltaFromPreviousBuild(5.0);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("com.something.AClass", StatisticScope.CLASS, CodeCoverageGranularity.LINE, 783, 1000);
        stat.setNewStatistic(false);
        stat.setDeltaFromPreviousBuild(2.5);
        expectedStats.add(stat);
        stat = new CodeCoverageStatistic("com.something.NewClass", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 624, 1000);
        stat.setNewStatistic(true);
        stat.setDeltaFromPreviousBuild(0.0);
        expectedStats.add(stat);
        return expectedStats;
    }

    private SessionFactory setupMockSessionFactory(BuildResultsSummary previousBuildResultsSummary, boolean throwExceptionOnClose, boolean throwExceptionOnUpdate) {
        Mock mockSession = mock(Session.class);
        Mock mockTransaction = mock(Transaction.class);
        mockSession.expects(once()).method("isOpen").will(returnValue(true));
        mockSession.expects(once()).method("beginTransaction").will(returnValue(mockTransaction.proxy()));
        if (throwExceptionOnUpdate) {
            mockSession.expects(once()).method("update").with(same(previousBuildResultsSummary)).will(throwException(new HibernateException("test update exception")));
        } else {
            mockSession.expects(once()).method("update").with(same(previousBuildResultsSummary));
            mockTransaction.expects(once()).method("commit");
        }
        mockTransaction.expects(once()).method("wasCommitted").will(returnValue(true));
        mockSession.expects(once()).method("isOpen").will(returnValue(true));
        if (throwExceptionOnClose) {
            mockSession.expects(once()).method("close").will(throwException(new HibernateException("test close exception")));
        } else {
            mockSession.expects(once()).method("close");
        }

        Mock mockSessionFactory = mock(SessionFactory.class);
        mockSessionFactory.expects(once()).method("openSession").will(returnValue(mockSession.proxy()));
        return (SessionFactory) mockSessionFactory.proxy();
    }    

    private FileVisitorFactory setupMockFileVisitorFactory(String resultsPath, CoverageReportParsingResults parsingResults, File baseDirectory,
                                                           CoverageProvider coverageProvider, StatisticScope scopeLimit) {
        Mock mockCoverageFileVisitor = mock(CoverageFileVisitor.class);
        mockCoverageFileVisitor.expects(once()).method("visitFilesThatMatch").with(eq(resultsPath));
        mockCoverageFileVisitor.expects(once()).method("getCoverageParsingResults").will(returnValue(parsingResults));
        CoverageFileVisitor coverageFileVisitor = (CoverageFileVisitor) mockCoverageFileVisitor.proxy();

        Mock mockFileVisitorFactory = mock(FileVisitorFactory.class);
        mockFileVisitorFactory.expects(once()).method("newCoverageFileVisitor")
                .with(same(baseDirectory), same(coverageProvider), eq(scopeLimit)).will(returnValue(coverageFileVisitor));
        return (FileVisitorFactory) mockFileVisitorFactory.proxy();
    }

    private CoverageProviderLocator setupMockCoverageProviderLocator(Map customBuildConfiguration, CoverageReportParser coverageReportParser,
                                                                     CoverageProvider coverageProvider) {
        Mock mockCoverageProviderLocator = mock(CoverageProviderLocator.class);
        mockCoverageProviderLocator.expects(once()).method("findCoverageProvider").with(eq(customBuildConfiguration))
                .will(returnValue(coverageProvider));
        return (CoverageProviderLocator) mockCoverageProviderLocator.proxy();
    }

    private CoverageProvider setupMockCoverageProvider(CoverageReportParser coverageReportParser) {
        Mock mockCoverageProvider = mock(CoverageProvider.class);
        return (CoverageProvider) mockCoverageProvider.proxy();
    }

    private BuildResults setupMockBuildResults(Map customBuildData, BuildResultsSummary buildResultsSummary, BuildResults previousBuildResults,
                                               File baseDirectory, String errorMessageFragment) {
        Mock mockBuildResults = mock(BuildResults.class);
        if (customBuildData != null) {
            mockBuildResults.expects(atLeastOnce()).method("getCustomBuildData").will(returnValue(customBuildData));
        }
        if (baseDirectory != null) {
            mockBuildResults.expects(once()).method("getSourceDirectory").will(returnValue(baseDirectory));
        }
        if (buildResultsSummary != null) {
            mockBuildResults.expects(once()).method("getBuildResultsSummary").will(returnValue(buildResultsSummary));
        }
        if (previousBuildResults != null) {
            mockBuildResults.expects(once()).method("getPreviousBuildResults").will(returnValue(previousBuildResults));
        }
        if (errorMessageFragment != null) {
            mockBuildResults.expects(once()).method("addErrorMessage").with(stringContains(errorMessageFragment));
        }
        return (BuildResults) mockBuildResults.proxy();
    }
    
    private BuildResultsSummary setupMockBuildResultsSummary(Map customBuildData) {
        Mock mockBuildResultsSummary = mock(BuildResultsSummary.class);
        if (customBuildData != null) {
            mockBuildResultsSummary.expects(atLeastOnce()).method("getCustomBuildData").will(returnValue(customBuildData));
        }
        return (BuildResultsSummary) mockBuildResultsSummary.proxy();
    }

    private Build setupMockBuild(Map customBuildConfiguration) {
        Mock mockBuildDefinition = mock(BuildDefinition.class);
        mockBuildDefinition.expects(once()).method("getCustomConfiguration").will(returnValue(customBuildConfiguration));
        BuildDefinition buildDefinition = (BuildDefinition) mockBuildDefinition.proxy();

        Mock mockBuild = mock(Build.class);
        mockBuild.expects(once()).method("getBuildDefinition").will(returnValue(buildDefinition));
        return (Build) mockBuild.proxy();
    }
}
