package com.atlassian.bamboo.plugins.coverage;

import junit.framework.TestCase;

import java.util.Set;
import java.util.HashSet;

public class CoverageReportParsingResultsTest extends TestCase {
    public void testHasErrors() throws Exception {
        CoverageReportParsingResults results = new CoverageReportParsingResults();
        assertFalse("Results should not have errors.", results.hasErrors());
        results.addErrorMessage("Some error.");
        assertTrue("Results should have errors.", results.hasErrors());
    }

    public void testMergeResultsRetainsAllErrors() throws Exception {
        Set expectedErrors = new HashSet();
        CoverageReportParsingResults results = new CoverageReportParsingResults();
        String error = "Some error.";
        results.addErrorMessage(error);
        expectedErrors.add(error);

        CoverageReportParsingResults otherResults = new CoverageReportParsingResults();
        error = "Another error.";
        otherResults.addErrorMessage(error);
        expectedErrors.add(error);

        results.mergeResults(otherResults);
        assertEquals("Incorrect number of errors in results.", 2, results.getErrorMessages().size());
        assertTrue(results.getErrorMessages().containsAll(expectedErrors));
    }

    public void testMergeResultsEliminatesDuplicateErrors() throws Exception {
        Set expectedErrors = new HashSet();
        CoverageReportParsingResults results = new CoverageReportParsingResults();
        String error = "Some error.";
        results.addErrorMessage(error);
        expectedErrors.add(error);

        CoverageReportParsingResults otherResults = new CoverageReportParsingResults();
        error = "Another error.";
        otherResults.addErrorMessage(error);
        otherResults.addErrorMessage("Some error.");
        expectedErrors.add(error);

        results.mergeResults(otherResults);
        assertEquals("Incorrect number of errors in results.", 2, results.getErrorMessages().size());
        assertTrue(results.getErrorMessages().containsAll(expectedErrors));
    }

    public void testMergeResultsRetainsAllStats() throws Exception {
        Set expectedStats = new HashSet();
        CoverageReportParsingResults results = new CoverageReportParsingResults();
        CodeCoverageStatistic stat = new CodeCoverageStatistic("com.something.ClassOne", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 10, 100);
        results.addStatistic(stat);
        expectedStats.add(stat);

        CoverageReportParsingResults otherResults = new CoverageReportParsingResults();
        stat = new CodeCoverageStatistic("com.something.ClassTwo", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 234, 1000);
        otherResults.addStatistic(stat);
        expectedStats.add(stat);

        results.mergeResults(otherResults);
        assertEquals("Incorrect number of statistics in results.", 2, results.getCoverageStats().size());
        results.getCoverageStats().containsAll(expectedStats);
    }

    public void testMergeResultsMergesMatchingStats() throws Exception {
        CoverageReportParsingResults results = new CoverageReportParsingResults();
        results.addStatistic(new CodeCoverageStatistic("com.something.ClassOne", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 10, 100));
        results.addStatistic(new CodeCoverageStatistic("com.something.ClassTwo", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 4, 40));

        CoverageReportParsingResults otherResults = new CoverageReportParsingResults();
        otherResults.addStatistic(new CodeCoverageStatistic("com.something.ClassOne", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 5, 50));

        results.mergeResults(otherResults);
        assertEquals(2, results.getCoverageStats().size());
        for (CodeCoverageStatistic stat : results.getCoverageStats()) {
            if (stat.getName().equals("com.something.ClassOne")) {
                assertEquals(15, stat.getCoveredCount());
                assertEquals(150, stat.getTotalCount());
            } else {
                assertEquals(4, stat.getCoveredCount());
                assertEquals(40, stat.getTotalCount());
            }
        }
    }

    public void testAddStat() throws Exception {
        CoverageReportParsingResults results = new CoverageReportParsingResults();
        results.addStatistic(new CodeCoverageStatistic("com.something.ClassOne", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 10, 100));
        assertEquals(1, results.getCoverageStats().size());
        CodeCoverageStatistic stat = results.getCoverageStats().iterator().next();
        assertEquals(10, stat.getCoveredCount());
        assertEquals(100, stat.getTotalCount());
    }

    public void testNewStatMergedIfMatchesExisting() throws Exception {
        CoverageReportParsingResults results = new CoverageReportParsingResults();
        results.addStatistic(new CodeCoverageStatistic("com.something.ClassOne", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 10, 100));
        results.addStatistic(new CodeCoverageStatistic("com.something.ClassOne", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 5, 50));
        results.addStatistic(new CodeCoverageStatistic("com.something.ClassTwo", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 4, 40));
        assertEquals(2, results.getCoverageStats().size());
        for (CodeCoverageStatistic stat : results.getCoverageStats()) {
            if (stat.getName().equals("com.something.ClassOne")) {
                assertEquals(15, stat.getCoveredCount());
                assertEquals(150, stat.getTotalCount());
            } else {
                assertEquals(4, stat.getCoveredCount());
                assertEquals(40, stat.getTotalCount());
            }
        }
    }

    public void testMultipleStatsAdded() throws Exception {
        Set<CodeCoverageStatistic> stats = new HashSet<CodeCoverageStatistic>();
        stats.add(new CodeCoverageStatistic("com.something.ClassOne", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 10, 100));
        stats.add(new CodeCoverageStatistic("com.something.ClassTwo", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 4, 40));
        CoverageReportParsingResults results = new CoverageReportParsingResults();
        results.addStatistics(stats);
        assertEquals(2, results.getCoverageStats().size());
        for (CodeCoverageStatistic stat : results.getCoverageStats()) {
            if (stat.getName().equals("com.something.ClassOne")) {
                assertEquals(10, stat.getCoveredCount());
                assertEquals(100, stat.getTotalCount());
            } else {
                assertEquals(4, stat.getCoveredCount());
                assertEquals(40, stat.getTotalCount());
            }
        }
    }

    public void testAddMultiplesMergesMatches() throws Exception {
        Set<CodeCoverageStatistic> stats = new HashSet<CodeCoverageStatistic>();
        stats.add(new CodeCoverageStatistic("com.something.ClassOne", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 10, 100));
        stats.add(new CodeCoverageStatistic("com.something.ClassTwo", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 4, 40));
        CoverageReportParsingResults results = new CoverageReportParsingResults();
        results.addStatistic(new CodeCoverageStatistic("com.something.ClassOne", StatisticScope.CLASS, CodeCoverageGranularity.BLOCK, 5, 50));
        results.addStatistics(stats);
        assertEquals(2, results.getCoverageStats().size());
        for (CodeCoverageStatistic stat : results.getCoverageStats()) {
            if (stat.getName().equals("com.something.ClassOne")) {
                assertEquals(15, stat.getCoveredCount());
                assertEquals(150, stat.getTotalCount());
            } else {
                assertEquals(4, stat.getCoveredCount());
                assertEquals(40, stat.getTotalCount());
            }
        }
    }
}
