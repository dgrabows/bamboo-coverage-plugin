<html>
<head>
	<title> [@ui.header pageKey='Code Coverage Summary' object='${build.name}' title=true /]</title>
</head>

<body>
    [@ui.header pageKey='Code Coverage Summary' object='${build.name}' /]
    [@cp.buildSubMenu selectedTab='CodeCoverage' /]

    [#if buildCoverageSummary?exists]
    [#if buildsSinceDecrease?exists]
        <div id="successRate">
            <div id="successRatePercentage">
                <h2> ${buildCoverageSummary.projectCoveragePercent?string('0.0')}% </h2>
                <p> coverage </p>
            </div>
            <dl>
                <dt class="first"> Last Build +/-: </dt>
                <dd class="first"> ${buildCoverageSummary.projectCoverageDelta?string('+0.0;-0.0')}% </dd>
                <dt> Builds Since Decrease: </dt>
                <dd> ${buildsSinceDecrease} </dd>
            </dl>
        </div>
    [/#if]
    [/#if]

    <div id="graphs" class="topPadded">
        <div id="graph1">
            [@ww.action name="viewCodeCoverageGraph" namespace="/build" executeResult="true" /]
        </div>
        <div id="graph2">
            [@ww.action name="viewCoverageDeltaByAuthor" namespace="/build" executeResult="true" /]
        </div>
    </div>
</body>
</html>