<html>
<head>
	<title> [@ui.header pageKey='Coverage' object='${build.name} ${buildResults.buildNumber}' title=true /]</title>
</head>

<body>
    [@cp.resultsSubMenu selectedTab='CodeCoverage' /]
    [#if !buildCoverageSummary?exists]
        <div class="section">
            No coverage data was loaded for this build.
        </div>
    [#else]
        <div class="section">
            <h2>Class Coverage Changes</h2>

            [@ui.bambooInfoDisplay titleKey='Classes Moving Up' float=true]
                <table class="grid">
                    [#if buildCoverageSummary.positiveClassStatistics?has_content]
                        <tr>
                            <th>Class</th>
                            <th>Coverage</th>
                            <th>Change</th>
                        </tr>
                        [#list buildCoverageSummary.positiveClassStatistics as coverageStat]
                            [#assign opacity = 50 + (coverageStat.deltaFromPreviousBuild / 2)?int]
                            [#assign cellStyle = "background-color: #393; color: #fff; filter:alpha(opacity=" + opacity + "); opacity: " + opacity / 100.0 + "; -moz-opacity:" + ((opacity / 100.0) - 0.001) + ";"]
                            <tr>
                                <td style="${cellStyle}" title="${coverageStat.name}">${coverageStat.name?split(".")?last}</td>
                                <td style="${cellStyle}">${coverageStat.percent?string('0.0')}%</td>
                                <td style="${cellStyle}">${coverageStat.deltaFromPreviousBuild?string('0.0')}%</td>
                            </tr>
                        [/#list]
                    [#else]
                        <tr>
                            <td>No classes increased coverage for this build.</td>
                        </tr>
                    [/#if]
                </table>
            [/@ui.bambooInfoDisplay]

            [@ui.bambooInfoDisplay titleKey='Classes Moving Down' float=true]
                <table class="grid">
                    [#if buildCoverageSummary.negativeClassStatistics?has_content]
                        <tr>
                            <th>Class</th>
                            <th>Coverage</th>
                            <th>Change</th>
                        </tr>
                        [#list buildCoverageSummary.negativeClassStatistics as coverageStat]
                            [#assign opacity = 50 + (coverageStat.deltaFromPreviousBuild / -2)?int]
                            [#assign cellStyle = "background-color: #c33; color: #fff; filter:alpha(opacity=" + opacity + "); opacity: " + opacity / 100.0 + "; -moz-opacity:" + ((opacity / 100.0) - 0.001) + ";"]
                            <tr>
                                <td style="${cellStyle}" title="${coverageStat.name}">${coverageStat.name?split(".")?last}</td>
                                <td style="${cellStyle}">${coverageStat.percent?string('0.0')}%</td>
                                <td style="${cellStyle}">${coverageStat.deltaFromPreviousBuild?string('0.0')}%</td>
                            </tr>
                        [/#list]
                    [#else]
                        <tr>
                            <td>No classes decreased coverage for this build.</td>
                        </tr>
                    [/#if]
                </table>
            [/@ui.bambooInfoDisplay]
        </div>

        <div class="section">
            <h2>Package Coverage Changes</h2>

            [@ui.bambooInfoDisplay titleKey='Packages Moving Up' float=true]
                <table class="grid">
                    [#if buildCoverageSummary.positivePackageStatistics?has_content]
                        <tr>
                            <th>Package</th>
                            <th>Coverage</th>
                            <th>Change</th>
                        </tr>
                        [#list buildCoverageSummary.positivePackageStatistics as coverageStat]
                            [#assign opacity = 50 + (coverageStat.deltaFromPreviousBuild / 2)?int]
                            [#assign cellStyle = "background-color: #393; color: #fff; filter:alpha(opacity=" + opacity + "); opacity: " + opacity / 100.0 + "; -moz-opacity:" + ((opacity / 100.0) - 0.001) + ";"]
                            <tr>
                                <td style="${cellStyle}">${coverageStat.name?replace(".", " . ")}</td>
                                <td style="${cellStyle}">${coverageStat.percent?string('0.0')}%</td>
                                <td style="${cellStyle}">${coverageStat.deltaFromPreviousBuild?string('0.0')}%</td>
                            </tr>
                        [/#list]
                    [#else]
                        <tr>
                            <td>No packages increased coverage for this build.</td>
                        </tr>
                    [/#if]
                </table>
            [/@ui.bambooInfoDisplay]

            [@ui.bambooInfoDisplay titleKey='Packages Moving Down' float=true]
                <table class="grid">
                    [#if buildCoverageSummary.negativePackageStatistics?has_content]
                        <tr>
                            <th>Package</th>
                            <th>Coverage</th>
                            <th>Change</th>
                        </tr>
                        [#list buildCoverageSummary.negativePackageStatistics as coverageStat]
                            [#assign opacity = 50 + (coverageStat.deltaFromPreviousBuild / -2)?int]
                            [#assign cellStyle = "background-color: #c33; color: #fff; filter:alpha(opacity=" + opacity + "); opacity: " + opacity / 100.0 + "; -moz-opacity:" + ((opacity / 100.0) - 0.001) + ";"]
                            <tr>
                                <td style="${cellStyle}">${coverageStat.name?replace(".", " . ")}</td>
                                <td style="${cellStyle}">${coverageStat.percent?string('0.0')}%</td>
                                <td style="${cellStyle}">${coverageStat.deltaFromPreviousBuild?string('0.0')}%</td>
                            </tr>
                        [/#list]
                    [#else]
                        <tr>
                            <td>No packages decreased coverage for this build.</td>
                        </tr>
                    [/#if]
                </table>
            [/@ui.bambooInfoDisplay]
        </div>

        <div class="section">
            [#if buildCoverageSummary.newClassStatisticsLimited]
                <h2>New Classes (list was limited to ${buildCoverageSummary.maxNewClasses})</h2>
            [#else]
                <h2>New Classes</h2>
            [/#if]

            [@ui.bambooInfoDisplay titleKey='New Classes Since Last Build' float=true]
                <table class="grid">
                    [#if buildCoverageSummary.newClassStatistics?has_content]
                        <tr>
                            <th>Class</th>
                            <th>Coverage</th>
                        </tr>
                        [#list buildCoverageSummary.newClassStatistics as coverageStat]
                            <tr>
                                <td title="${coverageStat.name}">${coverageStat.name?split(".")?last}</td>
                                <td>${coverageStat.percent?string('0.0')}%</td>
                            </tr>
                        [/#list]
                    [#else]
                        <tr>
                            <td>No new classes for this build.</td>
                        </tr>
                    [/#if]
                </table>
            [/@ui.bambooInfoDisplay]
        </div>
    [/#if]
</body>
</html>