package com.atlassian.bamboo.plugins.coverage;

import java.io.File;

public interface FileVisitorFactory {
    CoverageFileVisitor newCoverageFileVisitor(File baseDirectory, CoverageProvider coverageProvider, StatisticScope scopeLimit);
}
