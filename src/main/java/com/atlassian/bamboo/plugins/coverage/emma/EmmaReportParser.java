package com.atlassian.bamboo.plugins.coverage.emma;

import com.atlassian.bamboo.plugins.coverage.*;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.net.MalformedURLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Obtains code coverage statistics from an EMMA results XML file.
 */
public class EmmaReportParser extends BaseReportParser implements CoverageReportParser {

    public EmmaReportParser(StatisticScope scopeLimit) {
        this.scopeLimit = scopeLimit;
    }

    public CoverageReportParsingResults parse(File file) throws MalformedURLException, DocumentException
    {
        SAXReader reader = configureReader();
        Document emmaReport = reader.read(file);

        CoverageReportParsingResults results = new CoverageReportParsingResults();

        if (!isValidEmmaResultFile(emmaReport)) {
            results.addErrorMessage("The coverage plugin skipped parsing the file " + file.getName() +
                    " because it does not appear to be a valid EMMA coverage report file.");
            return results;
        }

        results.addStatistics(buildProjectCoverageStats(emmaReport));
        if (StatisticScope.CLASS == scopeLimit || StatisticScope.PACKAGE == scopeLimit) {
            results.addStatistics(buildPackageCoverageStats(emmaReport));
        }
        if (StatisticScope.CLASS == scopeLimit) {
            results.addStatistics(buildClassCoverageStats(emmaReport));
        }

        return results;
    }

    private Set<CodeCoverageStatistic> buildProjectCoverageStats(Document emmaReport) {
        Node projectNode = emmaReport.selectSingleNode("/report/data/all");
        return buildStatisticsForNode(projectNode, null, StatisticScope.PROJECT);
    }

    private Set<CodeCoverageStatistic> buildPackageCoverageStats(Document emmaReport) {
        Set<CodeCoverageStatistic> packageStats = new HashSet<CodeCoverageStatistic>();
        List packageNodes = emmaReport.selectNodes("/report/data/all/package");
        for (Iterator iterator = packageNodes.iterator(); iterator.hasNext();) {
            Node packageNode = (Node) iterator.next();
            packageStats.addAll(buildStatisticsForNode(packageNode, packageNode.valueOf("@name"), StatisticScope.PACKAGE));
        }
        return packageStats;
    }

    private Set<CodeCoverageStatistic> buildClassCoverageStats(Document emmaReport) {
        Set<CodeCoverageStatistic> classStats = new HashSet<CodeCoverageStatistic>();
        List classNodes = emmaReport.selectNodes("/report/data/all/package//class");
        for (Iterator iterator = classNodes.iterator(); iterator.hasNext();) {
            Node classNode = (Node) iterator.next();
            String packageName = classNode.valueOf("../../@name");
            String qualifiedClassName = (packageName != null ? packageName + "." : "") + classNode.valueOf("@name");
            classStats.addAll(buildStatisticsForNode(classNode, qualifiedClassName, StatisticScope.CLASS));
        }
        return classStats;
    }

    private Set<CodeCoverageStatistic> buildStatisticsForNode(Node node, String name, StatisticScope scope) {
        Set<CodeCoverageStatistic> stats = new HashSet<CodeCoverageStatistic>();
        List coverageNodes = node.selectNodes("coverage");
        for (Iterator iterator = coverageNodes.iterator(); iterator.hasNext();) {
            Node coverageNode = (Node) iterator.next();
            CodeCoverageStatistic stat = buildCoverageStatistic(coverageNode, name, scope);
            if (stat != null) {
                stats.add(stat);
            }
        }
        return stats;
    }

    private CodeCoverageStatistic buildCoverageStatistic(Node coverageNode, String name, StatisticScope scope) {
        String coverageValue = coverageNode.valueOf("@value");
        if (coverageValue != null) {
            Matcher matcher = COVERAGE_VALUE_PATTERN.matcher(coverageValue);
            if (matcher.matches()) {
                Double covered = Double.valueOf(matcher.group(1));
                Double total = Double.valueOf(matcher.group(2));

                String coverageType = coverageNode.valueOf("@type");
                if (coverageType != null) {
                    CodeCoverageGranularity granularity = determineGranularity(coverageType);
                    if (granularity != null) {
                        return new CodeCoverageStatistic(name, scope, granularity, covered.longValue(), total.longValue());
                    }
                }
            }
        }
        return null;
    }

    private CodeCoverageGranularity determineGranularity(String coverageType) {
        CodeCoverageGranularity granularity = null;
        if (coverageType != null) {
            if (coverageType.startsWith("class")) {
                granularity = CodeCoverageGranularity.CLASS;
            } else if (coverageType.startsWith("method")) {
                granularity = CodeCoverageGranularity.METHOD;
            } else if (coverageType.startsWith("block")) {
                granularity = CodeCoverageGranularity.BLOCK;
            } else if (coverageType.startsWith("line")) {
                granularity = CodeCoverageGranularity.LINE;
            }
        }
        return granularity;
    }

    private boolean isValidEmmaResultFile(Document document) {
        return document.selectSingleNode("/report") != null &&
                document.selectSingleNode("/report/stats") != null &&
                document.selectSingleNode("/report/data") != null;
    }

    private static final Pattern COVERAGE_VALUE_PATTERN = Pattern.compile(".*\\((\\d+\\.?\\d*)/(\\d+\\.?\\d*)\\)");

    private StatisticScope scopeLimit;
}
