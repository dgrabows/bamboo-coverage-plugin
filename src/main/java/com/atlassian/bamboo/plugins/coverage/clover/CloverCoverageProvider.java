package com.atlassian.bamboo.plugins.coverage.clover;

import com.atlassian.bamboo.plugins.coverage.CoverageProvider;
import com.atlassian.bamboo.plugins.coverage.CoverageReportParser;
import com.atlassian.bamboo.plugins.coverage.StatisticScope;
import com.atlassian.bamboo.plugins.coverage.CodeCoverageGranularity;

/**
 * Implements support for Clover code coverage tool.
 */
public class CloverCoverageProvider implements CoverageProvider {

    public CoverageReportParser getCoverageReportParser(StatisticScope coverageScopeLimit) {
        return new CloverReportParser(coverageScopeLimit);
    }

    public StatisticScope[] getSupportedScopes() {
        return SUPPORTED_SCOPES;
    }

    public CodeCoverageGranularity[] getSupportedGranularities() {
        return SUPPORTED_GRANULARITIES;
    }


    public CodeCoverageGranularity getPreferredGranularity() {
        return CodeCoverageGranularity.ELEMENT;
    }

    private static final StatisticScope[] SUPPORTED_SCOPES =
            new StatisticScope[] {StatisticScope.PROJECT, StatisticScope.PACKAGE, StatisticScope.CLASS};
    private static final CodeCoverageGranularity[] SUPPORTED_GRANULARITIES =
            new CodeCoverageGranularity[] {CodeCoverageGranularity.ELEMENT, CodeCoverageGranularity.METHOD,
                    CodeCoverageGranularity.CONDITIONAL, CodeCoverageGranularity.STATEMENT};
}
