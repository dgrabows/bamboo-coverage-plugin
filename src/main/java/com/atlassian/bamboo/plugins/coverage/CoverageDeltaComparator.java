package com.atlassian.bamboo.plugins.coverage;

import java.util.Comparator;

/**
 * Comparator for sorting {@link CodeCoverageStatistic} instances based on the natural order of their coverage delta
 * properties.
 */
public class CoverageDeltaComparator implements Comparator {
    public int compare(Object o1, Object o2) {
        CodeCoverageStatistic stat1 = (CodeCoverageStatistic) o1;
        CodeCoverageStatistic stat2 = (CodeCoverageStatistic) o2;

        return Double.compare(stat1.getDeltaFromPreviousBuild(), stat2.getDeltaFromPreviousBuild());
    }
}
