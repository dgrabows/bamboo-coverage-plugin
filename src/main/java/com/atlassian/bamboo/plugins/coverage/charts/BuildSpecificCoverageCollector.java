package com.atlassian.bamboo.plugins.coverage.charts;

import com.atlassian.bamboo.charts.collater.TimePeriodCollater;
import com.atlassian.bamboo.plugins.coverage.CodeCoverageGranularity;
import com.atlassian.bamboo.plugins.coverage.CoverageProvider;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;

import java.util.Iterator;

/**
 * Generates multi-series data set containing code coverage data over time for a project. When used to generate
 * cross-project reports, a separate series will be created for each build.
 */
public class BuildSpecificCoverageCollector extends AbstractMultiSeriesTimePeriodCollector {

    public BuildSpecificCoverageCollector() {
        super();
        includeBuildInSeriesKey = true;
    }

    protected String[] getSeriesKeys(BuildResultsSummary summary) {
        CoverageProvider coverageProvider = searchResultsForCoverageProvider();
        if (coverageProvider != null) {
            return new String[] {coverageProvider.getPreferredGranularity().toString()};
        } else {
            return new String[] {CodeCoverageGranularity.LINE.toString()};
        }
    }

    protected TimePeriodCollater getCollater() {
        return new TimePeriodCoverageCollater();
    }

    private CoverageProvider searchResultsForCoverageProvider() {
        CoverageProvider coverageProvider = null;
        Iterator results = getResultsList().iterator();
        while (results.hasNext() && coverageProvider == null) {
            BuildResultsSummary result = (BuildResultsSummary) results.next();
            coverageProvider = getCoverageProviderLocator().findCoverageProvider(result.getCustomBuildData());
        }
        return coverageProvider;
    }
}
