package com.atlassian.bamboo.plugins.coverage;

import org.dom4j.io.SAXReader;
import org.xml.sax.SAXException;

/**
 * Common base class for coverage report parser implementations.
 */
public abstract class BaseReportParser {
    /**
     * Gets a SAX reader that ignores DTD declarations, since resolving these may require internet connectivity and
     * and that the DTD actually exist at the given URL.
     *
     * @return SAX reader configured to ignore DTDs 
     */
    protected SAXReader configureReader() {
        SAXReader reader = null;
        try {
            reader = new SAXReader("org.apache.xerces.parsers.SAXParser");
            reader.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        } catch (SAXException e) {
            // Can't find Xerces implementation, so take whatever the system default is. Loading DTDs won't be disabled,
            // but it has a better chance of working that not parsing the file at all.
            reader = new SAXReader();
        }
        return reader;
    }
}
