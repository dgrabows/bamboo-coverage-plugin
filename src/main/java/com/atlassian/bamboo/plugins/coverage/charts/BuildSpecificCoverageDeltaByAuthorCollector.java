package com.atlassian.bamboo.plugins.coverage.charts;

import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.charts.collater.TimePeriodCollater;
import com.atlassian.bamboo.plugins.coverage.CoverageProvider;
import com.atlassian.bamboo.plugins.coverage.StatisticScope;
import com.atlassian.bamboo.plugins.coverage.CodeCoverageGranularity;

import java.util.Iterator;

public class BuildSpecificCoverageDeltaByAuthorCollector extends AbstractMultiSeriesTimePeriodCollector {
    public BuildSpecificCoverageDeltaByAuthorCollector() {
        includeBuildInSeriesKey = true;
    }

    protected String[] getSeriesKeys(BuildResultsSummary summary) {
        String[] authors = null;
        if (summary.getChangedByAuthors() != null && summary.getChangedByAuthors().length() > 0) {
            authors = TimePeriodCoverageDeltaCollater.parseAuthors(summary.getChangedByAuthors());
        }
        return authors;
    }

    protected TimePeriodCollater getCollater() {
        return new TimePeriodCoverageDeltaCollater();
    }
}
