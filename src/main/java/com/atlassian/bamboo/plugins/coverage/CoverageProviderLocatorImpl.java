package com.atlassian.bamboo.plugins.coverage;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.Map;

import com.atlassian.bamboo.plugins.coverage.emma.EmmaCoverageProvider;
import com.atlassian.bamboo.plugins.coverage.clover.CloverCoverageProvider;
import com.atlassian.bamboo.plugins.coverage.cobertura.CoberturaCoverageProvider;

/**
 * Finds the appropriate coverage provider implementation.
 */
public class CoverageProviderLocatorImpl implements CoverageProviderLocator {

    public CoverageProviderLocatorImpl() {
    }

    public CoverageProvider findCoverageProvider(Map buildConfiguration) {
        String providerName = (String) buildConfiguration.get(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY);
        if (StringUtils.isNotBlank(providerName)) {
            if (CoverageBuildProcessor.EMMA_PROVIDER_NAME.equals(providerName)) {
                return new EmmaCoverageProvider();
            } else if (CoverageBuildProcessor.CLOVER_PROVIDER_NAME.equals(providerName)) {
                return new CloverCoverageProvider();
            } else if (CoverageBuildProcessor.COBERTURA_PROVIDER_NAME.equals(providerName)) {
                return new CoberturaCoverageProvider();
            } else {
                log.error("The code coverage plugin was configured to use a coverage provider with the name " +
                        providerName + ", but no provider could be located for that name. Valid coverage provider " +
                        "names are " + CoverageBuildProcessor.CLOVER_PROVIDER_NAME + ", " + CoverageBuildProcessor.COBERTURA_PROVIDER_NAME + ", and " +
                        CoverageBuildProcessor.EMMA_PROVIDER_NAME + ".");
            }
        }
        return null;
    }

    private static final Logger log = Logger.getLogger(CoverageProviderLocatorImpl.class);
}
