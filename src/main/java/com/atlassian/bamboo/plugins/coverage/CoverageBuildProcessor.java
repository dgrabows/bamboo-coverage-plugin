package com.atlassian.bamboo.plugins.coverage;

import com.atlassian.bamboo.build.Build;
import com.atlassian.bamboo.build.CustomBuildProcessor;
import com.atlassian.bamboo.results.BuildResults;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.utils.error.SimpleErrorCollection;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.atlassian.spring.container.ContainerManager;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.Session;
import net.sf.hibernate.SessionFactory;
import net.sf.hibernate.Transaction;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.Iterator;
import java.util.Map;

/**
 * Reads in code coverage statistics from a report and puts them into the custom build data.
 */
public class CoverageBuildProcessor implements CustomBuildProcessor {
    public static final String COVERAGE_RESULTS_XML_PATH_KEY = "custom.coverage.path";
    public static final String COVERAGE_ENABLED_KEY = "custom.coverage.enabled";
    public static final String COVERAGE_PROVIDER_KEY = "custom.coverage.provider";
    public static final String COVERAGE_SCOPE_KEY = "custom.coverage.scope";

    public static final String EMMA_PROVIDER_NAME = "emma";
    public static final String CLOVER_PROVIDER_NAME = "clover";
    public static final String COBERTURA_PROVIDER_NAME = "cobertura";

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void setCoverageProviderLocator(CoverageProviderLocator coverageProviderLocator) {
        this.coverageProviderLocator = coverageProviderLocator;
    }

    public void setCodeCoverageStatisticPersister(BuildCoveragePersister buildCoveragePersister) {
        this.buildCoveragePersister = buildCoveragePersister;
    }

    public void setFileVisitorFactory(FileVisitorFactory fileVisitorFactory) {
        this.fileVisitorFactory = fileVisitorFactory;
    }

    public void run(Build build, final BuildResults buildResult) {
        try {
            BuildCoverage buildCoverage = null;
            Map customConfiguration = build.getBuildDefinition().getCustomConfiguration();
            if (customConfiguration != null && StringUtils.isNotBlank((String) customConfiguration.get(COVERAGE_ENABLED_KEY)) &&
                    customConfiguration.get(COVERAGE_ENABLED_KEY).equals("true")) {
                buildResult.getCustomBuildData().put(COVERAGE_PROVIDER_KEY, customConfiguration.get(COVERAGE_PROVIDER_KEY));
                CoverageProvider coverageProvider = getCoverageProviderLocator().findCoverageProvider(customConfiguration);
                if (coverageProvider != null) {
                    StatisticScope scopeLimit = StatisticScope.fromString((String) customConfiguration.get(COVERAGE_SCOPE_KEY));
                    CoverageFileVisitor fileVisitor = getFileVisitorFactory().newCoverageFileVisitor(buildResult.getSourceDirectory(), coverageProvider, scopeLimit);
                    String pathPattern = (String) customConfiguration.get(COVERAGE_RESULTS_XML_PATH_KEY);
                    fileVisitor.visitFilesThatMatch(pathPattern);
                    CoverageReportParsingResults parsingResults = fileVisitor.getCoverageParsingResults();
                    addParsingErrorsToBuildResult(parsingResults, buildResult);
                    buildCoverage = parsingResults.extractBuildCoverage();

                    BuildCoverage previousBuildCoverage = findPreviousBuildCoverage(buildResult);
                    if (previousBuildCoverage != null) {
                        buildCoverage.populateChangeFromPreviousBuild(previousBuildCoverage);
                    }
                }
            }

            if (buildCoverage != null) {
                getBuildCoveragePersister().writeBuildCoverage(buildResult.getCustomBuildData(), buildCoverage);
            }
        } catch (HibernateException e) {
            buildResult.addErrorMessage("The coverage plugin was unable to process code coverage data due to an " +
                    "error while retrieving previous build data." + (e.getMessage() != null ? " " + e.getMessage() : ""));            
        }
    }

    public ErrorCollection validate(BuildConfiguration configuration) {
        ErrorCollection ec = new SimpleErrorCollection();
        if (configuration.getBoolean(COVERAGE_ENABLED_KEY)) {
            if (StringUtils.isBlank(configuration.getString(COVERAGE_PROVIDER_KEY))) {
                ec.addError(COVERAGE_PROVIDER_KEY, "Please specify the code coverage provider.");
            }
            if (StringUtils.isBlank(configuration.getString(COVERAGE_RESULTS_XML_PATH_KEY))) {
                ec.addError(COVERAGE_RESULTS_XML_PATH_KEY, "Please specify the directory containing the code coverage XML report file.");
            }
            if (StringUtils.isBlank(configuration.getString(COVERAGE_SCOPE_KEY))) {
                ec.addError(COVERAGE_SCOPE_KEY, "Please specify the code coverage scope.");
            }
        }
        return ec;
    }

    protected SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            sessionFactory = (SessionFactory) ContainerManager.getComponent("sessionFactory");
        }
        return sessionFactory;
    }

    protected CoverageProviderLocator getCoverageProviderLocator() {
        if (coverageProviderLocator == null) {
            coverageProviderLocator = new CoverageProviderLocatorImpl();
        }
        return coverageProviderLocator;
    }


    protected BuildCoveragePersister getBuildCoveragePersister() {
        if (buildCoveragePersister == null) {
            buildCoveragePersister = new BuildCoverageMapPersister();
        }
        return buildCoveragePersister;
    }

    protected FileVisitorFactory getFileVisitorFactory() {
        if (fileVisitorFactory == null) {
            fileVisitorFactory = new FileVisitorFactoryImpl();
        }
        return fileVisitorFactory;
    }

    private void addParsingErrorsToBuildResult(CoverageReportParsingResults results, BuildResults buildResult) {
        if (results.hasErrors()) {
            for (Iterator errorMessages = results.getErrorMessages().iterator(); errorMessages.hasNext();)
            {
                buildResult.addErrorMessage((String) errorMessages.next());
            }
        }
    }

    private BuildCoverage findPreviousBuildCoverage(BuildResults buildResult) throws HibernateException {
        BuildCoverage previousBuildCoverage = null;
        BuildResults previousBuildResults = buildResult.getPreviousBuildResults();
        while (previousBuildResults != null && previousBuildCoverage == null) {
            BuildResultsSummary previousSummary = previousBuildResults.getBuildResultsSummary();
            if (previousSummary != null) {
                Session session = null;
                Transaction transaction = null;
                try {
                    session = getSessionFactory().openSession();
                    if (session != null && session.isOpen()) {
                        transaction = session.beginTransaction();
                        session.update(previousSummary);
                        BuildCoveragePersister coveragePersister = getBuildCoveragePersister();
                        previousBuildCoverage = coveragePersister.readBuildCoverage(previousSummary.getCustomBuildData());
                        coveragePersister.removeDetailedCoverageStatistics(previousSummary.getCustomBuildData());
                        transaction.commit();
                    }
                } finally {
                    if (transaction != null && !transaction.wasCommitted()) {
                        transaction.rollback();
                    }
                    if (session != null && session.isOpen()) {
                        try {
                            session.close();
                        } catch (HibernateException e) {
                            log.debug("An error occurred while closing a Hibernate session used to retrieve previous build data.", e);
                        }
                    }
                }
            }
            previousBuildResults = previousBuildResults.getPreviousBuildResults();
        }
        return previousBuildCoverage;
    }

    private SessionFactory sessionFactory;
    private CoverageProviderLocator coverageProviderLocator;
    private BuildCoveragePersister buildCoveragePersister;
    private FileVisitorFactory fileVisitorFactory;

    private static final Logger log = Logger.getLogger(CoverageBuildProcessor.class);
}
