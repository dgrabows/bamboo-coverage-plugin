package com.atlassian.bamboo.plugins.coverage.charts;

import com.atlassian.bamboo.reports.charts.BambooReportLineChart;
import com.atlassian.bamboo.util.NumberUtils;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.time.TimeTableXYDataset;
import org.jfree.data.time.TimePeriod;

/**
 * A chart of code coverage deltas over time, broken down by author.
 */
public class CoverageDeltaByAuthorLineChart extends BambooReportLineChart implements XYToolTipGenerator {
    public CoverageDeltaByAuthorLineChart() {
        setyAxisLabel("Test Coverage Delta (change in %)");
    }

    public String generateToolTip(XYDataset xyDataset, int series, int item) {
        TimeTableXYDataset dataset = (TimeTableXYDataset) xyDataset;

        double coverageDelta = dataset.getYValue(series, item);
        String authorName = (String) dataset.getSeriesKey(series);
        TimePeriod timePeriod = dataset.getTimePeriod(item);

        return NumberUtils.round(coverageDelta, 1) + "% code coverage delta on " + timePeriod + " for " + authorName;
    }
}
