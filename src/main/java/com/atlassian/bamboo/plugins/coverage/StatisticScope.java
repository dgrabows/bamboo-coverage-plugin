package com.atlassian.bamboo.plugins.coverage;

/**
 * Enumeration of {@link CodeCoverageStatistic} scopes.
 */
public class StatisticScope {

    public static final StatisticScope PROJECT = new StatisticScope("project");
    public static final StatisticScope PACKAGE = new StatisticScope("package");
    public static final StatisticScope CLASS = new StatisticScope("class");

    private StatisticScope(String type) {
        this.scope = type;
    }

    public static StatisticScope fromString(String scope) {
        if (PROJECT.toString().equals(scope)) {
            return PROJECT;
        } else if (PACKAGE.toString().equals(scope)) {
            return PACKAGE;
        } else if (CLASS.toString().equals(scope)) {
            return CLASS;
        } else {
            return null;
        }
    }

    public String toString() {
        return scope;
    }


    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StatisticScope that = (StatisticScope) o;

        if (scope != null ? !scope.equals(that.scope) : that.scope != null) return false;

        return true;
    }

    public int hashCode() {
        return (scope != null ? scope.hashCode() : 0);
    }

    private String scope;
}
