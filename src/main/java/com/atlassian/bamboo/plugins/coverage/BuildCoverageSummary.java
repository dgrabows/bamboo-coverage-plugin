package com.atlassian.bamboo.plugins.coverage;

import org.apache.commons.collections.CollectionUtils;

import java.util.*;

/**
 * Summary of code coverage data for a build.
 */
public class BuildCoverageSummary {
    public static final int MAX_NEW_CLASSES = 25;

    public BuildCoverageSummary() {
        positiveClassStatistics = new ArrayList<CodeCoverageStatistic>();
        negativeClassStatistics = new ArrayList<CodeCoverageStatistic>();
        positivePackageStatistics = new ArrayList<CodeCoverageStatistic>();
        negativePackageStatistics = new ArrayList<CodeCoverageStatistic>();
        newClassStatistics = new ArrayList<CodeCoverageStatistic>();
    }

    public BuildCoverageSummary(Set<CodeCoverageStatistic> detailedStatistics, CodeCoverageGranularity granularityToRank) {
        this();
        Set<CodeCoverageStatistic> detailedStatsForTargetGranularity = new HashSet(detailedStatistics);
        CollectionUtils.filter(detailedStatsForTargetGranularity, new CoverageStatisticPredicate(granularityToRank));
        populateProjectStats(detailedStatsForTargetGranularity);
        populateNewClassStatistics(detailedStatsForTargetGranularity);
        populateLargestPackageDeltas(detailedStatsForTargetGranularity);
        populateLargestClassDeltas(detailedStatsForTargetGranularity);
    }

    public Double getProjectCoveragePercent() {
        return projectCoveragePercent;
    }

    public void setProjectCoveragePercent(Double projectCoveragePercent) {
        this.projectCoveragePercent = projectCoveragePercent;
    }

    public Double getProjectCoverageDelta() {
        return projectCoverageDelta;
    }

    public void setProjectCoverageDelta(Double projectCoverageDelta) {
        this.projectCoverageDelta = projectCoverageDelta;
    }

    public List<CodeCoverageStatistic> getPositiveClassStatistics() {
        return Collections.unmodifiableList(positiveClassStatistics);
    }

    public void addPositiveClassStatistics(List<CodeCoverageStatistic> stats) {
        positiveClassStatistics.addAll(stats);
    }

    public List<CodeCoverageStatistic> getNegativeClassStatistics() {
        return Collections.unmodifiableList(negativeClassStatistics);
    }

    public void addNegativeClassStatistics(List<CodeCoverageStatistic> stats) {
        negativeClassStatistics.addAll(stats);
    }

    public boolean containsClassStatistics() {
        return !(positiveClassStatistics.isEmpty() && negativeClassStatistics.isEmpty());
    }

    public List<CodeCoverageStatistic> getPositivePackageStatistics() {
        return Collections.unmodifiableList(positivePackageStatistics);
    }

    public void addPositivePackageStatistics(List<CodeCoverageStatistic> stats) {
        positivePackageStatistics.addAll(stats);
    }

    public List<CodeCoverageStatistic> getNegativePackageStatistics() {
        return Collections.unmodifiableList(negativePackageStatistics);
    }

    public void addNegativePackageStatistics(List<CodeCoverageStatistic> stats) {
        negativePackageStatistics.addAll(stats);
    }

    public boolean containsPackageStatistics() {
        return !(positivePackageStatistics.isEmpty() && negativePackageStatistics.isEmpty());
    }

    public List<CodeCoverageStatistic> getNewClassStatistics() {
        return Collections.unmodifiableList(newClassStatistics);
    }

    public void addNewClassStatistics(List<CodeCoverageStatistic> stats) {
        newClassStatistics.addAll(stats);
    }

    public boolean containsNewClassStatistics() {
        return !newClassStatistics.isEmpty();
    }

    public boolean isNewClassStatisticsLimited() {
        return newClassStatisticsLimited;
    }

    public void setNewClassStatisticsLimited(boolean newClassStatisticsLimited) {
        this.newClassStatisticsLimited = newClassStatisticsLimited;
    }

    public static int getMaxNewClasses() {
        return MAX_NEW_CLASSES;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BuildCoverageSummary that = (BuildCoverageSummary) o;

        if (newClassStatisticsLimited != that.newClassStatisticsLimited) return false;
        if (negativeClassStatistics != null ? !negativeClassStatistics.equals(that.negativeClassStatistics) : that.negativeClassStatistics != null)
            return false;
        if (negativePackageStatistics != null ? !negativePackageStatistics.equals(that.negativePackageStatistics) : that.negativePackageStatistics != null)
            return false;
        if (newClassStatistics != null ? !newClassStatistics.equals(that.newClassStatistics) : that.newClassStatistics != null)
            return false;
        if (positiveClassStatistics != null ? !positiveClassStatistics.equals(that.positiveClassStatistics) : that.positiveClassStatistics != null)
            return false;
        if (positivePackageStatistics != null ? !positivePackageStatistics.equals(that.positivePackageStatistics) : that.positivePackageStatistics != null)
            return false;
        if (projectCoverageDelta != null ? !projectCoverageDelta.equals(that.projectCoverageDelta) : that.projectCoverageDelta != null)
            return false;
        if (projectCoveragePercent != null ? !projectCoveragePercent.equals(that.projectCoveragePercent) : that.projectCoveragePercent != null)
            return false;

        return true;
    }

    public int hashCode() {
        int result;
        result = (projectCoveragePercent != null ? projectCoveragePercent.hashCode() : 0);
        result = 31 * result + (projectCoverageDelta != null ? projectCoverageDelta.hashCode() : 0);
        result = 31 * result + (positiveClassStatistics != null ? positiveClassStatistics.hashCode() : 0);
        result = 31 * result + (negativeClassStatistics != null ? negativeClassStatistics.hashCode() : 0);
        result = 31 * result + (positivePackageStatistics != null ? positivePackageStatistics.hashCode() : 0);
        result = 31 * result + (negativePackageStatistics != null ? negativePackageStatistics.hashCode() : 0);
        result = 31 * result + (newClassStatistics != null ? newClassStatistics.hashCode() : 0);
        result = 31 * result + (newClassStatisticsLimited ? 1 : 0);
        return result;
    }

    private void populateProjectStats(Set<CodeCoverageStatistic> detailedStatistics) {
        CoverageStatisticPredicate projectStatPredicate = new CoverageStatisticPredicate(StatisticScope.PROJECT);
        CodeCoverageStatistic projectCoverage = (CodeCoverageStatistic) CollectionUtils.find(detailedStatistics, projectStatPredicate);
        if (projectCoverage != null) {
            projectCoveragePercent = new Double(projectCoverage.getPercent());
            projectCoverageDelta = new Double(projectCoverage.getDeltaFromPreviousBuild());
        }
    }

    private void populateNewClassStatistics(Set<CodeCoverageStatistic> detailedStatistics) {
        newClassStatistics.addAll(detailedStatistics);
        CollectionUtils.filter(newClassStatistics, new CoverageStatisticPredicate(StatisticScope.CLASS, Boolean.TRUE));
        Collections.sort(newClassStatistics, new CoverageStatisticNameComparator());
        if (newClassStatistics.size() > MAX_NEW_CLASSES) {
            this.newClassStatistics = newClassStatistics.subList(0, MAX_NEW_CLASSES);
            newClassStatisticsLimited = true;
        }
    }

    private void populateLargestPackageDeltas(Set<CodeCoverageStatistic> detailedStatistics) {
        List packageCoverageStats = new ArrayList(detailedStatistics);
        CollectionUtils.filter(packageCoverageStats, new CoverageStatisticPredicate(StatisticScope.PACKAGE));
        if (!packageCoverageStats.isEmpty()) {
            Collections.sort(packageCoverageStats, new CoverageDeltaComparator());
            positivePackageStatistics.addAll(packageCoverageStats.subList(packageCoverageStats.size() >= 5 ? packageCoverageStats.size() - 5 : 0, packageCoverageStats.size()));
            CollectionUtils.filter(positivePackageStatistics, new PositiveCoverageDeltaPredicate());
            Collections.reverse(positivePackageStatistics);
            negativePackageStatistics.addAll(packageCoverageStats.subList(0, packageCoverageStats.size() >= 5 ? 4 : packageCoverageStats.size()));
            CollectionUtils.filter(negativePackageStatistics, new NegativeCoverageDeltaPredicate());
        }
    }

    private void populateLargestClassDeltas(Set<CodeCoverageStatistic> detailedStatistics) {
        List classCoverageStats = new ArrayList(detailedStatistics);
        CollectionUtils.filter(classCoverageStats, new CoverageStatisticPredicate(StatisticScope.CLASS));
        if (!classCoverageStats.isEmpty()) {
            Collections.sort(classCoverageStats, new CoverageDeltaComparator());
            positiveClassStatistics.addAll(classCoverageStats.subList(classCoverageStats.size() >= 5 ? classCoverageStats.size() - 5 : 0, classCoverageStats.size()));
            CollectionUtils.filter(positiveClassStatistics, new PositiveCoverageDeltaPredicate());
            Collections.reverse(positiveClassStatistics);
            negativeClassStatistics.addAll(classCoverageStats.subList(0, classCoverageStats.size() >= 5 ? 4 : classCoverageStats.size()));
            CollectionUtils.filter(negativeClassStatistics, new NegativeCoverageDeltaPredicate());
        }
    }

    private Double projectCoveragePercent;
    private Double projectCoverageDelta;

    private List<CodeCoverageStatistic> positiveClassStatistics;
    private List<CodeCoverageStatistic> negativeClassStatistics;

    private List<CodeCoverageStatistic> positivePackageStatistics;
    private List<CodeCoverageStatistic> negativePackageStatistics;

    private List<CodeCoverageStatistic> newClassStatistics;
    private boolean newClassStatisticsLimited;
}
