package com.atlassian.bamboo.plugins.coverage;

/**
 * Provides information about what is supported by a code coverage tool (i.e. EMMA, Cobertura) and implementations
 * of components specific to that tool.
 *
 * An implementation of this interface should be defined for each code coverage tool supported.
 */
public interface CoverageProvider {

    /**
     * Provides an implementation of {@link CoverageReportParser} for the code coverage tool being supported.
     * @param coverageScopeLimit the most detailed scope that coverage statistics will be generated for
     * @return report parser
     */
    CoverageReportParser getCoverageReportParser(StatisticScope coverageScopeLimit);

    /**
     * Specifies the different levels at which code coverage statistics can be provided by this coverage provider. For
     * example, if the provider can produce project, package, and class level coverage statistics, the method would
     * return {@link StatisticScope#PROJECT}, {@link StatisticScope#PACKAGE}, and {@link StatisticScope#CLASS}.
     * @return supported scopes
     */
    StatisticScope[] getSupportedScopes();

    /**
     * Specifies the different code coverage granularities that are supported by this provider. For example,
     * if the provider can produce method, line, and block coverage statistics, the method would return
     * {@link CodeCoverageGranularity#METHOD}, {@link CodeCoverageGranularity#LINE}, and
     * {@link CodeCoverageGranularity#BLOCK}.
     * @return supported granularities
     */
    CodeCoverageGranularity[] getSupportedGranularities();

    /**
     * Provides the preferred code coverage granularity for the provider. In cases where only statistics for a single
     * granularity can be considered, this is the granularity that will be used. For example, this will be the
     * granularity used to highlight codes with the greatest coverage delta for a build.
     * @return preferred granularity
     */
    CodeCoverageGranularity getPreferredGranularity();
}
