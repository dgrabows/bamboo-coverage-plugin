package com.atlassian.bamboo.plugins.coverage;

import org.apache.commons.collections.Predicate;
import com.atlassian.bamboo.plugins.coverage.StatisticScope;
import com.atlassian.bamboo.plugins.coverage.CodeCoverageGranularity;
import com.atlassian.bamboo.plugins.coverage.CodeCoverageStatistic;

/**
 * Predicate for matching {@link com.atlassian.bamboo.plugins.coverage.CodeCoverageStatistic} instances based on
 * various properties.
 */
public class CoverageStatisticPredicate implements Predicate {

    /**
     * Constructs a predicate that will match any {@link CodeCoverageStatistic} with the statistic scope provided.
     * @param scope scope to match
     */
    public CoverageStatisticPredicate(StatisticScope scope) {
        this.scope = scope;
    }

    /**
     * Constructs a predicate that will match any (@link CodeCoverageStatistic} with the granularity provided.
     * @param granularity granularity to match
     */
    public CoverageStatisticPredicate(CodeCoverageGranularity granularity) {
        this.granularity = granularity;
    }

    /**
     * Constructs a predicate that will match any (@link CodeCoverageStatistic} with the scope and granularity provided.
     * @param scope scop to match
     * @param granularity granularity to match
     */
    public CoverageStatisticPredicate(StatisticScope scope, CodeCoverageGranularity granularity) {
        this.scope = scope;
        this.granularity = granularity;
    }

    /**
     * Constructs a predicate that will match any {@link CodeCoverageStatistic} with the scope and value for the new
     * statistic flag provided.
     * @param scope scope to match
     * @param newStatistic value of the newStatistic property to match
     */
    public CoverageStatisticPredicate(StatisticScope scope, Boolean newStatistic) {
        this.scope = scope;
        this.newStatistic = newStatistic;
    }

    /**
     * Constructs a predicate that will match any (@link CodeCoverageStatistic} with the scope, granularity, and value
     * for the new statistic flag provided.
     * @param scope scope to match
     * @param granularity granularity to match
     * @param newStatistic value of the newStatistic property to match
     */
    public CoverageStatisticPredicate(StatisticScope scope, CodeCoverageGranularity granularity, Boolean newStatistic) {
        this.scope = scope;
        this.granularity = granularity;
        this.newStatistic = newStatistic;
    }

    public boolean evaluate(Object object) {
        if (!(object instanceof CodeCoverageStatistic)) {
            return false;
        }

        boolean matches = true;
        CodeCoverageStatistic stat = (CodeCoverageStatistic) object;
        if (scope != null) {
            matches &= scope == stat.getScope();
        }
        if (granularity != null) {
            matches &= granularity == stat.getGranularity();
        }
        if (newStatistic != null) {
            matches &= (newStatistic.booleanValue() == stat.isNewStatistic());
        }
        return matches;
    }

    private StatisticScope scope;
    private CodeCoverageGranularity granularity;
    private Boolean newStatistic;
}
