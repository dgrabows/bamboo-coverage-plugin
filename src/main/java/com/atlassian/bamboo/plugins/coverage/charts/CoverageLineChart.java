package com.atlassian.bamboo.plugins.coverage.charts;

import com.atlassian.bamboo.reports.charts.BambooReportLineChart;
import com.atlassian.bamboo.util.NumberUtils;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.time.TimeTableXYDataset;
import org.jfree.data.time.TimePeriod;

/**
 * Line chart tracking the percent of the project code covered at all available levels of granularity.
 */
public class CoverageLineChart extends BambooReportLineChart implements XYToolTipGenerator {

    public CoverageLineChart() {
        setyAxisLabel("% Test Coverage");
    }


    public String generateToolTip(XYDataset xyDataset, int series, int item) {
        TimeTableXYDataset dataset = (TimeTableXYDataset) xyDataset;

        double percentageCovered = dataset.getYValue(series, item);
        String buildKey = (String) dataset.getSeriesKey(series);
        TimePeriod timePeriod = dataset.getTimePeriod(item);

        return "Tests on " + timePeriod + " covered " + NumberUtils.round(percentageCovered, 1) + "% of " + buildKey + "s";
    }
}
