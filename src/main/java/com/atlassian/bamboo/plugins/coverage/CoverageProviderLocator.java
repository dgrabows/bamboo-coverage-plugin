package com.atlassian.bamboo.plugins.coverage;

import java.util.Map;

/**
 * Facilities for locating a coverage provider.
 */
public interface CoverageProviderLocator {
    /**
     * Returns a coverage provider, based on the build configuration provided.
     * @param buildConfiguration build configuration
     * @return coverage provider specified by build configuration; null if none can be determined
     */
    CoverageProvider findCoverageProvider(Map buildConfiguration);
}
