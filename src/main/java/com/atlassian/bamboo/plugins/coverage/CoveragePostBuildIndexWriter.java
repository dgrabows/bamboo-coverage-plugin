package com.atlassian.bamboo.plugins.coverage;

import com.atlassian.bamboo.index.CustomPostBuildIndexWriter;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import java.util.Iterator;
import java.util.Map;

public class CoveragePostBuildIndexWriter implements CustomPostBuildIndexWriter {
    public void updateIndexDocument(Document document, BuildResultsSummary buildResultsSummary) {
        Map customData = buildResultsSummary.getCustomBuildData();
        if (customData.containsKey(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY)) {
            Field field = new Field(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY,
                    (String) customData.get(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY),
                    Field.Store.YES,
                    Field.Index.UN_TOKENIZED);
            document.add(field);
        }
        if (customData.containsKey(BuildCoverageMapPersister.VERSION_PROPERTY)) {
            Field field = new Field(BuildCoverageMapPersister.VERSION_PROPERTY,
                    (String) customData.get(BuildCoverageMapPersister.VERSION_PROPERTY),
                    Field.Store.YES,
                    Field.Index.UN_TOKENIZED);
            document.add(field);
        }
        for (Iterator dataKeys = customData.keySet().iterator(); dataKeys.hasNext();) {
            String key = (String) dataKeys.next();
            if (key.startsWith(BuildCoverageMapPersister.COVERAGE_SUMMARY_PREFIX)) {
                Field field = new Field(key,
                        (String) customData.get(key),
                        Field.Store.YES,
                        Field.Index.UN_TOKENIZED);
                document.add(field);
            }
        }
    }
}
