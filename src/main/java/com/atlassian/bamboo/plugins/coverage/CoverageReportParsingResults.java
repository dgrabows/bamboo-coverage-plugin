package com.atlassian.bamboo.plugins.coverage;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.EqualPredicate;

import java.util.*;

/**
 * Results of parsing one or more coverage report files, including any coverage statistics read from the files and
 * any errors encountered.
 */
public class CoverageReportParsingResults {
    public CoverageReportParsingResults() {
        this(null);
    }

    public CoverageReportParsingResults(CodeCoverageGranularity granularityToRank) {
        errorMessages = new HashSet<String>();
        coverageStats = new HashSet<CodeCoverageStatistic>();
        this.granularityToRank = granularityToRank;
    }

    public Set<CodeCoverageStatistic> getCoverageStats() {
        return Collections.unmodifiableSet(coverageStats);
    }

    public void addStatistic(CodeCoverageStatistic stat) {
        if (coverageStats.contains(stat)) {
            CodeCoverageStatistic existingStat = (CodeCoverageStatistic) CollectionUtils.find(coverageStats, new EqualPredicate(stat));
            existingStat.merge(stat);
        } else {
            coverageStats.add(stat);
        }
    }

    public void addStatistics(Set<CodeCoverageStatistic> statistics) {
        for (CodeCoverageStatistic stat : statistics) {
            addStatistic(stat);
        }
    }

    public void addErrorMessage(String errorMessage) {
        errorMessages.add(errorMessage);
    }

    public Set<String> getErrorMessages() {
        return Collections.unmodifiableSet(errorMessages);
    }

    public boolean hasErrors() {
        return !errorMessages.isEmpty();
    }

    public BuildCoverage extractBuildCoverage() {
        if (!coverageStats.isEmpty()) {
            return new BuildCoverage(coverageStats, granularityToRank != null ? granularityToRank : CodeCoverageGranularity.LINE);
        } else {
            return null;
        }
    }

    /**
     * Incorporates the coverage statistics and error messages from the provided parsing results into this set of
     * parsing results.
     * @param results coverage report parsing results to add
     */
    public void mergeResults(CoverageReportParsingResults results) {
        addStatistics(results.getCoverageStats());
        errorMessages.addAll(results.getErrorMessages());
    }

    private Set<CodeCoverageStatistic> coverageStats;
    private Set<String> errorMessages;
    private CodeCoverageGranularity granularityToRank;
}
