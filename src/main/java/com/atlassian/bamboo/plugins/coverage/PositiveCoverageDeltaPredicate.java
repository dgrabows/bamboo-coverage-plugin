package com.atlassian.bamboo.plugins.coverage;

import org.apache.commons.collections.Predicate;

/**
 * Evaluates a {@link CodeCoverageStatistic} to determine if its code coverage delta is positive.
 */
public class PositiveCoverageDeltaPredicate implements Predicate {
    public boolean evaluate(Object object) {
        if (!(object instanceof CodeCoverageStatistic)) {
            return false;
        }

        return ((CodeCoverageStatistic) object).getDeltaFromPreviousBuild() > 0.0;
    }
}
