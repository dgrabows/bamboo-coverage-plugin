package com.atlassian.bamboo.plugins.coverage.emma;

import com.atlassian.bamboo.plugins.coverage.CodeCoverageGranularity;
import com.atlassian.bamboo.plugins.coverage.CoverageProvider;
import com.atlassian.bamboo.plugins.coverage.CoverageReportParser;
import com.atlassian.bamboo.plugins.coverage.StatisticScope;

/**
 * Implements support for the EMMA code coverage tool.
 */
public class EmmaCoverageProvider implements CoverageProvider {

    public CoverageReportParser getCoverageReportParser(StatisticScope coverageScopeLimit) {
        return new EmmaReportParser(coverageScopeLimit);
    }

    public StatisticScope[] getSupportedScopes() {
        return SUPPORTED_SCOPES;
    }

    public CodeCoverageGranularity[] getSupportedGranularities() {
        return SUPPORTED_GRANULARITIES;
    }

    public CodeCoverageGranularity getPreferredGranularity() {
        return CodeCoverageGranularity.BLOCK;
    }

    private static final StatisticScope[] SUPPORTED_SCOPES =
            new StatisticScope[] {StatisticScope.PROJECT, StatisticScope.PACKAGE, StatisticScope.CLASS};
    private static final CodeCoverageGranularity[] SUPPORTED_GRANULARITIES =
            new CodeCoverageGranularity[] {CodeCoverageGranularity.CLASS, CodeCoverageGranularity.METHOD,
                    CodeCoverageGranularity.LINE, CodeCoverageGranularity.BLOCK};
}
