package com.atlassian.bamboo.plugins.coverage;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * Reads and writes {@link CodeCoverageStatistic} instances to and from a {@link Map}.
 */
public class BuildCoverageMapPersister implements BuildCoveragePersister {

    public static final String COVERAGE_DATA_PREFIX = "coverage.stat";
    public static final String COVERAGE_SUMMARY_PREFIX = "coverage.summary";
    public static final String DATA_KEY_SEPARATOR = ".";
    public static final String VERSION_PROPERTY = "coverage.version";

    public BuildCoverageSummary readBuildCoverageSummary(Map storage) {
        BuildCoverageSummary summary = null;
        if (containsStatsForSupportedVersion(storage)) {
            summary = new BuildCoverageSummary();

            try {
                String projectCoverageKey = COVERAGE_SUMMARY_PREFIX + DATA_KEY_SEPARATOR + PERCENT_PROPERTY;
                if (storage.containsKey(projectCoverageKey)) {
                    summary.setProjectCoveragePercent(Double.parseDouble((String) storage.get(projectCoverageKey)));
                }
            } catch (NumberFormatException e) {
                summary.setProjectCoveragePercent(0.0);
            }
            try {
                String projectDeltaKey = COVERAGE_SUMMARY_PREFIX + DATA_KEY_SEPARATOR + DELTA_PROPERTY;
                if (storage.containsKey(projectDeltaKey)) {
                    summary.setProjectCoverageDelta(Double.parseDouble((String) storage.get(projectDeltaKey)));
                }
            } catch (NumberFormatException e) {
                summary.setProjectCoverageDelta(0.0);
            }

            String prefix = COVERAGE_SUMMARY_PREFIX + DATA_KEY_SEPARATOR + SUMMARY_SCOPE_CLASS + DATA_KEY_SEPARATOR + SUMMARY_LIST_POSITIVE;
            summary.addPositiveClassStatistics(readSummaryStats(storage, prefix));

            prefix = COVERAGE_SUMMARY_PREFIX + DATA_KEY_SEPARATOR + SUMMARY_SCOPE_CLASS + DATA_KEY_SEPARATOR + SUMMARY_LIST_NEGATIVE;
            summary.addNegativeClassStatistics(readSummaryStats(storage, prefix));

            prefix = COVERAGE_SUMMARY_PREFIX + DATA_KEY_SEPARATOR + SUMMARY_SCOPE_PACKAGE + DATA_KEY_SEPARATOR + SUMMARY_LIST_POSITIVE;
            summary.addPositivePackageStatistics(readSummaryStats(storage, prefix));

            prefix = COVERAGE_SUMMARY_PREFIX + DATA_KEY_SEPARATOR + SUMMARY_SCOPE_PACKAGE + DATA_KEY_SEPARATOR + SUMMARY_LIST_NEGATIVE;
            summary.addNegativePackageStatistics(readSummaryStats(storage, prefix));

            prefix = COVERAGE_SUMMARY_PREFIX + DATA_KEY_SEPARATOR + SUMMARY_SCOPE_CLASS + DATA_KEY_SEPARATOR + NEW_PROPERTY;
            summary.addNewClassStatistics(readSummaryStats(storage, prefix));

            String limitedNewClassesProperty = COVERAGE_SUMMARY_PREFIX + DATA_KEY_SEPARATOR + SUMMARY_SCOPE_CLASS + DATA_KEY_SEPARATOR + NEW_PROPERTY + DATA_KEY_SEPARATOR + LIMITED_PROPERTY;
            if (storage.containsKey(limitedNewClassesProperty)) {
                summary.setNewClassStatisticsLimited(Boolean.parseBoolean((String) storage.get(limitedNewClassesProperty)));
            } else {
                summary.setNewClassStatisticsLimited(false);
            }
        }
        return summary;
    }

    public void writeBuildCoverage(Map storage, BuildCoverage buildCoverage) {
        writeVersion(storage);
        writeBuildCoverageSummary(storage, buildCoverage.getSummary());
        if (buildCoverage.containsDetailedStatistics()) {
            writeDetailedStats(storage, buildCoverage.getDetailedStatistics());
        }
    }

    public void removeDetailedCoverageStatistics(Map storage) {
        if (containsStatsForSupportedVersion(storage)) {
            for (Iterator entryIter = storage.entrySet().iterator(); entryIter.hasNext();) {
                Map.Entry<String, String> entry = (Map.Entry<String, String>) entryIter.next();
                if (entry.getKey().startsWith(COVERAGE_DATA_PREFIX)) {
                    entryIter.remove();
                }
            }
        }
    }

    public BuildCoverage readBuildCoverage(Map storage) {
        BuildCoverage coverage = null;
        if (containsStatsForSupportedVersion(storage)) {
            BuildCoverageSummary summary = readBuildCoverageSummary(storage);
            Set<CodeCoverageStatistic> detailedStats = readDetailedStats(storage);
            if (detailedStats.isEmpty()) {
                coverage = new BuildCoverage(summary);
            } else {
                coverage = new BuildCoverage(summary, detailedStats);
            }
        }
        return coverage;
    }

    public boolean containsStatsForSupportedVersion(Map storage) {
        if (storage.containsKey(VERSION_PROPERTY)) {
            String version = (String) storage.get(VERSION_PROPERTY);
            for (int i = 0; i < SUPPORTED_VERSIONS.length; i++) {
                if (SUPPORTED_VERSIONS[i].equals(version)) {
                   return true;
                }
            }
        }
        return false;
    }

    // todo consolidate with readDetailedStats
    private List<CodeCoverageStatistic> readSummaryStats(Map storage, String prefix) {
        List<CodeCoverageStatistic> stats = new ArrayList<CodeCoverageStatistic>();
        List coverageStatKeys = new ArrayList(storage.keySet());
        CollectionUtils.filter(coverageStatKeys, new MatchingStatPrefixPredicate(prefix));
        Collections.sort(coverageStatKeys);

        CodeCoverageStatistic currentStat = null;
        int currentIndex = -1;
        for (Iterator iterator = coverageStatKeys.iterator(); iterator.hasNext();) {
            String statKey = (String) iterator.next();
            Integer index = getCoverageSummaryStatIndex(statKey);
            String propertyName = getCoverageSummaryStatProperyName(statKey);
            if (index != null && propertyName != null) {
                if (index.intValue() != currentIndex || currentStat == null) {
                    currentIndex = index.intValue();
                    currentStat = new CodeCoverageStatistic();
                    stats.add(currentStat);
                }

                populateStatProperty(currentStat, propertyName, (String) storage.get(statKey));
            }
        }

        return stats;
    }

    private void writeBuildCoverageSummary(Map storage, BuildCoverageSummary summary) {
        if (summary.getProjectCoveragePercent() != null) {
            storage.put(COVERAGE_SUMMARY_PREFIX + DATA_KEY_SEPARATOR + PERCENT_PROPERTY, Double.toString(summary.getProjectCoveragePercent()));
        }
        if (summary.getProjectCoverageDelta() != null) {
            storage.put(COVERAGE_SUMMARY_PREFIX + DATA_KEY_SEPARATOR + DELTA_PROPERTY, Double.toString(summary.getProjectCoverageDelta()));
        }
        if (summary.containsClassStatistics()) {
            String positiveClassPrefix = COVERAGE_SUMMARY_PREFIX + DATA_KEY_SEPARATOR + SUMMARY_SCOPE_CLASS + DATA_KEY_SEPARATOR + SUMMARY_LIST_POSITIVE;
            writeSummaryStats(storage, summary.getPositiveClassStatistics(), positiveClassPrefix);

            String negativeClassPrefix = COVERAGE_SUMMARY_PREFIX + DATA_KEY_SEPARATOR + SUMMARY_SCOPE_CLASS + DATA_KEY_SEPARATOR + SUMMARY_LIST_NEGATIVE;
            writeSummaryStats(storage, summary.getNegativeClassStatistics(), negativeClassPrefix);
        }
        if (summary.containsPackageStatistics()) {
            String positivePackagePrefix = COVERAGE_SUMMARY_PREFIX + DATA_KEY_SEPARATOR + SUMMARY_SCOPE_PACKAGE + DATA_KEY_SEPARATOR + SUMMARY_LIST_POSITIVE;
            writeSummaryStats(storage, summary.getPositivePackageStatistics(), positivePackagePrefix);

            String negativePackagePrefix = COVERAGE_SUMMARY_PREFIX + DATA_KEY_SEPARATOR + SUMMARY_SCOPE_PACKAGE + DATA_KEY_SEPARATOR + SUMMARY_LIST_NEGATIVE;
            writeSummaryStats(storage, summary.getNegativePackageStatistics(), negativePackagePrefix);
        }
        if (summary.containsNewClassStatistics()) {
            String newClassPrefix = COVERAGE_SUMMARY_PREFIX + DATA_KEY_SEPARATOR + SUMMARY_SCOPE_CLASS + DATA_KEY_SEPARATOR + NEW_PROPERTY;
            writeSummaryStats(storage, summary.getNewClassStatistics(), newClassPrefix);
            if (summary.isNewClassStatisticsLimited()) {
                String limitedNewProperty = COVERAGE_SUMMARY_PREFIX + DATA_KEY_SEPARATOR + SUMMARY_SCOPE_CLASS + DATA_KEY_SEPARATOR + NEW_PROPERTY + DATA_KEY_SEPARATOR + LIMITED_PROPERTY;
                storage.put(limitedNewProperty, Boolean.toString(summary.isNewClassStatisticsLimited()));
            }
        }
    }    

    // todo consolidate with readSummaryStats
    private Set<CodeCoverageStatistic> readDetailedStats(Map storage) {
        Set stats = new HashSet();
        List coverageStatKeys = new ArrayList(storage.keySet());
        CollectionUtils.filter(coverageStatKeys, new MatchingStatPrefixPredicate(COVERAGE_DATA_PREFIX));
        Collections.sort(coverageStatKeys);

        CodeCoverageStatistic currentStat = null;
        int currentIndex = -1;
        for (Iterator iterator = coverageStatKeys.iterator(); iterator.hasNext();) {
            String statKey = (String) iterator.next();
            Integer index = getCoverageStatIndex(statKey);
            String propertyName = getCoverageStatProperyName(statKey);
            if (index != null && propertyName != null) {
                if (index.intValue() != currentIndex || currentStat == null) {
                    currentIndex = index.intValue();
                    currentStat = new CodeCoverageStatistic();
                    stats.add(currentStat);
                }

                populateStatProperty(currentStat, propertyName, (String) storage.get(statKey));
            }
        }
        return stats;
    }

    private void writeDetailedStats(Map storage, Set<CodeCoverageStatistic> coverageStats) {
        int index = 0;
        for (Iterator iterator = coverageStats.iterator(); iterator.hasNext();) {
            CodeCoverageStatistic coverageStat = (CodeCoverageStatistic) iterator.next();
            writeCoverageStat(storage, coverageStat, COVERAGE_DATA_PREFIX, index++, true);
        }
    }

    private void populateStatProperty(CodeCoverageStatistic currentStat, String propertyName, String statValue) {
        if (NAME_PROPERTY.equals(propertyName)) {
            currentStat.setName(statValue);
        } else if (SCOPE_PROPERTY.equals(propertyName)) {
            currentStat.setScope(StatisticScope.fromString(statValue));
        } else if (GRANULARITY_PROPERTY.equals(propertyName)) {
            currentStat.setGranularity(CodeCoverageGranularity.fromString(statValue));
        }  else if (COVERED_PROPERTY.equals(propertyName)) {
            try {
                currentStat.setCoveredCount(Long.parseLong(statValue));
            } catch (NumberFormatException e) {
                currentStat.setCoveredCount(0);
            }
        } else if (TOTAL_PROPERTY.equals(propertyName)) {
            try {
                currentStat.setTotalCount(Long.parseLong(statValue));
            } catch (NumberFormatException e) {
                currentStat.setTotalCount(0);
            }
        } else if (DELTA_PROPERTY.equals(propertyName)) {
            try {
                currentStat.setDeltaFromPreviousBuild(Double.parseDouble(statValue));
            } catch (NumberFormatException e) {
                currentStat.setDeltaFromPreviousBuild(0.0);
            }
        } else if (NEW_PROPERTY.equals(propertyName)) {
            currentStat.setNewStatistic(Boolean.parseBoolean(statValue));
        }
    }

    private void writeSummaryStats(Map storage, List<CodeCoverageStatistic> stats, String positiveClassPrefix) {
        int index = 0;
        for (Iterator<CodeCoverageStatistic> positiveClassDeltas = stats.iterator(); positiveClassDeltas.hasNext();)
        {
            CodeCoverageStatistic positiveClassDelta = positiveClassDeltas.next();
            writeCoverageStat(storage, positiveClassDelta, positiveClassPrefix, index++, false);
        }
    }

    private void writeCoverageStat(Map storage, CodeCoverageStatistic coverageStat, String prefix, int index, boolean saveFullDetail) {
        String keyBase = prefix + "[" + index + "]" + DATA_KEY_SEPARATOR;
        storage.put(keyBase + NAME_PROPERTY, coverageStat.getName());
        storage.put(keyBase + DELTA_PROPERTY, Double.toString(coverageStat.getDeltaFromPreviousBuild()));
        storage.put(keyBase + COVERED_PROPERTY, Long.toString(coverageStat.getCoveredCount()));
        storage.put(keyBase + TOTAL_PROPERTY, Long.toString(coverageStat.getTotalCount()));
        if (saveFullDetail) {
            storage.put(keyBase + SCOPE_PROPERTY, coverageStat.getScope().toString());
            storage.put(keyBase + GRANULARITY_PROPERTY, coverageStat.getGranularity().toString());
            if (coverageStat.isNewStatistic()) {
                storage.put(keyBase + NEW_PROPERTY, Boolean.toString(coverageStat.isNewStatistic()));
            }
        }
    }

    private void writeVersion(Map storage) {
        storage.put(VERSION_PROPERTY, CURRENT_VERSION);
    }

    private Integer getCoverageStatIndex(String statKey) {
        Matcher indexMatcher = STAT_PROPERTY_PATTERN.matcher(statKey);
        return indexMatcher.matches() ? new Integer(indexMatcher.group(1)) : null;
    }

    private Integer getCoverageSummaryStatIndex(String statKey) {
        Matcher indexMatcher = SUMMARY_STAT_PROPERTY_PATTERN.matcher(statKey);
        return indexMatcher.matches() ? new Integer(indexMatcher.group(1)) : null;
    }

    private String getCoverageStatProperyName(String statKey) {
        Matcher indexMatcher = STAT_PROPERTY_PATTERN.matcher(statKey);
        return indexMatcher.matches() ? indexMatcher.group(2) : null;
    }

    private String getCoverageSummaryStatProperyName(String statKey) {
        Matcher indexMatcher = SUMMARY_STAT_PROPERTY_PATTERN.matcher(statKey);
        return indexMatcher.matches() ? indexMatcher.group(2) : null;
    }

    private static final Pattern STAT_PROPERTY_PATTERN = Pattern.compile((COVERAGE_DATA_PREFIX + "\\[(\\d+)\\]" + DATA_KEY_SEPARATOR + "(\\w+)").replaceAll("\\.", "\\."));
    private static final Pattern SUMMARY_STAT_PROPERTY_PATTERN = Pattern.compile((COVERAGE_SUMMARY_PREFIX + "[a-zA-Z" + DATA_KEY_SEPARATOR + "]+" + "\\[(\\d+)\\]" + DATA_KEY_SEPARATOR + "(\\w+)").replaceAll("\\.", "\\."));
    private static final String NAME_PROPERTY = "name";
    private static final String SCOPE_PROPERTY = "scope";
    private static final String GRANULARITY_PROPERTY = "granularity";
    private static final String COVERED_PROPERTY = "covered";
    private static final String TOTAL_PROPERTY = "total";
    private static final String DELTA_PROPERTY = "delta";
    private static final String NEW_PROPERTY = "new";
    private static final String SUMMARY_SCOPE_CLASS = "class";
    private static final String SUMMARY_SCOPE_PACKAGE = "package";
    private static final String SUMMARY_LIST_POSITIVE = "positive";
    private static final String SUMMARY_LIST_NEGATIVE = "negative";
    private static final String PERCENT_PROPERTY = "percent";
    private static final String LIMITED_PROPERTY = "limited";
    private static final String CURRENT_VERSION = "1.1";
    private static final String[] SUPPORTED_VERSIONS = {"1.1"};

    private class MatchingStatPrefixPredicate implements Predicate {
        public MatchingStatPrefixPredicate(String prefix) {
            this.prefix = prefix;
        }

        public boolean evaluate(Object object) {
            return object instanceof String && ((String) object).startsWith(prefix);
        }

        private String prefix;
    }
}
