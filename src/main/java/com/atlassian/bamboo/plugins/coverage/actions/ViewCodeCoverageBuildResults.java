package com.atlassian.bamboo.plugins.coverage.actions;

import com.atlassian.bamboo.build.ViewBuildResults;
import com.atlassian.bamboo.plugins.coverage.*;

import java.util.Map;

/**
 * Action that handles displaying the code coverage tab on the build results page.
 */
public class ViewCodeCoverageBuildResults extends ViewBuildResults {
    public void setCoverageProviderLocator(CoverageProviderLocator coverageProviderLocator) {
        this.coverageProviderLocator = coverageProviderLocator;
    }

    public void setCodeCoverageStatisticPersister(BuildCoveragePersister buildCoveragePersister) {
        this.buildCoveragePersister = buildCoveragePersister;
    }

    public BuildCoverageSummary getBuildCoverageSummary() {
        return buildCoverageSummary;
    }

    public String execute() throws Exception {
        String status = super.doExecute();
        if (status.equals(SUCCESS)) {
            if (getBuildResults() != null) {
                Map customBuildData = getBuildResults().getBuildResultsSummary().getCustomBuildData();
                if (customBuildData != null && !customBuildData.isEmpty()) {
                    buildCoverageSummary = getBuildCoveragePersister().readBuildCoverageSummary(customBuildData);
                }
            }
            return SUCCESS;
        } else {
            return status;
        }
    }

    protected CoverageProviderLocator getCoverageProviderLocator() {
        if (coverageProviderLocator == null) {
            coverageProviderLocator = new CoverageProviderLocatorImpl();
        }
        return coverageProviderLocator;
    }

    protected BuildCoveragePersister getBuildCoveragePersister() {
        if (buildCoveragePersister == null) {
            buildCoveragePersister = new BuildCoverageMapPersister();
        }
        return buildCoveragePersister;
    }

    private CoverageProviderLocator coverageProviderLocator;
    private BuildCoveragePersister buildCoveragePersister;
    private BuildCoverageSummary buildCoverageSummary;
}
