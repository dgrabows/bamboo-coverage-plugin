package com.atlassian.bamboo.plugins.coverage;

import java.io.File;

public class FileVisitorFactoryImpl implements FileVisitorFactory {
    public CoverageFileVisitor newCoverageFileVisitor(File baseDirectory, CoverageProvider coverageProvider, StatisticScope scopeLimit) {
        return new XmlCoverageFileVisitor(baseDirectory, coverageProvider, scopeLimit);
    }
}
