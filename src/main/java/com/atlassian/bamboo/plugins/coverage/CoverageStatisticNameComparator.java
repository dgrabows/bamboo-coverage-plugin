package com.atlassian.bamboo.plugins.coverage;

import java.util.Comparator;

/**
 * Comparator for sorting {@link CodeCoverageStatistic} instances based on the natural order of their names.
 */
public class CoverageStatisticNameComparator implements Comparator {
    public int compare(Object o1, Object o2) {
        CodeCoverageStatistic stat1 = (CodeCoverageStatistic) o1;
        CodeCoverageStatistic stat2 = (CodeCoverageStatistic) o2;

        return stat1.getName().compareTo(stat2.getName());
    }
}
