package com.atlassian.bamboo.plugins.coverage;

import org.dom4j.DocumentException;

import java.util.Collection;
import java.io.InputStream;
import java.io.File;
import java.net.MalformedURLException;

/**
 * Builds code coverage statistics from a coverage report file.
 */
public interface CoverageReportParser {
    /**
     * Builds code coverage statistics from the report contained in the provided file.
     *
     * @param file file containing report
     * @return collection of {@link CodeCoverageStatistic}
     * @throws MalformedURLException thrown if unable to open file identified
     * @throws DocumentException thrown if parsing the report file fails
     */
    CoverageReportParsingResults parse(File file) throws MalformedURLException, DocumentException;
}
