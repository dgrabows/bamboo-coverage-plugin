package com.atlassian.bamboo.plugins.coverage;

import com.atlassian.bamboo.utils.FileVisitor;

import java.io.File;

/**
 * Parses XML code coverage report files and aggregates statistics.
 */
class XmlCoverageFileVisitor extends FileVisitor implements CoverageFileVisitor {

    public XmlCoverageFileVisitor(File baseDirectory, CoverageProvider coverageProvider, StatisticScope scopeLimit) {
        super(baseDirectory);
        this.coverageParser = coverageProvider.getCoverageReportParser(scopeLimit);
        atLeastFileFound = false;
        overallResults = new CoverageReportParsingResults(coverageProvider.getPreferredGranularity());
    }

    public CoverageReportParsingResults getCoverageParsingResults() {
        if (!atLeastFileFound) {
            overallResults.addErrorMessage("The coverage plugin was unable to find a coverage report file.");
        }
        return overallResults;
    }

    public void visitFile(File file) {
        if (file.getName().endsWith("xml")) {
            try {
                atLeastFileFound = true;
                CoverageReportParsingResults fileResults = coverageParser.parse(file);
                overallResults.mergeResults(fileResults);
            } catch (Exception e) {
                overallResults.addErrorMessage("The coverage plugin was unable to parse the coverage report file \"" +
                        file.getName() + "\" due to " +
                        (e.getMessage() != null ? "the error: " + e.getMessage() : "an unspecified error."));
            }
        }
    }

    private CoverageReportParsingResults overallResults;
    private CoverageReportParser coverageParser;
    private boolean atLeastFileFound;
}
