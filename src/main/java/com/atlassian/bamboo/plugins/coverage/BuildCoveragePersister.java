package com.atlassian.bamboo.plugins.coverage;

import java.util.Map;

/**
 * Persists code coverage statistics for a build.
 */
public interface BuildCoveragePersister {
    BuildCoverage readBuildCoverage(Map storage);

    void writeBuildCoverage(Map storage, BuildCoverage buildCoverage);

    void removeDetailedCoverageStatistics(Map storage);

    BuildCoverageSummary readBuildCoverageSummary(Map storage);

    boolean containsStatsForSupportedVersion(Map storage);
}
