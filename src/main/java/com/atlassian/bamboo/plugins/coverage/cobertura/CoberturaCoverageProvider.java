package com.atlassian.bamboo.plugins.coverage.cobertura;

import com.atlassian.bamboo.plugins.coverage.CoverageProvider;
import com.atlassian.bamboo.plugins.coverage.CoverageReportParser;
import com.atlassian.bamboo.plugins.coverage.StatisticScope;
import com.atlassian.bamboo.plugins.coverage.CodeCoverageGranularity;
import com.atlassian.bamboo.plugins.coverage.clover.CloverReportParser;
import com.atlassian.bamboo.plugins.coverage.clover.CloverCoverageProvider;

/**
 * Implements support for the Cobertura code coverage tool.
 */
public class CoberturaCoverageProvider implements CoverageProvider {

    public CoverageReportParser getCoverageReportParser(StatisticScope coverageScopeLimit) {
        return new CoberturaReportParser(coverageScopeLimit);
    }

    public StatisticScope[] getSupportedScopes() {
        return SUPPORTED_SCOPES;
    }

    public CodeCoverageGranularity[] getSupportedGranularities() {
        return SUPPORTED_GRANULARITIES;
    }

    public CodeCoverageGranularity getPreferredGranularity() {
        return CodeCoverageGranularity.LINE;
    }

    private static final StatisticScope[] SUPPORTED_SCOPES =
            new StatisticScope[] {StatisticScope.PROJECT, StatisticScope.PACKAGE, StatisticScope.CLASS};
    private static final CodeCoverageGranularity[] SUPPORTED_GRANULARITIES =
            new CodeCoverageGranularity[] {CodeCoverageGranularity.LINE, CodeCoverageGranularity.BRANCH};
}
