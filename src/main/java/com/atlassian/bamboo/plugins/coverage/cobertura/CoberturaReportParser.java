package com.atlassian.bamboo.plugins.coverage.cobertura;

import com.atlassian.bamboo.plugins.coverage.*;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.net.MalformedURLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Code coverage report parser for Clover XML reports.
 */
public class CoberturaReportParser extends BaseReportParser implements CoverageReportParser {

    public CoberturaReportParser(StatisticScope scopeLimit) {
        this.scopeLimit = scopeLimit;
    }

    public CoverageReportParsingResults parse(File file) throws MalformedURLException, DocumentException {
        SAXReader reader = configureReader();
        Document coberturaReport = reader.read(file);

        CoverageReportParsingResults results = new CoverageReportParsingResults();

        if (!isValidCoberturaResultFile(coberturaReport)) {
            results.addErrorMessage("The coverage plugin skipped parsing the file " + file.getName() +
                    " because it does not appear to be a valid Cobertura coverage report file.");
            return results;
        }

        results.addStatistics(buildProjectCoverageStats(coberturaReport));
        if (StatisticScope.CLASS == scopeLimit || StatisticScope.PACKAGE == scopeLimit) {
            results.addStatistics(buildPackageCoverageStats(coberturaReport));
        }
        if (StatisticScope.CLASS == scopeLimit) {
            results.addStatistics(buildClassCoverageStats(coberturaReport));
        }

        return results;
    }

    private Set<CodeCoverageStatistic> buildProjectCoverageStats(Document report) {
        Set<CodeCoverageStatistic> stats = new HashSet<CodeCoverageStatistic>();
        List projectMetrics = report.selectNodes("/coverage");
        for (Iterator iterator = projectMetrics.iterator(); iterator.hasNext();) {
            Node projectNode = (Node) iterator.next();
            stats.addAll(buildStatisticsFromNode(projectNode, null, StatisticScope.PROJECT));
        }
        return stats;
    }

    private Set<CodeCoverageStatistic> buildPackageCoverageStats(Document report) {
        Set<CodeCoverageStatistic> stats = new HashSet<CodeCoverageStatistic>();
        List packages = report.selectNodes("//package");
        for (Iterator iterator = packages.iterator(); iterator.hasNext();) {
            Node packageNode = (Node) iterator.next();
            stats.addAll(buildStatisticsFromNode(packageNode, packageNode.valueOf("@name"), StatisticScope.PACKAGE));
        }
        return stats;
    }

    private Set<CodeCoverageStatistic> buildClassCoverageStats(Document report) {
        Set<CodeCoverageStatistic> stats = new HashSet<CodeCoverageStatistic>();
        List classes = report.selectNodes("//class");
        for (Iterator iterator = classes.iterator(); iterator.hasNext();) {
            Node classNode = (Node) iterator.next();
            stats.addAll(buildStatisticsFromNode(classNode, classNode.valueOf("@name"), StatisticScope.CLASS));
        }
        return stats;
    }

    private Collection buildStatisticsFromNode(Node node, String statName, StatisticScope scope) {
        Collection stats = new ArrayList();

        Number totalLineCount;
        Number coveredLineCount;
        if (scope == StatisticScope.CLASS) {
            totalLineCount = node.numberValueOf("count(./lines/line)");
            coveredLineCount = node.numberValueOf("count(./lines/line[@hits > 0])");
        } else {
            totalLineCount = node.numberValueOf("count(.//class/lines/line)");
            coveredLineCount = node.numberValueOf("count(.//class/lines/line[@hits > 0])");
        }

        if (totalLineCount != null && coveredLineCount != null) {
            stats.add(new CodeCoverageStatistic(statName, scope, CodeCoverageGranularity.LINE, coveredLineCount.longValue(), totalLineCount.longValue()));
        }

        long coveredBranchCount = 0;
        long totalBranchCount = 0;
        List branches;
        if (scope == StatisticScope.CLASS) {
            branches = node.selectNodes("./lines/line[@branch='true']");
        } else {
            branches = node.selectNodes(".//class/lines/line[@branch='true']");
        }
        for (Iterator branchIter = branches.iterator(); branchIter.hasNext();) {
            Node branch = (Node) branchIter.next();
            String branchStats = branch.valueOf("@condition-coverage");

            Matcher matcher = BRANCH_COVERAGE_PATTERN.matcher(branchStats);
            if (matcher.matches()) {
                coveredBranchCount += Long.parseLong(matcher.group(1));
                totalBranchCount += Long.parseLong(matcher.group(2));
            }
        }

        stats.add(new CodeCoverageStatistic(statName, scope, CodeCoverageGranularity.BRANCH, coveredBranchCount, totalBranchCount));

        return stats;
    }

    private boolean isValidCoberturaResultFile(Document document) {
        return document.selectSingleNode("/coverage/packages") != null;
    }

    private StatisticScope scopeLimit;

    private static final Pattern BRANCH_COVERAGE_PATTERN = Pattern.compile(".*\\((\\d+)/(\\d+)\\).*");
}
