package com.atlassian.bamboo.plugins.coverage;

/**
 * A statistic about code.
 */
public class CodeCoverageStatistic {
    public CodeCoverageStatistic() {
    }

    public CodeCoverageStatistic(String name, StatisticScope scope, CodeCoverageGranularity granularity, long coveredCount, long totalCount) {
        this.name = name;
        this.scope = scope;
        this.granularity = granularity;
        this.totalCount = totalCount;
        this.coveredCount = coveredCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StatisticScope getScope() {
        return scope;
    }

    public void setScope(StatisticScope scope) {
        this.scope = scope;
    }

    public CodeCoverageGranularity getGranularity() {
        return granularity;
    }

    public void setGranularity(CodeCoverageGranularity granularity) {
        this.granularity = granularity;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public long getCoveredCount() {
        return coveredCount;
    }

    public void setCoveredCount(long coveredCount) {
        this.coveredCount = coveredCount;
    }

    public double getPercent() {
        if (totalCount > 0) {
            return ((double) coveredCount / (double) totalCount) * 100.0;
        } else {
            return 0.0;
        }
    }

    public double getDeltaFromPreviousBuild() {
        return deltaFromPreviousBuild;
    }

    public void setDeltaFromPreviousBuild(double deltaFromPreviousBuild) {
        this.deltaFromPreviousBuild = deltaFromPreviousBuild;
    }

    public boolean isNewStatistic() {
        return newStatistic;
    }

    public void setNewStatistic(boolean newStatistic) {
        this.newStatistic = newStatistic;
    }

    public void merge(CodeCoverageStatistic otherStat) {
        if (!equals(otherStat)) {
            throw new IllegalArgumentException("Code coverage statistics may only be merged if they are equal to each other.");
        }

        coveredCount += otherStat.coveredCount;
        totalCount += otherStat.totalCount;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CodeCoverageStatistic that = (CodeCoverageStatistic) o;

        if (granularity != null ? !granularity.equals(that.granularity) : that.granularity != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (scope != null ? !scope.equals(that.scope) : that.scope != null) return false;

        return true;
    }

    public int hashCode() {
        int result;
        result = (name != null ? name.hashCode() : 0);
        result = 31 * result + (scope != null ? scope.hashCode() : 0);
        result = 31 * result + (granularity != null ? granularity.hashCode() : 0);
        return result;
    }


    public String toString() {
        return "CodeCoverageStatistic{" +
                "name='" + name + '\'' +
                ", scope=" + scope +
                ", granularity=" + granularity +
                ", coveredCount=" + coveredCount +
                ", totalCount=" + totalCount +
                ", deltaFromPreviousBuild=" + deltaFromPreviousBuild +
                ", newStatistic=" + newStatistic +
                '}';
    }

    private String name;
    private StatisticScope scope;
    private CodeCoverageGranularity granularity;
    private long totalCount;
    private long coveredCount;
    private double deltaFromPreviousBuild;
    private boolean newStatistic;
}
