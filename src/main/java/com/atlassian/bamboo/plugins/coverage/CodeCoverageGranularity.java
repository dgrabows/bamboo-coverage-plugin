package com.atlassian.bamboo.plugins.coverage;

/**
 * Enumeration of code coverage granularities.
 */
public class CodeCoverageGranularity {

    public static final CodeCoverageGranularity CLASS = new CodeCoverageGranularity("class");
    public static final CodeCoverageGranularity METHOD = new CodeCoverageGranularity("method");
    public static final CodeCoverageGranularity CONDITIONAL = new CodeCoverageGranularity("conditional");
    public static final CodeCoverageGranularity BRANCH = new CodeCoverageGranularity("branch");
    public static final CodeCoverageGranularity LINE = new CodeCoverageGranularity("line");
    public static final CodeCoverageGranularity STATEMENT = new CodeCoverageGranularity("statement");
    public static final CodeCoverageGranularity BLOCK = new CodeCoverageGranularity("block");
    public static final CodeCoverageGranularity ELEMENT = new CodeCoverageGranularity("element");

    private CodeCoverageGranularity(String granularity) {
        this.granularity = granularity;
    }

    public static CodeCoverageGranularity fromString(String granularity) {
        if (CLASS.toString().equals(granularity)) {
            return CLASS;
        } else if (METHOD.toString().equals(granularity)) {
            return METHOD;
        } else if (CONDITIONAL.toString().equals(granularity)) {
            return CONDITIONAL;
        } else if (BRANCH.toString().equals(granularity)) {
            return BRANCH;
        } else if (LINE.toString().equals(granularity)) {
            return LINE;
        } else if (STATEMENT.toString().equals(granularity)) {
            return STATEMENT;
        } else if (BLOCK.toString().equals(granularity)) {
            return BLOCK;
        } else if (ELEMENT.toString().equals(granularity)) {
            return ELEMENT;
        } else {
            return null;
        }
    }

    public String toString() {
        return granularity;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CodeCoverageGranularity that = (CodeCoverageGranularity) o;

        if (granularity != null ? !granularity.equals(that.granularity) : that.granularity != null) return false;

        return true;
    }

    public int hashCode() {
        return (granularity != null ? granularity.hashCode() : 0);
    }

    private String granularity;
}
