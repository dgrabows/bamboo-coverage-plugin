package com.atlassian.bamboo.plugins.coverage.clover;

import com.atlassian.bamboo.plugins.coverage.*;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.net.MalformedURLException;
import java.util.*;

/**
 * Code coverage report parser for Clover XML reports.
 */
public class CloverReportParser extends BaseReportParser implements CoverageReportParser {

    public CloverReportParser(StatisticScope scopeLimit) {
        this.scopeLimit = scopeLimit;
    }

    public CoverageReportParsingResults parse(File file) throws MalformedURLException, DocumentException {
        SAXReader reader = configureReader();
        Document cloverReport = reader.read(file);

        CoverageReportParsingResults results = new CoverageReportParsingResults();

        if (!isValidCloverResultFile(cloverReport)) {
            results.addErrorMessage("The coverage plugin skipped parsing the file " + file.getName() +
                    " because it does not appear to be a valid Clover coverage report file.");
            return results;
        }

        results.addStatistics(buildProjectCoverageStats(cloverReport));
        if (StatisticScope.CLASS == scopeLimit || StatisticScope.PACKAGE == scopeLimit) {
            results.addStatistics(buildPackageCoverageStats(cloverReport));
        }
        if (StatisticScope.CLASS == scopeLimit) {
            results.addStatistics(buildClassCoverageStats(cloverReport));
        }

        return results;
    }

    private Set<CodeCoverageStatistic> buildProjectCoverageStats(Document report) {
        Set<CodeCoverageStatistic> stats = new HashSet<CodeCoverageStatistic>();
        List projectMetrics = report.selectNodes("//project/metrics");
        for (Iterator iterator = projectMetrics.iterator(); iterator.hasNext();) {
            Node metricsNode = (Node) iterator.next();
            stats.addAll(buildStatisticsFromMetricsNode(metricsNode, null, StatisticScope.PROJECT));
        }
        return stats;
    }

    private Set<CodeCoverageStatistic> buildPackageCoverageStats(Document report) {
        Set<CodeCoverageStatistic> stats = new HashSet<CodeCoverageStatistic>();
        List packages = report.selectNodes("//package");
        for (Iterator iterator = packages.iterator(); iterator.hasNext();) {
            Node packageNode = (Node) iterator.next();
            Node metricsNode = packageNode.selectSingleNode("metrics");
            if (metricsNode != null) {
                stats.addAll(buildStatisticsFromMetricsNode(metricsNode, packageNode.valueOf("@name"), StatisticScope.PACKAGE));
            }
        }
        return stats;
    }

    private Set<CodeCoverageStatistic> buildClassCoverageStats(Document report) {
        Set<CodeCoverageStatistic> stats = new HashSet<CodeCoverageStatistic>();
        List classes = report.selectNodes("//class");
        for (Iterator iterator = classes.iterator(); iterator.hasNext();) {
            Node classNode = (Node) iterator.next();
            Node metricsNode = classNode.selectSingleNode("metrics");
            if (metricsNode != null) {
                String qualifiedClassName = classNode.valueOf("../../@name") + "." + classNode.valueOf("@name");
                stats.addAll(buildStatisticsFromMetricsNode(metricsNode, qualifiedClassName, StatisticScope.CLASS));
            }
        }
        return stats;
    }

    private Collection buildStatisticsFromMetricsNode(Node metricsNode, String statName, StatisticScope scope) {
        Collection stats = new ArrayList();

        for (int i = 0; i < METRIC_NAMES.length; i++) {
            String metricName = METRIC_NAMES[i];

            Long metricTotalCount = getLongValueFromNode(metricsNode, "@" + metricName);
            Long metricCoveredCount = getLongValueFromNode(metricsNode, "@covered" + metricName);
            if (metricTotalCount != null && metricCoveredCount != null) {
                stats.add(new CodeCoverageStatistic(statName, scope, METRIC_GRANULARITIES[i], metricCoveredCount.longValue(), metricTotalCount.longValue()));
            }
        }

        return stats;
    }

    private Long getLongValueFromNode(Node metricsNode, String attribute) {
        Number number = metricsNode.numberValueOf(attribute);
        if (number != null) {
            return new Long(number.longValue());
        } else {
            return null;
        }
    }

    private boolean isValidCloverResultFile(Document document) {
        return document.selectSingleNode("/coverage/project") != null;
    }

    private static final String[] METRIC_NAMES = new String[] {"elements", "methods", "conditionals", "statements"};
    private static final CodeCoverageGranularity[] METRIC_GRANULARITIES =
            new CodeCoverageGranularity[] {CodeCoverageGranularity.ELEMENT, CodeCoverageGranularity.METHOD,
                    CodeCoverageGranularity.CONDITIONAL, CodeCoverageGranularity.STATEMENT};

    private StatisticScope scopeLimit;
}
