package com.atlassian.bamboo.plugins.coverage.actions;

import com.atlassian.bamboo.build.FilterController;
import com.atlassian.bamboo.plugins.coverage.*;
import com.atlassian.bamboo.reports.collector.ReportCollector;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.ww2.actions.BuildActionSupport;
import com.atlassian.bamboo.ww2.aware.ResultsListAware;
import com.atlassian.bamboo.ww2.aware.permissions.BuildReadSecurityAware;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginManager;
import org.jfree.data.time.TimeTableXYDataset;
import org.jfree.data.xy.XYDataset;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ViewCodeCoverageSummary extends BuildActionSupport implements ResultsListAware, BuildReadSecurityAware {
    public PluginManager getPluginManager()
    {
        return pluginManager;
    }

    public void setPluginManager(PluginManager pluginManager)
    {
        this.pluginManager = pluginManager;
    }

        public void setCoverageProviderLocator(CoverageProviderLocator coverageProviderLocator) {
        this.coverageProviderLocator = coverageProviderLocator;
    }

    public void setCodeCoverageStatisticPersister(BuildCoveragePersister buildCoveragePersister) {
        this.buildCoveragePersister = buildCoveragePersister;
    }

    public FilterController getFilterController()
    {
        return filterController;
    }

    public void setFilterController(FilterController filterController)
    {
        this.filterController = filterController;
    }


    public List getResultsList()
    {
        return resultsList;
    }

    public void setResultsList(List resultsList)
    {
        this.resultsList = resultsList;
    }

    public XYDataset getDataset()
    {
        return dataset;
    }

    public void setDataset(XYDataset dataset)
    {
        this.dataset = dataset;
    }


    public String getReportKey()
    {
        return reportKey;
    }

    public void setReportKey(String reportKey)
    {
        this.reportKey = reportKey;
    }

    public BuildCoverageSummary getBuildCoverageSummary() {
        return buildCoverageSummary;
    }

    public Integer getBuildsSinceDecrease() {
        return buildsSinceDecrease;
    }

    public String doViewLineCoverage() throws Exception
    {
        setReportKey("com.atlassian.bamboo.plugins.coverage:codeCoverage");
        if (resultsList != null && !resultsList.isEmpty())
        {
            populateDatasetForChart();
        }
        return SUCCESS;
    }

    public String doViewCoverageDeltaByAuthor() throws Exception
    {
        setReportKey("com.atlassian.bamboo.plugins.coverage:authorCoverageDelta");
        if (resultsList != null && !resultsList.isEmpty())
        {
            populateDatasetForChart();
        }
        return SUCCESS;
    }

    public String execute() throws Exception
    {
        if (resultsList != null && !resultsList.isEmpty())
        {
            populateLatestBuildCoverageStats();
        }

        return SUCCESS;
    }


    protected CoverageProviderLocator getCoverageProviderLocator() {
        if (coverageProviderLocator == null) {
            coverageProviderLocator = new CoverageProviderLocatorImpl();
        }
        return coverageProviderLocator;
    }

    protected BuildCoveragePersister getBuildCoveragePersister() {
        if (buildCoveragePersister == null) {
            buildCoveragePersister = new BuildCoverageMapPersister();
        }
        return buildCoveragePersister;
    }

    private void populateDatasetForChart() {
        ModuleDescriptor descriptor = pluginManager.getPluginModule(getReportKey());
        if (descriptor != null)
        {
            ReportCollector collector = (ReportCollector) descriptor.getModule();
            collector.setResultsList(resultsList);
            collector.setParams(Collections.EMPTY_MAP);
            dataset = (TimeTableXYDataset) collector.getDataset();
        }
    }

    private void populateLatestBuildCoverageStats() {
        BuildResultsSummary latestResults = (BuildResultsSummary) resultsList.get(0);
        CoverageProvider coverageProvider = getCoverageProviderLocator().findCoverageProvider(latestResults.getCustomBuildData());
        if (coverageProvider != null) {
            buildCoverageSummary = findMostRecentBuildCoverageSummary();
            if (buildCoverageSummary != null) {
                BuildResultsSummary mostRecentDecrease = findMostRecentCoverageDecrease();
                buildsSinceDecrease = new Integer(mostRecentDecrease != null ? resultsList.indexOf(mostRecentDecrease) : resultsList.size());
            }
        }
    }

    private BuildCoverageSummary findMostRecentBuildCoverageSummary() {
        for (Iterator buildResults = resultsList.iterator(); buildResults.hasNext();) {
            BuildResultsSummary buildResult = (BuildResultsSummary) buildResults.next();
            BuildCoverageSummary buildCoverageSummary = getBuildCoveragePersister().readBuildCoverageSummary(buildResult.getCustomBuildData());
            if (buildCoverageSummary != null) {
                return buildCoverageSummary;
            }
        }
        return null;
    }

    private BuildResultsSummary findMostRecentCoverageDecrease() {
        BuildResultsSummary mostRecentCoverageDecrease = null;
        for (Iterator results = resultsList.iterator(); results.hasNext();) {
            BuildResultsSummary buildResultsSummary = (BuildResultsSummary) results.next();
            BuildCoverageSummary coverageSummary = getBuildCoveragePersister().readBuildCoverageSummary(buildResultsSummary.getCustomBuildData());
            if (coverageSummary != null && coverageSummary.getProjectCoverageDelta() < 0.0) {
                mostRecentCoverageDecrease = buildResultsSummary;
                break;
            }
        }
        return mostRecentCoverageDecrease;
    }

    // --------------------------------------------------------------------------------------------------Dependencies
    PluginManager pluginManager;
    CoverageProviderLocator coverageProviderLocator;
    BuildCoveragePersister buildCoveragePersister;
    // ----------------------------------------------------------------------------------------------- Statistic Display
    BuildCoverageSummary buildCoverageSummary;
    Integer buildsSinceDecrease;
    // -------------------------------------------------------------------------------------------------- Charting
    List resultsList;
    XYDataset dataset;
    String reportKey = "com.atlassian.bamboo.plugins.coverage:codeCoverage"; // default.
    // -------------------------------------------------------------------------------------------------- Filter
    FilterController filterController;
}
