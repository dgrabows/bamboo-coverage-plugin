package com.atlassian.bamboo.plugins.coverage;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.EqualPredicate;

import java.util.*;

/**
 * Code coverage statistics collected for a build.
 */
public class BuildCoverage {
    public BuildCoverage(BuildCoverageSummary summary) {
        this.summary = summary;
    }

    public BuildCoverage(Set<CodeCoverageStatistic> detailedStatistics, CodeCoverageGranularity granularityToRank) {
        this.granularityToRank = granularityToRank;
        this.detailedStatistics = new HashSet<CodeCoverageStatistic>(detailedStatistics);
        CollectionUtils.filter(this.detailedStatistics, new CoverageStatisticPredicate(granularityToRank));
    }

    public BuildCoverage(BuildCoverageSummary summary, Set<CodeCoverageStatistic> detailedStatistics) {
        this.summary = summary;
        this.detailedStatistics = new HashSet<CodeCoverageStatistic>(detailedStatistics);
    }

    public BuildCoverageSummary getSummary() {
        if (summary == null) {
            calculateSummary();
        }
        return summary;
    }

    public boolean containsDetailedStatistics() {
        return detailedStatistics != null;
    }

    public Set<CodeCoverageStatistic> getDetailedStatistics() {
        return detailedStatistics;
    }

    public void populateChangeFromPreviousBuild(BuildCoverage previousBuildCoverage) {
        if (containsDetailedStatistics()) {
            for (Iterator currentStatIter = detailedStatistics.iterator(); currentStatIter.hasNext();) {
                CodeCoverageStatistic currentStat = (CodeCoverageStatistic) currentStatIter.next();
                CodeCoverageStatistic previousStat = null;
                if (previousBuildCoverage.containsDetailedStatistics()) {
                    previousStat = (CodeCoverageStatistic) CollectionUtils.find(previousBuildCoverage.detailedStatistics, new EqualPredicate(currentStat));
                }
                if (previousStat != null) {
                    currentStat.setDeltaFromPreviousBuild(currentStat.getPercent() - previousStat.getPercent());
                } else {
                    currentStat.setNewStatistic(true);
                }
            }
        } else {
            throw new IllegalStateException("Change from previous build cannot be calculated when the build coverage " +
                    "does not contained detailed statistics.");
        }
    }

    private void calculateSummary() {
        if (containsDetailedStatistics()) {
            summary = new BuildCoverageSummary(detailedStatistics, granularityToRank);
        }
    }

    private BuildCoverageSummary summary;
    private Set<CodeCoverageStatistic> detailedStatistics;
    private CodeCoverageGranularity granularityToRank;
}
