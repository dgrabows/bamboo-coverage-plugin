package com.atlassian.bamboo.plugins.coverage.charts;

import com.atlassian.bamboo.charts.collater.TimePeriodCollater;
import com.atlassian.bamboo.reports.collector.AbstractTimePeriodCollector;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.plugins.coverage.CoverageProviderLocator;
import com.atlassian.bamboo.plugins.coverage.CoverageProviderLocatorImpl;
import org.jfree.data.general.Dataset;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeTableXYDataset;

import java.util.*;

public abstract class AbstractMultiSeriesTimePeriodCollector extends AbstractTimePeriodCollector {

    public AbstractMultiSeriesTimePeriodCollector() {
        includeBuildInSeriesKey = false;
    }

    public void setCoverageProviderLocator(CoverageProviderLocator coverageProviderLocator) {
        this.coverageProviderLocator = coverageProviderLocator;
    }

    public Dataset getDataset() {
        Map seriesToPeriodMap = new HashMap();
        TimeTableXYDataset dataset = new TimeTableXYDataset();
        for (Iterator iterator = getResultsList().iterator(); iterator.hasNext();) {
            BuildResultsSummary summary = (BuildResultsSummary) iterator.next();
            String[] seriesKeys = getSeriesKeys(summary);
            if (seriesKeys != null) {
                for (int i = 0; i < seriesKeys.length; i++) {
                    String key;
                    if (includeBuildInSeriesKey) {
                        key = summary.getBuildKey() + "-" + seriesKeys[i];
                    } else {
                        key = seriesKeys[i];
                    }
                    Date buildDate = summary.getBuildDate();
                    TimePeriodCollater collaterForSeries = (TimePeriodCollater) seriesToPeriodMap.get(key);
                    if (collaterForSeries == null) {
                        collaterForSeries = createCollater(getPeriod(buildDate, getPeriodRange()), key);
                        collaterForSeries.addResult(summary);
                        seriesToPeriodMap.put(key, collaterForSeries);
                    } else if (isInPeriod(collaterForSeries.getPeriod(), buildDate)) {
                        collaterForSeries.addResult(summary);
                    } else {
                        writeCollaterToDataSet(dataset, collaterForSeries);
                        RegularTimePeriod nextPeriod = collaterForSeries.getPeriod().next();
                        while (!isInPeriod(nextPeriod, buildDate)) {
                            nextPeriod = nextPeriod.next();
                        }
                        collaterForSeries = createCollater(nextPeriod, key);
                        collaterForSeries.addResult(summary);
                        seriesToPeriodMap.put(key, collaterForSeries);
                    }
                }
            }
        }

        Collection finalPeriods = seriesToPeriodMap.values();
        TimePeriodCollater timePeriodCollater;
        for (Iterator iterator = finalPeriods.iterator(); iterator.hasNext(); writeCollaterToDataSet(dataset, timePeriodCollater))
            timePeriodCollater = (TimePeriodCollater) iterator.next();

        return dataset;
    }

    abstract protected String[] getSeriesKeys(BuildResultsSummary summary);

    protected CoverageProviderLocator getCoverageProviderLocator() {
        if (coverageProviderLocator == null) {
            coverageProviderLocator = new CoverageProviderLocatorImpl();
        }
        return coverageProviderLocator;
    }     

    protected boolean includeBuildInSeriesKey;

    private CoverageProviderLocator coverageProviderLocator;
}
