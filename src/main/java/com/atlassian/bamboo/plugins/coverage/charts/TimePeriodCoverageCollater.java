package com.atlassian.bamboo.plugins.coverage.charts;

import com.atlassian.bamboo.builder.BuildState;
import com.atlassian.bamboo.plugins.coverage.*;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultStatisticsProvider;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;
import java.util.Map;

/**
 * Gathers average code coverage for the project within a particular time period.
 */
public class TimePeriodCoverageCollater extends AbstractTimePeriodCoverageStatisticCollater {

    public TimePeriodCoverageCollater() {
    }

    public void addResult(ResultStatisticsProvider resultStatisticsProvider) {
        if (BuildState.SUCCESS.equals(resultStatisticsProvider.getBuildState()) &&
                resultStatisticsProvider instanceof BuildResultsSummary) {
            BuildResultsSummary results = (BuildResultsSummary) resultStatisticsProvider;
            Map buildData = results.getCustomBuildData();
            BuildCoverageSummary summary = getCodeCoverageStatisticPersister().readBuildCoverageSummary(buildData);
            if (summary != null) {
                totalCoverage += summary.getProjectCoveragePercent();
                resultCount++;
            }
        }
    }

    public double getValue() {
        if (resultCount > 0) {
            return totalCoverage / (double) resultCount;
        } else {
            return 0.0;
        }
    }

    private double totalCoverage;
    private int resultCount;
}
