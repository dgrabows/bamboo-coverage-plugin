package com.atlassian.bamboo.plugins.coverage;

import java.io.File;

/**
 * Parsers code coverage report files and aggregates statistics.
 */
public interface CoverageFileVisitor {
    CoverageReportParsingResults getCoverageParsingResults();

    void visitFilesThatMatch(String string);

    void visitFile(File file);
}
