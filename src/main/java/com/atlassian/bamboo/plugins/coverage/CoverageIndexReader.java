package com.atlassian.bamboo.plugins.coverage;

import com.atlassian.bamboo.index.CustomIndexReader;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import java.util.Enumeration;

public class CoverageIndexReader implements CustomIndexReader {
    public void extractFromDocument(Document document, BuildResultsSummary buildResultsSummary) {
        Enumeration fields = document.fields();
        while (fields.hasMoreElements()) {
            Field field = (Field) fields.nextElement();
            if (field.name().equals(CoverageBuildProcessor.COVERAGE_PROVIDER_KEY) ||
                    field.name().equals(BuildCoverageMapPersister.VERSION_PROPERTY) ||
                    field.name().startsWith(BuildCoverageMapPersister.COVERAGE_SUMMARY_PREFIX)) {
                buildResultsSummary.getCustomBuildData().put(field.name(), field.stringValue());
            }
        }
    }
}
