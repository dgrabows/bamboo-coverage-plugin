package com.atlassian.bamboo.plugins.coverage.charts;

import com.atlassian.bamboo.builder.BuildState;
import com.atlassian.bamboo.plugins.coverage.*;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultStatisticsProvider;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;
import java.util.Map;

/**
 * Gathers average code coverage delta for the project within a particular time period.
 */
public class TimePeriodCoverageDeltaCollater extends AbstractTimePeriodCoverageStatisticCollater {

    public TimePeriodCoverageDeltaCollater() {
        totalDelta = 0.0;
    }

    public void addResult(ResultStatisticsProvider resultStatisticsProvider) {
        if (BuildState.SUCCESS.equals(resultStatisticsProvider.getBuildState()) &&
                resultStatisticsProvider instanceof BuildResultsSummary) {
            BuildResultsSummary results = (BuildResultsSummary) resultStatisticsProvider;
            int authorCount = 0;
            if (results.getChangedByAuthors() != null && results.getChangedByAuthors().length() > 0) {
                authorCount = parseAuthors(results.getChangedByAuthors()).length;
            }
            if (authorCount < 1) {
                authorCount = 1;
            }
            Map buildData = results.getCustomBuildData();

            BuildCoverageSummary summary = getCodeCoverageStatisticPersister().readBuildCoverageSummary(buildData);

            if (summary != null) {
                totalDelta += summary.getProjectCoverageDelta() / (double) authorCount;
            }
        }
    }

    public double getValue() {
        return totalDelta;
    }

    public static String[] parseAuthors(String authors) {
        return authors.split(", +");
    }

    private double totalDelta;
}
