package com.atlassian.bamboo.plugins.coverage.charts;

import com.atlassian.bamboo.charts.collater.TimePeriodCollater;
import com.atlassian.bamboo.charts.timeperiod.AbstractTimePeriodCollater;
import com.atlassian.bamboo.plugins.coverage.BuildCoverageMapPersister;
import com.atlassian.bamboo.plugins.coverage.BuildCoveragePersister;

/**
 * Base class for gathering average code coverage statistics within a particular time period.
 */
public abstract class AbstractTimePeriodCoverageStatisticCollater extends AbstractTimePeriodCollater implements TimePeriodCollater {
    public void setCodeCoverageStatisticPersister(BuildCoveragePersister buildCoveragePersister) {
        this.buildCoveragePersister = buildCoveragePersister;
    }

    protected BuildCoveragePersister getCodeCoverageStatisticPersister() {
        if (buildCoveragePersister == null) {
            buildCoveragePersister = new BuildCoverageMapPersister();
        }
        return buildCoveragePersister;
    }

    private BuildCoveragePersister buildCoveragePersister;
}
