package com.atlassian.bamboo.plugins.coverage.actions;

import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.bamboo.build.BuildManager;
import com.atlassian.bamboo.plugins.coverage.CoverageBuildProcessor;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * Determines if the code coverage tab should be displayed for a build plan.
 */
public class CodeCoverageBuildWebItemCondition implements Condition {
    private BuildManager buildManager;

    public void init(Map arguments) throws PluginParseException {

    }

    public boolean shouldDisplay(Map context) {
		String buildKey = context.get("buildKey") == null ? null : (String) context.get("buildKey");
		if (buildKey == null) {
			return false;
        }
        com.atlassian.bamboo.build.Build build = buildManager.getBuildByKey(buildKey);
		if (build == null) {
			return false;
        }
		Map customConfiguration = build.getBuildDefinition().getCustomConfiguration();
		return customConfiguration != null
                && customConfiguration.containsKey(CoverageBuildProcessor.COVERAGE_ENABLED_KEY)
                && !StringUtils.isEmpty((String) customConfiguration.get(CoverageBuildProcessor.COVERAGE_ENABLED_KEY))
                && customConfiguration.get(CoverageBuildProcessor.COVERAGE_ENABLED_KEY).equals("true");
    }


    public BuildManager getBuildManager() {
        return buildManager;
    }

    public void setBuildManager(BuildManager buildManager) {
        this.buildManager = buildManager;
    }
}
